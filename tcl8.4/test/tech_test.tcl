#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

proc main {} {
    global argv
    array set opt $argv
    if {![info exists opt(--lib)]} {
        error "Missing --lib"
    }

    oa::oaDMInit
    oa::oaTechInit

    if {[info exists opt(--libdefs)]} {
        oa::oaLibDefList_openLibs $opt(--libdefs)
    } else {
        oa::oaLibDefList_openLibs
    }

    set techlib [oa::oaLib_find $opt(--lib)]
    if {$techlib == "NULL"} {
        error "Couldn't find $opt(--libs)."
    }

    set lib_access [oa::oaLibAccess "read"]
    if {![$techlib getAccess $lib_access]} {
        error "lib $opt(--lib): No Access!"
    }

    set tech [oa::oaTech_open $techlib]
    if {$tech == "NULL"} {
        error "No tech info!"
    }

    oa::foreach layer [$tech getLayers] {
        set layer_name [$layer getName]
        puts "layer [$layer getNumber] = $layer_name"
    }
}

main

