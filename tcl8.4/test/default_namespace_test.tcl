#!/usr/bin/env tcl
##############################################################################
# Copyright 2011 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

proc checkNameSpace {nsName} {
    set ns [$nsName]
    oa::oaNameSpace_eval $ns {
        set defaultNs [oa::oaNameSpace_getDefault]
        if {$defaultNs != $ns} {
            puts "$nsName did not work.  Expected $ns, got $defaultNs"
        } else {
            puts "$nsName works!"
        }
    }
}

proc main {} {
    puts [oa::oaNameSpace_getDefault]
    checkNameSpace oa::oaCdbaNS
    checkNameSpace oa::oaUnixNS
    checkNameSpace oa::oaSpefNS
    checkNameSpace oa::oaSpfNS
    checkNameSpace oa::oaWinNS
    checkNameSpace oa::oaSpiceNS
    checkNameSpace oa::oaNativeNS
    checkNameSpace oa::oaLefNS
    checkNameSpace oa::oaVerilogNS
    checkNameSpace oa::oaVhdlNS
}

main

