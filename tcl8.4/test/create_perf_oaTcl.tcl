#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################
#

package require oa


#############################################################################
# FUNCTIONS
##############################################################################

proc create_rectangle { block max_shapes unit_size use_lnum use_pnum } {
  for {set i 1} {$i < $max_shapes} {incr i} {
      set left [expr $i*$unit_size]
      set bottom 0
      set right [expr ($i+1)*$unit_size]
      set top $unit_size
      set box [oa::Box $left $bottom $right $top]
      oa::RectCreate $block $use_lnum $use_pnum $box
  }
}

proc create_paths { block max_shapes unit_size use_lnum use_pnum } {
  for {set i 1} {$i < $max_shapes} {incr i} {
     set st [ expr $i*2*$unit_size]
     set pt1 [list $st 0 ]
     set pt2 [list [expr $st+$i*$unit_size] 0]
     set pt3 [list [expr $st+$i*$unit_size] [expr $i*$unit_size]]
     set pts [list $pt1 $pt2 $pt3]
     oa::PathCreate $block $use_lnum $use_pnum [expr $unit_size/4] $pts
  }
}

proc iter_shapes { block } {
  set shapes [oa::getShapes $block]
  while {[set shape [oa::getNext $shapes]] != ""} {
      oa::isValid $shape
  }
}

proc show_time { title exe_block } {
    puts $title
    set timer [oa::Timer]
    #exec callgrind_control -i=on
    eval $exe_block
    #exec callgrind_control -i=off
    #exec callgrind_control --dump="$title"
    set elapsed_time [format "%4.2f" [oa::getElapsed $timer] ]
    puts "Elapsed time: $elapsed_time"
}


proc main {} {


  ##############################################################################
  # DEFAULTS
  ##############################################################################

  # Layer definitions: name, lay_num, material_str
  set layers { { poly     1  poly  }
               { ndiff    2  nDiff }
               { pdiff    3  pDiff }
               { contact  4  cut   }
               { metal1   5  metal }
               { via1     6  cut   } }

  set max_shapes 1000000
  set unit_size  1.0


  ##############################################################################
  # ARGUMENTS
  ##############################################################################

  global argv
  array set opt $argv

  if {![info exists opt(--lib)]} {
    error "Missing --lib"
  } elseif {![info exists opt(--cell)]} {
    error "Missing --cell"
  } elseif {![info exists opt(--view)]} {
    error "Missing --view"
  }
 
  set libname  $opt(--lib)
  set cellname $opt(--cell)
  set viewname $opt(--view)

  if {![info exists opt(--test-safety)]} {
    set test_safety 0
  } else {
    set test_safety 1
  }

  oa::DesignInit

  if {[info exists opt(--libdefs)]} {
    oa::LibDefListOpenLibs $opt(--libdefs)
  } else {
    oa::LibDefListOpenLibs
  }

  # Create the library

  set lib [oa::LibFind $libname ]
  if {$lib != ""} {
      error "Library already exists - remove first: $libname"
  }

  set lib [oa::LibCreate $libname "./$libname" [ oa::LibMode shared ] "oaDMFileSys" ]
  if {![oa::getAccess $lib "write"]} {
    error "Unable to get write access."
  }

  # Create the tech in the new library

  set tech [ oa::TechCreate newlib ]

  foreach { layer_info } $layers {
      foreach { name num mat_str } $layer_info {
          oa::PhysicalLayerCreate $tech  $name $num [ oa::Material $mat_str ]
      }
  }
    
  set use_lnum [ lindex [ lindex $layers 0 ] 1 ]

  set use_pnum $oa::oavPurposeNumberDrawing

  # Create a design and top block

  set view_type   [oa::ViewTypeGet [oa::ReservedViewType "maskLayout"] ]

  set design [oa::DesignOpen $libname $cellname $viewname $view_type "w" ]
  set block [oa::BlockCreate $design]

  # Create a bunch of rectangles

  show_time "Create rectangles" "create_rectangle $block $max_shapes $unit_size $use_lnum $use_pnum"

  # Create a bunch of paths

  show_time "Create paths" "create_paths $block $max_shapes $unit_size $use_lnum $use_pnum"

  # Iterate through all shapes in the block

  show_time "Iterate shapes" "iter_shapes $block"

  oa::save $design
  oa::close $design
  oa::save $tech
}

main

#eof

