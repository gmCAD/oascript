###############################################################################
#
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

include ../make.variables

ifeq "$(MAKELEVEL)" "0"
  $(info "make level is target_language")
endif


SWIGIGNORE	+= -w201 -w362 -w365 -w366 -w367 -w368 -w378 -w389 -w503 -w383 -w509 -w361 -w394 -w395 -w401 -w314 -w315
SWIGFLAGS	+= -c++ -tcl -namespace -prefix oa -nodefaultctor -I.. -I. -I$(SWIGINC)/tcl

CPPFLAGS	+= -I$(TCL_INC) -I.. -I$(SWIGINC)/tcl
TCL_ARCH	:= $(shell uname -i)

default : all

tcl : all

builddir:
	mkdir -p oa/$(TCL_ARCH)
	mkdir -p oa/etc

_PHONY : default all tcl $(MODS) wrappers clean builddir

.SECONDARY : $(MODS=%=%_wrap.cc) $(MODS:%=%_wrap.o)

IFILES := $(wildcard *.i) $(wildcard ../macros/*.i) $(wildcard ../generic/typemaps/*.i)

define mod_rules

$(1) : oa/$(1).tcl oa/$(TCL_ARCH)/$(1).so

$(1)_wrap.cc : ../$(1).i $$(IFILES)
	$$(SWIG) $$(SWIGFLAGS) -o $$@ -outdir oa ../$(1).i
	patch $$@ < fix_cc.diff.1

oa/$(TCL_ARCH)/$(1).so : LDLIBS += $($(1)_LDLIBS)

oa/$(TCL_ARCH)/$(1).so : $(1)_wrap.o
	$$(CXX) $$(LDFLAGS) -o $$@ $$^ $$(LDLIBS)

endef

$(foreach mod,$(MODS),$(eval $(call mod_rules,$(mod))))

oa/etc/oaAppDef.tcl : oaAppDef.tcl
	cp -f $< $@

oa/etc/namespace.tcl : namespace.tcl
	cp -f $< $@

oa/etc/oaTclCompat.tcl : oaTclCompat.tcl
	cp -f $< $@

pkgIndex.tcl : builddir $(MODS:%=oa/$(TCL_ARCH)/%.so) oa/etc/oaAppDef.tcl oa/etc/namespace.tcl oa/etc/oaTclCompat.tcl
	cp -f pkgIndex.tcl.src pkgIndex.tcl

all : pkgIndex.tcl

wrappers : $(MODS:%=%_wrap.cc)

clean :
	$(MAKE) -C test clean
	rm -rf pkgIndex.tcl *_wrap.o *_wrap.cc *_wrap.cc.orig oa

test:
	LD_LIBRARY_PATH=$(OA_ROOT)/$(OA_LIB):$(LD_LIBRARY_PATH) $(MAKE) -C test

# -------- stuff for testing the labs

export OA_ROOT
export OA_OPTMODE
#export OAS_ROOT = $(realpath ..)
export OAS_ROOT = $(CURDIR)/..

LABS = labs

TEST_TARGETS :=

include ../testtarget.mk

