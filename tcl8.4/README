This is a description of the Tcl bindings for OpenAccess.

There is no comprehensive API documentation for the Tcl interface; please
refer to the C++ OpenAccess API documentation.

--------------------------------------------------------------------------------
Getting Started
--------------------------------------------------------------------------------

To use the Tcl bindings, the TCLLIBPATH environment variable must include the
oasTcl 'tcl8.4' directory (the directory where this README file is located).

Then the package can be loaded into a Tcl interpreter by invoking:

package require oa

--------------------------------------------------------------------------------
Modules and Namespaces
--------------------------------------------------------------------------------

The build process for the Tcl bindings creates eight loadable Tcl modules, based
on the subdirectories in the OA source area:

base.so
common.so
design.so
dm.so
plugin.so
tech.so
util.so
wafer.so

A top-level package index file will automatically load all of these into the
interpreter upon request for the oa package.

All of the Tcl commands in the API are contained in the oa:: namespace.

--------------------------------------------------------------------------------
Examples
--------------------------------------------------------------------------------

The "test" and "labs" directory contain several scripts that exercise the Tcl
OpenAccess bindings.  These scripts can be used as examples.

--------------------------------------------------------------------------------
Class Static Methods and Class Instance Methods
--------------------------------------------------------------------------------

Class static methods are named in the form: <class>_<method>.

For example, the C++ call oa::oaLibDefList::openLibs can be called in Tcl as
follows:

oa::oaLibDefList_openLibs

Class instances are treated as commands in the Tcl bindings.  To call a method
given a class, invoke the instance as a command and pass the method name as the
second argument.  For example, to get the top block given an oaDesign instance,

set topBlock [$design getTopBlock]

--------------------------------------------------------------------------------
Object Validation (Scripting Safety)
--------------------------------------------------------------------------------

When calling an instance method, the Tcl interface will try to verify that the
calling object and any objects passed in as arguments are valid (using the
oaObject::isValid() function from the C++ API).

If an instance or object is found to be invalid, the Tcl API will issue a Tcl
error.

Note that the bindings does not detect invalid iterators, and will likely crash
if one is used.

--------------------------------------------------------------------------------
Enumerated Types and Enum Wrappers
--------------------------------------------------------------------------------

All enumerated values in OA (e.g. oaLibAccessEnum) are available as integer
constants in the oa namespace:

set access $oa::oacReadLibAccess

Classes that are simple wrappers around an enumerated value (ex. oaLibAccess)
are supported in the wrappers:

set access [oa::oaLibAccess $oa::oacReadLibAccess]

As a convenience, when a function takes an enum wrapper argument, you may
either pass an enum wrapper object, or you may pass the enumerated value
directly.  Either of these will work:

$lib getAccess [oa::oaLibAccess $oa::oacReadLibAccess]
$lib getAccess $oa::oacReadLibAccess

To obtain the enumerated value from an enum wrapper, use the special "typeEnum"
member function:

set orient [$ref getOrient]
if {[$orient typeEnum] == $oa::oacR90} {
    ...
}

--------------------------------------------------------------------------------
oaString
--------------------------------------------------------------------------------

The oaString class is translated as a Tcl string, and a Tcl string
is used in place of an oaString object for function arguments.  For calls in
C++ API that use a pass-by-reference model for an oaString return value, the
Tcl API will return a Tcl string instead.  For example:

    set libpath [$lib getPath]
    puts "libpath = $libpath"

--------------------------------------------------------------------------------
oaNames and the Default oaNameSpace
--------------------------------------------------------------------------------
The oa Name classes are specially typemapped to support auto-conversion from
regular strings when used as an input argument.  The string value is
automatically converted to the appropriate Name class using the default
oaNameSpace.

So for instance, the following scripting code:

   set ns [oa::oaNativeNS]
   set libName [oa::oaScalarName $ns "Lib"]
   set cellName [oa::oaScalarName $ns "Cell"]
   set viewName [oa::oaScalarName $ns "View"]
   set design [oa::oaDesign_open $libName $cellName $viewName "r"]

Can be written much more simply:

   set design [oa::oaDesign_open "Lib" "Cell" "View" "r"]

oaScript has also added extended the OpenAccess API to allow quick
string retrieval using the default oaNameSpace where otherwise
oaScalarNames would be required.

Here's an example.  The following code:

   set ns [oa::oaNativeNS]
   set libName [oa::oaScalarName]
   $design getLibName $libName
   set libStr ""
   $libName get $ns libStr
   puts $libStr

Can be abbreviated to:
   puts [$design getLibName]

And given an oaInst, this code:

   set ns [oa::oaNativeNS]
   set instName [oa::oaScalarName]
   $inst getName $instName
   set instStr ""
   $instName get $ns instStr
   puts $instStr

Can be instead written as:
   puts [$inst getName]

The default oaNameSpace can be obtained by calling:

oa::oaNameSpace_getDefault

The default oaNameSpace is initially an oa::oaNativeNS and it can be changed
using oa::oaNameSpace_push and restored to its original value using
oa::oaNameSpace_pop.  Since the "push" and "pop" calls internally manipulate
a stack, nesting is allowed.

The default oaNameSpace can be overriden for a code block.  To do this, call
oa::oaNameSpace_eval wth your override namespace and script.  For example:

oa::oaNameSpace_eval [oa::CdbaNS] { myScript }

--------------------------------------------------------------------------------
oaPoint
--------------------------------------------------------------------------------

As in the C++ API, coordinates are always integer values in DB units.

Like the oaString class, calls in the C++ API that use a pass-by-reference model
for an oaPoint& argument can instead return an oaPoint in the Tcl API, for
example, given a oaRef object:

set point [$ref getOrigin]

For functions that accept const oaPoint& arguments, the Tcl API accepts either
a list of two integers, or an oaPoint object.  For example,

set point { 0 10 }
$ref setOrigin $point

or, the equivalent:

set point [oa::oaPoint 0 10]
$ref setOrigin $point

The string representation of an oaPoint object can be obtained by calling
"toStr" on the object.  For example,

puts [$point toStr]

Will print: 0 10

--------------------------------------------------------------------------------
oaBox
--------------------------------------------------------------------------------

Functions that accept an oaBox& in the C++ API also will return an oaBox object 
in Tcl.  For example,

set bbox [$shape getBBox]

Functions that accept const oaBox& arguments can be passed either a list of four
integers or an oaBox object. For example,

set bbox { 0 0 10 10 }
oa::oaRect_create $block $use_lnum $use_pnum $bbox

or the equivalent:

set bbox [oa::oaBox 0 0 10 10]
oa::oaRect_create $block $use_lnum $use_pnum $bbox

The string representation of an oaBox object can be obtained by calling
"toStr" on the object.  For example,

puts [$bbox toStr]

Will print: 0 0 10 10

--------------------------------------------------------------------------------
oaTransform
--------------------------------------------------------------------------------

The oaTransform class is not specially typemapped.

set t [oa::oaTransform 10 10 $oa::oac90]

if {[$t isIdentity]} {
...
}

TODO: Allow inputs of three element lists for oaTransforms.

--------------------------------------------------------------------------------
Collections and Iterators
--------------------------------------------------------------------------------

oaCollection is supported:

    set cells [$lib getCells]

    if {![$cells isEmpty]} {
        ...
    }

The easiest way to iterate over a collection is to use oa::foreach.  For
example,

    set block [$design getTopBlock]
    oa::foreach inst [$block getInsts] {
        puts [$inst getName]
    }

To iterate using the native OA API, you can create an iterator for the C++
oaIter<class T>, use the command oaIter_<T>.  For example,

    set cellIter [oa::oaIter_oaCell $cells]
    while {[set cell [$cellIter getNext]] != "NULL"} {
        ...
    }

When an oaIter is exhausted, getNext will return the string "NULL".

--------------------------------------------------------------------------------
Arrays
--------------------------------------------------------------------------------

All oaArray<> template instantiations, and all classes that are derived from
an an oaArray<> template instantiation, are translated as regular Tcl lists.

    set biArr [oa::oaBuildInfo_getPackages]
    foreach bi $biArr {
        puts "package [$bi getPackageName]"
    }

oaArray<> objects that are returned via a pass-by-reference model in the C++
API are set as the command return value in Tcl.

--------------------------------------------------------------------------------
oaAppDef
--------------------------------------------------------------------------------

oaAppDefs are wrapped and available for use.  The interface resembles the C++
API closely with the template class parameter being passed as a string argument.

Here's an example of an oaIntAppDef for oa::oaNet:

set int [oa::oaIntAppDef_get oa::oaNet "foo"]
puts "name: [$int getName], default: [$int getDefault]"
$int getSession
oa::oaIntAppDef_find oa::oaNet "foo"

--------------------------------------------------------------------------------
Exceptions
--------------------------------------------------------------------------------

Exceptions thrown by the C++ API are caught and converted into Tcl errors.
Script authors should use Tcl's "catch" command to trap these errors.  For
example:

if {[catch { ... } err]} {
    puts "Caught an error: $err"
}

--------------------------------------------------------------------------------
Compatibility With Existing oaTcl Scripts
--------------------------------------------------------------------------------

A compatibility layer is available for existing oaTcl scripts.  To use the
compatiliby layer, use the following command before running your script:

package require oaTclCompat

The compatibilty layer is a work in progress.  It covers most of the oaTcl API
but not all of it.

