###############################################################################
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   09/07/10  1.0      scarver, Si2     OA Scripting Language Labs
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################

proc getBlock {design} {
    # Locate the top Block for the Design.
    return [$design getTopBlock]
}

proc getCellName {design} {
    return [$design getCellName $globalData::ns]
}

proc getLibName {design} {
    return [ $design getLibName $globalData::ns]
}

proc getViewName {design} {
    return [ $design getViewName $globalData::ns]
}

proc printViews {lib}  {
    set ns [oa::oaUnixNS]

    puts "---Views in lib named [$lib getName $ns]"
    oa::foreach view [$lib getViews] {
        puts "  [$view getName $ns] VT=[[$view getViewType] getName]"
    }
}


proc openDesign {scNameLib scNameCell scNameView mode}  {
    puts -nonewline "Opening Design [$scNameLib get $globalData::ns]"
    puts "/[$scNameCell get $globalData::ns]/$globalData::chpNameViewNetlist"

    set viewType [oa::oaViewType_get [oa::oaReservedViewType "netlist"] ]
    if { [catch { set design [oa::oaDesign_open $scNameLib $scNameCell $scNameView $viewType $mode] } err ] } {
        puts "  ***Can't find Design.  Check lib.defs file.  Quitting.\n" ;
        puts $err; # shc may be redundant...
        unexpectedException
    }
    return $design
}


proc makeSchSymView {chpNameCell}  {
    set scNameCell [oa::oaScalarName $globalData::ns $chpNameCell]
    set design [openDesign $globalData::scNameLibNetlist $scNameCell \
               $globalData::scNameViewNetlist $globalData::mode_read]

    set block [getBlock $design ]
    assert [$block isValid] "getBlock"

    # Save the new type of Design under the same cell/view names
    # but with in a new library local to this lab ($globalData::scNameLibSymbol).
    # NOTE: there is no way to change the ViewType with which a Design was created.
    # Then close that source Design.
    $design saveAs $globalData::scNameLibSymbol $scNameCell $globalData::scNameViewSymbol
    set block [getBlock $design ]
    assert [$block isValid] "getBlock"

    $design close

    puts "  Saved design from Lib=$globalData::chpNameLibNetlist into Lib=$globalData::chpNameLibSymbol"

    # Use openDesign() to open the newly created Design (in LibSymbol) in APPEND mode.
    # Return the Design handle.
    set design [openDesign $globalData::scNameLibSymbol $scNameCell \
               $globalData::scNameViewSymbol $globalData::mode_append]

    set block [getBlock $design ]
    assert [$block isValid] "getBlock"
    return $design
}


proc saveCloseDesign {design}  {
    puts "  Saving \"[ getCellName $design ]\" in symbol library named \"[getLibName $design]\""
    $design save
    $design close
}


proc makeLabel {block labelPosition}  {
    # Using data from the globals singleton, create a Text Object that consists
    # of the Design's cell name. Place it at the labelPosition, aligned lower left,
    # no rotation, a fixed font, height=4, no overbar, with the flags set to
    # indicate that it is to be visible and displayed in drafting mode.
    set design [$block getDesign]
    set text [oa::oaText_create $block $globalData::numLayerText \
             $globalData::numPurposeText [getCellName $design ] \
             $labelPosition   $oa::oacLowerLeftTextAlign $oa::oacR0 \
             $oa::oacFixedFont 4 ]

    # Transfer the text value of the oaText managed object into the str utility variable.
    set str [$text getText]

    puts "  Created label = $str"

    return $str
}
