#!/usr/bin/env tcl
###############################################################################
#
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################
# This program dumps the connectivity and shape data from an OpenAccess
# Design into an ASCII format that can be sorted and diff'ed to compare
# it to the expected dump contents. The output of this command should be
# passed through a "sort" command to allow the results to be deterministic
# (oaIter order cannot be guaranteed).
#
# This script uses the OAC Scripting WG Tcl bindings.
#
# This attempts to follow the C++ lab as closely as possible.  The output
# format will be slightly different and this script uses a Tcl coding style.
##############################################################################


package require oa

namespace eval dumper {

    variable df
    variable dumpName

    proc init {cellstr viewstr} {
        variable df
        variable dumpName
        #
        # Create a dump file named as a concatenation of cell and design names
        #
        set dumpName "$cellstr$viewstr.dump"
        if {[catch {set df [open $dumpName "w"]} err]} {
            error "Can't write file $dumpName"
        }
        puts "Writing dump to $dumpName"
    }

    proc log {formatStr args} {
        variable df
        set str [eval [concat format [list $formatStr] $args]]
        puts -nonewline $df $str

        # Uncomment to send a copy to stdout as well.
        #puts -nonewline $str
    }

    proc closeLog {} {
        variable df
        close $df
    }

    proc sortLog {} {
        variable dumpName
        # Sort output to same file name
        exec sort --ignore-case -o $dumpName $dumpName
    }
}

proc dumpCV {block design lib} {
    # Print high-level Design metadata.

    set ns [oa::oaNativeNS]

    set lstr [$design getLibName $ns]
    set cstr [$design getCellName $ns]
    set vstr [$design getViewName $ns]

    set ctype [$design getCellType]

    set bbox [$block getBBox]

    dumper::log "Design %s/%s/%s %s %s BBOX %s\n" $lstr $cstr $vstr \
        [[$design getViewType] getName] [$ctype getName] [$bbox toStr]
}

proc dumpInsts {block} {
    # Print all the Insts in the Design and all the InstTerms on each Inst.

    set ns [oa::oaNativeNS]

    oa::foreach inst [$block getInsts] {
        set orient [$inst getOrient]

        # The following line tests the ease-of-use extension for getName()
        #set iStr [$inst getName $ns]
        # The following line tests the native API for getName()
        $inst getName $ns iStr
        set libStr [$inst getLibName $ns]
        set cellStr [$inst getCellName $ns]
        set viewStr [$inst getViewName $ns]

        set origin [$inst getOrigin]
        set bbox [$inst getBBox]

        # Print the data on the Inst
        #
        dumper::log "Inst %s OF %s/%s/%s %s LOC (%s) BBOX %s\n" \
            $iStr $libStr $cellStr $viewStr \
            [$orient getName] [$origin toStr] [$bbox toStr]

        # Print a line for each InstTerm on this Inst
        oa::foreach iterm [$inst getInstTerms] {
            set net [$iterm getNet]
            set tStr [$iterm getTermName $ns]
            set nStr ""
            if {$net != "NULL"} {
                set nStr [$net getName $ns]
            }
            dumper::log "InstTerm %s of %s is on %s\n" $iStr $tStr $nStr
        }
    }
}

proc dumpNets {block} {
    # Print the Nets in the Design and Terms on each Net.

    set ns [oa::oaNativeNS]

    oa::foreach net [$block getNets] {
        set stype [$net getSigType]
        set nStr [$net getName $ns]

        # Print the Net line
        #
        dumper::log "Net %s %s \n" $nStr [$stype getName]

        # Print a line for each Term on this Net
        #
        oa::foreach term [$net getTerms] {
            set ttype [$term getTermType]
            set tStr [$term getName $ns]

            dumper::log "Term %s on %s type %s" $tStr $nStr [$ttype getName]
            oa::foreach pin [$term getPins] {
                dumper::log " Pin=%s" [$pin getName]
            }
            dumper::log "\n"
        }
    }
}

proc dumpShapes {block design} {
    # Print all of the shapes in the Block, include type and bounding box,
    # plus the name of any Pin or Net associated.

    set ns [oa::oaNativeNS]
    set tech [$design getTech]

    oa::foreach shape [$block getShapes] {

        # Start the Shape line with its type and bounding box
        #
        set bbox [$shape getBBox]
        dumper::log "Shape %s" [[$shape getType] getName]
        if {[[$shape getType] typeEnum] == $oa::oacTextType} {
            dumper::log "\"%s\"" [$shape getText]
        }
        dumper::log " at %s" [$bbox toStr]

        # Add layer and purpose names to the line
        #
        set lnum [$shape getLayerNum]
        set pnum [$shape getPurposeNum]
        
        if {$tech != "NULL"} {
            set layer [oa::oaLayer_find $tech $lnum]
            set purp [oa::oaPurpose_find $tech $pnum]
            if {$layer != "NULL"} {
                set lStr [$layer getName]
            } else {
                set lStr ""
            }
            if {$purp != "NULL"} {
                set pStr [$purp getName]
            } else {
                set pStr ""
            }
            dumper::log " LPP %s/%s" $lStr $pStr
        } else {
            # If there is no Tech, can't bind the Layer and Purpose nums to names;
            # so just print the number pair instead.
            #
            dumper::log " LPP %d/%d" $lnum $pnum
        }

        # Note if this is a Pin Shape.
        #
        if {[$shape hasPin]} {
            set pin [$shape getPin]
            set pStr [$pin getName]
            dumper::log " on pin %s" $pStr
        }

        # Note is this Shape is attached to a Net.
        #
        if {[$shape hasNet]} {
            set net [$shape getNet]
            set nStr [$net getName $ns]
            dumper::log " on net %s" $nStr
        }
        if {[[$shape getType] typeEnum] == $oa::oacPathSegType && [$shape hasRoute]} {
            dumper::log " ROUTE"
        }

        dumper::log "\n"
    }
}

proc dumpRtPoint {pt key} {
    # Print a begin or an end point for a Route.
    #
    set ns [oa::oaNativeNS]
    if {$pt == "NULL"} {
        dumper::log "%s NULL " $key
        return
    }
    dumper::log "%s %s" $key [[$pt getType] getName]
    set t [[$pt getType] typeEnum]
    if {$t == $oa::oacStdViaType || $t == $oa::oacCustomViaType} {
        set via $pt
        dumper::log "\[%s\]" [$via getViaDefName]
    } elseif {$t == $oa::oacInstTermType} {
        set instTerm $pt
        dumper::log "\[%s" [[$instTerm getInst] getName $ns]
        dumper::log "/%s\]" [[$instTerm getTerm] getName $ns]
    } elseif {$t == $oa::oacSteinerType} {
    } elseif {$t == $oa::oacPinType} {
        set pin $pt
        dumper::log " %s" [$pin getName]
        dumper::log "\[%s\]" [[$pin getTerm] getName $ns]
    } else {
        set shape $pt
        set bbox [$shape getBBox]
        set lnum [$shape getLayerNum]
        set pnum [$shape getPurposeNum]
        dumper::log " at %s on %d/%d " [$bbox toStr] $lnum $pnum
    }
}

proc dumpRtElement {relem} {
    # Print a single RouteElement ( PathSeg or Via ).

    if {[[$relem getType] typeEnum] == $oa::oacPathSegType} {
        set seg $relem
        set ptSt [oa::oaPoint]
        set ptEn [oa::oaPoint]
        set points [$seg getPoints $ptSt $ptEn]
        dumper::log "SEG at (%s)(%s) on %d" [$ptSt toStr] [$ptEn toStr] [$seg getLayerNum]
    } else {
        # Hope it's a Via -- probably should check that here!
        #
        set via $relem
        set ptOrigin [$via getOrigin]
        dumper::log "VIA %s at (%s)" [[$via getOrient] getName] [$ptOrigin toStr]
    }
}

proc dumpRoutes {block} {
    # Print the Design's Routes, including begin/end objects, segments, and vias.

    set ns [oa::oaNativeNS]

    oa::foreach rt [$block getRoutes] {
        set rstat [$rt getRouteStatus]
        set net [$rt getNet]
        dumper::log "Route ST %s " [$rstat getName]
        if {$net != "NULL"} {
            set nStr [$net getName $ns]
            dumper::log "NET %s " $nStr
        }
        set from [$rt getBeginConn]
        set to [$rt getEndConn]
        dumpRtPoint $from "START"
        dumpRtPoint $to "END"
        set relems [$rt getObjects]
        foreach element $relems {
            dumpRtElement $element
        }
        dumper::log "\n"
    }
}

proc oadump {libstr cellstr viewstr {cvType ""}} {
    oa::oaDesignInit

    # Parse the library definitions in the local lib.defs file
    #
    set pathLibDefs "./lib.defs"
    if {! [file exists $pathLibDefs]} {
        error "***Must have $pathLibDefs file"
    }
    oa::oaLibDefList_openLibs

    if {[set lib [oa::oaLib_find $libstr]] == "NULL"} {
        error "***Either Lib name=$libstr or its path mapping in file $pathLibDefs is invalid."
    }

    if {[catch {
        if {$cvType != ""} {
            set reservedType [oa::oaReservedViewType $cvType]
            set design [oa::oaDesign_open $libstr $cellstr $viewstr [oa::oaViewType_get $reservedType] "r"]
        } else {
            set design [oa::oaDesign_open $libstr $cellstr $viewstr "r"]
        }
    } err]} {
        error "Can't read Design $libstr/$cellstr/$viewstr (check lib.defs): $err"
    }

    puts "Reading Design $libstr/$cellstr/$viewstr."

    # Initialize the dumper to set up the file I/O
    #
    dumper::init $cellstr $viewstr

    # Now dump all selected objects in the Design
    #
    set block [$design getTopBlock]

    # Make sure bounding box of parent Design gets updated with shapes
    # in all its levels of hierarchy -- including those that were added
    # to a block while a parent instantiang that block was not open.
    #
    $design openHier 200

    dumpCV $block $design $lib
    dumpInsts $block
    dumpNets $block
    dumpShapes $block $design
    dumpRoutes $block

    dumper::closeLog
    dumper::sortLog
}

proc main {} {
    global argv

    # Check the number of arguments
    #
    if {[llength $argv] < 3} {
        error "Use oadump.tcl libraryName cellName viewName"
    }

    # If the 4th argument was supplied, it is the DesignType.
    #
    set cvType ""
    if {[llength $argv] == 4} {
        set cvType [lindex $argv 3]
    }

    # Get the names from the command line
    #
    set libstr [lindex $argv 0]
    set cellstr [lindex $argv 1]
    set viewstr [lindex $argv 2]

    oadump $libstr $cellstr $viewstr $cvType

}

if {[info exists argv]} {
    # run from the command line
    main
}

