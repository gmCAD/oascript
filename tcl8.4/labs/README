These labs are Tcl ports of the Si2 OA 10th Ed Tutorial C++ labs.  They differ 
from the C++ labs in that 
  - the "begin_skel" and "end_skel" directives have been removed, 
  - labs with multiple C++modules that compile into a single executable
    have been combined into a single script
  - variable and function names may have been changed to adhere to Tcl 
    conventions.

*IMPORTANT* Each lab lives in its own directory, and except for oadump,
each lab should be from executed ONLY from within that directory. This is 
because each lab may destroy and rebuild the lib.defs file and libraries 
IN THE CURRENT DIRECTORY, and may reference other labs' lib.defs file and 
libraries using paths relative to the current directory.  Consider a lab 
directory a sandbox for that particular lab.

*USER BEWARE* If a lab is executed from any directory other than its source 
directory, there is the risk of deleting an important lib.defs file or library.

-----------------------------------------------------------------------------
Please Note:

1) Environmental vaiables may be needed for lab to run:

export TCLLIBPATH="$TCLLIBPATH 'path to the TCL build directory'"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:'path to OpenAccess release runtime libs .../lib/<os-arch>/<mode>'

Exmples:
    export TCLLIBPATH="$TCLLIBPATH ../oa/x86_64"

    export TCLLIBPATH="$TCLLIBPATH /opt/OAS_scripting/tcl8.4/oa/x86_64"

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/OpenAccess/22.04p054/lib/linux_rhel40_64/opt


2) The netlist lab may overwrite a ./lib.defs file.

3) The design library name, path, and view may be set by hardcoded variables
in the labs.

4) Verify lab outputs to the provided gold.out files.

-----------------------------------------------------------------------------

Notes on Makefiles:

Each lab can be run WITHOUT a makefile, provided the paths are set correctly.

Makefiles are provided for two reasons:

   1) To imitate the C++ environment in which the labs were originally developed,
      so that users can use 'make run' and 'make check' to run the labs and test
      their output.

      If the labs are to be run as they are in the C++ environment, please set
      the OA_ROOT and OAS_ROOT variables in makefile.defs.

   2) To automate the testing of the language bindings. A successful lab run
      indicates that the bindings have been built correctly.

Makefiles are used at four different levels:

   1) top - builds and tests the bindings in all languages
   2) perl5/ python2.5/ ruby1.8/ tcl 8.4/ directories - builds and tests the
      bindings for that particular language. Tests inlcude the labs and optional
      other tests written for that particular language.
   3) parent lab directory (e.g. perl5/labs or tcl8.4/labs) - tests all the
      labs in that particular language. This tests ONLY the labs.

      The lab-related targets for these 3 levels are

      a) make test      - cleans, runs, and checks all the labs in all languages.  
                          A summary of errors is printed at the end.
      b) make labs      - runs and checks all the labs in all languages.
      c) make cleanlabs - cleans any logs, libraries, lib.defs, and output generated
                          by the labs.

   4) individual lab directory (e.g. perl5/labs/netlist or tcl8.4/labs/netlist) - 
                          applies to one particular lab. The targets are:

      a) make test      - cleans, runs, and checks this lab. A summary of errors
                          is printed at the end.
      b) make run       - runs this lab.
      c) make check     - checks the output of this lab. Included in the check is
                          a comparison of the dump of the lab's library contents.
      d) make cleanall  - cleans any logs, libraries, lib.defs, and output generated
                          by this lab.

      Targets run, check, and cleanall are consistent with the C++ labs.

The Makefile and makefile.defs files at the labs/ directory level are identical for all 
languages.

The Makefile in each individual lab directory is custom for that particular lab and language.


