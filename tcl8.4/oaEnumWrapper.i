/* -*- mode: c++ -*- */
/*
   Copyright 2010 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#undef ENUMWRAPPER
%define ENUMWRAPPER(ns, class, enumns, enumclass)

%rename (typeEnum) ns::class::operator enumclass() const;

%typemap(in) ns::class
{
    int val;
    if (Tcl_GetIntFromObj(interp, $input, &val) == TCL_OK) {
        $1 = ns::class((enumns::enumclass)val);
    } else {
        void* obj;
        if (SWIG_IsOK(SWIG_ConvertPtr($input, &obj, $&1_descriptor, $disown | %convertptr_flags))) {
                $1 = *(ns::class*) obj;
        } else {
            %argument_fail(SWIG_TypeError, "$type", $synmake, $argnum);
        }
    }
}

%typecheck(0) ns::class
{
    void* obj;
    int val;
    if (Tcl_GetIntFromObj(interp, $input, &val) == TCL_OK) {
        $1 = 1;
    } else if (SWIG_IsOK(SWIG_ConvertPtr($input, &obj, $&1_descriptor, 0))) {
        $1 = 1;
    } else {
        $1 = 0;
    }
}

%enddef

%include <macros/oa_enums.i>

