################################################################################
#  Copyright 2011 Synopsys, Inc.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
################################################################################

package provide appdefs 0.0

namespace eval oa {

    proc getDbInfo {objType dbTypeRef dtIndexRef domainRef} {
        upvar $dbTypeRef dbType
        upvar $dtIndexRef dtIndex
        upvar $domainRef domain
        switch $objType {
            oa::oaProp {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBasePropDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaGroup {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseGroupDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaGroupMember {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseGroupMemDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaValue {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseValueDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaConstraintGroupDef {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseConstraintGroupDefDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaConstraintParamDef {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseConstraintParamDefDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaConstraintParam {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseConstraintParamDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaConstraintDef {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseConstraintDefDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaConstraint {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseConstraintDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaGroupDef {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseGroupDefDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaConstraintGroup {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseConstraintGroupDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaConstraintGroupMem {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseConstraintGroupMemDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaConstraintGroupHeader {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseConstraintGroupHeaderDataType
                ::set domain $oa::oaNoDomain
            }
            oa::oaViaHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacViaHeaderDataType
                ::set domain $oa::oaBlockDomain
            }
            oa::oaVia {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacViaDataType
                ::set domain $oa::oaBlockDomain
            }
            oa::oaShape {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacShapeDataType
                ::set domain $oa::oaBlockDomain
            }
            oa::oaNet {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacNetDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaTerm {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacTermDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaDesign {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacDesignDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaBlock {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBlockDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaBusNetDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBusNetDefDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaBusTermDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBusTermDefDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaLPPHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacLPPHeaderDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaGuide {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacGuideDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaLayerHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacLayerHeaderDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaInstHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstHeaderDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaInst {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaRowHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacRowHeaderDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaRow {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacRowDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaVectorInstDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacVectorInstDefDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaRoute {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacRouteDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaInstTerm {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstTermDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaAssignment {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacAssignmentDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaConnectDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacConnectDefDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaPin {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacPinDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaTrackPattern {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacTrackPatternDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaMarker {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacMarkerDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaCluster {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacClusterDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaBoundary {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBoundaryDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaBlockage {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBlockageDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaScanChainSet {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacScanChainSetDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaScanChain {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacScanChainDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaScanChainInst {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacScanChainInstDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaSteiner {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacSteinerDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaCMap {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacCMapDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaGCellPattern {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacGCellPatternDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaAnalysisPoint {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacAnalysisPointDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaAnalysisOpPoint {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacAnalysisOpPointDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaOpPointHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacOpPointHeaderDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaReducedModel {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacReducedModelDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaPoleResidue {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacPoleResidueDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaElmore {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacElmoreDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaNode {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacNodeDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaDevice {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacDeviceDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaParasiticNetwork {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacParasiticNetworkDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaSubNetwork {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacSubNetworkDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaModule {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacModuleDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModInst {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModVectorInstDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacVectorInstDefDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModInstHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstHeaderDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModModuleInstHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacModuleInstHeaderDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModNet {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacNetDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModBusNetDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBusNetDefDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModInstTerm {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstTermDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModTerm {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacTermDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModBusTermDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBusTermDefDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModConnectDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacConnectDefDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaModAssignment {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacAssignmentDataType
                ::set domain $oa::oacModDomain
            }
            oa::oaOccurrence {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacOccurrenceDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccInst {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccVectorInstDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacVectorInstDefDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccInstHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstHeaderDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccModuleInstHeader {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacModuleInstHeaderDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccNet {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacNetDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccBusNetDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBusNetDefDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccInstTerm {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacInstTermDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccTerm {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacTermDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccBusTermDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacBusTermDefDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccConnectDef {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacConnectDefDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccAssignment {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacAssignmentDataType
                ::set domain $oa::oacOccDomain
            }
            oa::oaOccShape {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacOccShapeDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaFigGroup {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacFigGroupDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaFigGroupMem {
                ::set dbType $oa::oacDesignDBType
                ::set dtIndex $oa::oacFigGroupMemDataType
                ::set domain $oa::oacBlockDomain
            }
            oa::oaLib {
                ::set dbType $oa::oacLibDBType
                ::set dtIndex $oa::oacLibDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaCell {
                ::set dbType $oa::oacLibDBType
                ::set dtIndex $oa::oacCellDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaView {
                ::set dbType $oa::oacLibDBType
                ::set dtIndex $oa::oacViewDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaCellView {
                ::set dbType $oa::oacLibDBType
                ::set dtIndex $oa::oacCellViewDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaDMFile {
                ::set dbType $oa::oacLibDBType
                ::set dtIndex $oa::oacDMFileDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaDMData {
                ::set dbType $oa::oacDMDataDBType
                ::set dtIndex $oa::oacDMDataDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaTechHeader {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacTechHeaderDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaTech {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacTechDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaLayer {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacLayerDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaDerivedLayerParam {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacTechDerivedLayerParamDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaPurpose {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacPurposeDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaSiteDef {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacSiteDefDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaViaDef {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacViaDefDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaViaVariant {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseViaVariantDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaViaSpec {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacViaSpecDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaTechLayerHeader {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacTechLayerHeaderDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaTechViaDefHeader {
                ::set dbType $oa::oacTechDBType
                ::set dtIndex $oa::oacTechViaDefHeaderDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaViaVariantHeader {
                ::set dbType $oa::oacBaseDBType
                ::set dtIndex $oa::oacBaseViaVariantHeaderDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaWafer {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacWaferWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaWaferFeature {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacWaferFeatureWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaWaferDesc {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacWaferDescWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaFrame {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacFrameWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaReticle {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacReticleWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaReticleRef {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacReticleRefWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaStepperMap {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacStepperMapWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaImage {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacImageWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaFrameInst {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacFrameInstWaferDataType
                ::set domain $oa::oacNoDomain
            }
            oa::oaDesignInst {
                ::set dbType $oa::oacWaferDBType
                ::set dtIndex $oa::oacDesignInstWaferDataType
                ::set domain $oa::oacNoDomain
            }
            default {
                error "Unsupported object type for oaAppDef."
            }
        }
    }

    proc oaIntAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacIntAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaIntAppDef_get {objType name {objDef NULL} {defValue 0} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaIntAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $defValue $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaIntAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist $defValue]
        }
    }

    proc oaFloatAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacFloatAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaFloatAppDef_get {objType name {objDef NULL} {defValue 0} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaFloatAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $defValue $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaFloatAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist $defValue]
        }
    }

    proc oaStringAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacStringAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaStringAppDef_get {objType name {objDef NULL} {defValue ""} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaStringAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $defValue $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaStringAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist $defValue]
        }
    }

    proc oaIntraPointerAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacIntraPointerAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaIntraPointerAppDef_get {objType name {objDef NULL} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaIntraPointerAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaIntraPointerAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist]
        }
    }

    proc oaInterPointerAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacInterPointerAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaInterPointerAppDef_get {objType name {objDef NULL} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaInterPointerAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaInterPointerAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist]
        }
    }

    proc oaDataAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacDataAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaDataAppDef_get {objType name dataSize defValue {objDef NULL} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaDataAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $dataSize $defValue $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaDataAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist $dataSize $defValue]
        }
    }

    proc oaVarDataAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacVarDataAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaVarDataAppDef_get {objType name {defSize 0} {defValue NULL} {objDef NULL} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaVarDataAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $defSize $defValue $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaVarDataAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist $defSize $defValue]
        }
    }

    proc oaTimeAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacTimeAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaTimeAppDef_get {objType name {objDef NULL} {defValue 0} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaTimeAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $defValue $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaTimeAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist $defValue]
        }
    }

    proc oaDoubleAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacDoubleAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaDoubleAppDef_get {objType name {objDef NULL} {defValue 0} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaDoubleAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $defValue $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaDoubleAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist $defValue]
        }
    }

    proc oaVoidPointerAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacVoidPointerAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaVoidPointerAppDef_get {objType name {objDef NULL} {defValue 0} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaVoidPointerAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaVoidPointerAppDefProxy_getProxy $name $dbType $dtIndex $domain]
        }
    }

    proc oaBooleanAppDef_find {objType name {objDef NULL}} {
        oa::getDbInfo $objType dbType dtIndex domain
        if {$objDef == "NULL" || $objDef == ""} {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain]
        } else {
            ::set result [oa::oaAppDefProxy_findProxy $name $dbType $dtIndex $domain $objDef]
        }
        if {$result != "NULL" && [[$result getType] typeEnum] == $oa::oacBooleanAppDefType} {
            return $result
        }
        return NULL
    }

    proc oaBooleanAppDef_get {objType name {objDef NULL} {defValue 0} {persist 1}} {
        if {$objDef != "NULL" && $objDef != ""} {
            return [oa::oaBooleanAppDefProxy_getProxy $name $oa::oacNullIndex $oa::oacNullIndex $oa::oacNoDomain $persist $defValue $objDef]
        } else {
            oa::getDbInfo $objType dbType dtIndex domain
            return [oa::oaBooleanAppDefProxy_getProxy $name $dbType $dtIndex $domain $persist $defValue]
        }
    }

}
