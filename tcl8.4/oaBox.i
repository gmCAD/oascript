/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%header %{

int TclObj_to_oaBox(Tcl_Interp *interp, Tcl_Obj *tclobj, oaBox& box)
{
    int len;
    if (Tcl_ListObjLength(interp, tclobj, &len) == TCL_OK && len == 4) {
        Tcl_Obj* tempObj;
        oaInt4 x1, y1, x2, y2;
        if (Tcl_ListObjIndex(interp, tclobj, 0, &tempObj) == TCL_OK &&
            Tcl_GetIntFromObj(interp, tempObj, &x1) == TCL_OK &&
            Tcl_ListObjIndex(interp, tclobj, 1, &tempObj) == TCL_OK &&
            Tcl_GetIntFromObj(interp, tempObj, &y1) == TCL_OK &&
            Tcl_ListObjIndex(interp, tclobj, 2, &tempObj) == TCL_OK &&
            Tcl_GetIntFromObj(interp, tempObj, &x2) == TCL_OK &&
            Tcl_ListObjIndex(interp, tclobj, 3, &tempObj) == TCL_OK &&
            Tcl_GetIntFromObj(interp, tempObj, &y2) == TCL_OK) {
            box.set(x1, y1, x2, y2);
            return SWIG_OK;
        }
    }
    void* argp;
    if (SWIG_IsOK(SWIG_ConvertPtr(tclobj, &argp, SWIGTYPE_p_OpenAccess_4__oaBox, 0)) && argp != 0) {
        box = *(reinterpret_cast<oaBox*>(argp));
        return SWIG_OK;
    }
    return SWIG_ERROR;
}

%}

/// Ignore non-const versions of these functions
%ignore OpenAccess_4::oaBox::lowerLeft();
%ignore OpenAccess_4::oaBox::upperRight();
%ignore OpenAccess_4::oaBox::left();
%ignore OpenAccess_4::oaBox::bottom();
%ignore OpenAccess_4::oaBox::right();
%ignore OpenAccess_4::oaBox::top();

%typemap(in) const OpenAccess_4::oaBox& (OpenAccess_4::oaBox temp)
{
    /// oaBox_2: %typemap(in) const OpenAccess_4::oaBox& (OpenAccess_4::oaBox temp)
    int res;
    if (SWIG_IsOK(res = TclObj_to_oaBox(interp, $input, temp))) {
        $1 = &temp;
    } else {
        SWIG_exception_fail(SWIG_ArgError(res), "in method '$symname', argument $argnum of type '$1_type'");
    }
}

%typecheck(0) const OpenAccess_4::oaBox&
{
    /// oaBox_3: %typecheck(0) const OpenAccess_4::oaBox&
    void* tmpVoidPtr;
    int len;
    if (Tcl_ListObjLength(interp, $input, &len) == TCL_OK && len == 4) {
        $1 = 1;
    } else if (SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0))) {
        $1 = 1;
    } else {
        $1 = 0;
    }
}

