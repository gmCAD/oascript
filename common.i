/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef SWIGPERL
%module "oa::common";
#endif

#ifdef SWIGRUBY
%module "oa::common";
#endif

#ifdef SWIGTCL
%module "common";
#endif

#ifdef SWIGPYTHON
%module "common";
#endif

#ifdef SWIGCSHARP
%module "oasCSharpCommon";
#endif

%{

#include <oa/oaDesignDB.h>

USE_OA_NAMESPACE
USE_OA_COMMON_NAMESPACE

#define SWIG_SHADOW_ORIG SWIG_SHADOW
#undef SWIG_SHADOW
#define SWIG_SHADOW 0

%}

// Basic agnostic setup
%include "oa.i"

// Optional target-language-specific code:
%include <target_common_pre.i>



#if !defined(oaCommon_P)
#define oaCommon_P



// *****************************************************************************
// Platform macros
// *****************************************************************************
#if defined(_MSC_VER)
#define OA_WINDOWS
#endif



// *****************************************************************************
// Namespace and basic type declarations and definitions
// *****************************************************************************
%include "oa/oaCommonTypes.h"

// *****************************************************************************
// oaCommon Public Header Files
// *****************************************************************************
%include "oa/oaCommonHashFunction.h"
%include "oa/oaCommonHashTable.h"
%include "oa/oaCommonHashSet.h"
%include "oa/oaCommonHashMap.h"
%include "oa/oaCommonProcInfo.h"
%include "oa/oaCommonIBase.h"
%include "oa/oaCommonPlugInBase.h"
%include "oa/oaCommonPlugInMgr.h"
%include "oa/oaCommonPlugInError.h"

/* Line 76 chokes swig */
%include "munged_headers/oaCommonSPtr.h"

%include "oa/oaCommonFactory.h"
%include "oa/oaCommonAgg.h"
%include "oa/oaCommonXmlParser.h"
%include "oa/oaCommonXmlWriter.h"
%include "oa/oaCommonInstallDir.h"
%include "oa/oaCommonSharedLib.h"



// *****************************************************************************
// oaCommon Public Inline Function Files
// *****************************************************************************
%include "oa/oaCommonHashFunction.inl"
%include "oa/oaCommonHashTable.inl"
%include "oa/oaCommonHashSet.inl"
%include "oa/oaCommonHashMap.inl"
%include "oa/oaCommonProcInfo.inl"
%include "oa/oaCommonIBase.inl"
%include "oa/oaCommonPlugInBase.inl"
%include "oa/oaCommonPlugInError.inl"
%include "oa/oaCommonSPtr.inl"
%include "oa/oaCommonFactory.inl"
%include "oa/oaCommonAgg.inl"
%include "oa/oaCommonXmlParser.inl"
%include "oa/oaCommonXmlWriter.inl"
%include "oa/oaCommonInstallDir.inl"
%include "oa/oaCommonSharedLib.inl"



#endif

// Optional target-language-specific code:
%include <target_common_post.i>

%include <generic/extensions/common.i>

