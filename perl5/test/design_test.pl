#!/usr/bin/perl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

use strict;
use warnings;

use File::Basename;
use FindBin;
use Getopt::Long;

use lib dirname($FindBin::RealBin);
use oa::design;

my %options;
GetOptions(\%options,
	   "libdefs=s",
	   "lib=s",
	   "cell=s",
	   "view=s"
	   );

eval {

    oa::oaDesignInit();

    if (exists($options{libdefs})) {
	&oa::oaLibDefList::openLibs($options{libdefs});
    } else {
	&oa::oaLibDefList::openLibs();
    }

    my $ns = new oa::oaNativeNS;

    my $lib_name = $options{lib} || die "Missing mandatory -lib argument";
    my $cell_name = $options{cell} || die "Missing mandatory -cell argument";;
    my $view_name = $options{view} || die "Missing mandatory -view argument";;

    my $lib = oa::oaLib::find($lib_name);
    if (!$lib) {
	print "Couldn't find $lib_name.\n";
	exit 1;
    }
    if (!$lib->getAccess(new oa::oaLibAccess($oa::oacReadLibAccess))) {
	print "lib $lib_name: No access!\n";
	exit 1;
    }

    my $design = oa::oaDesign::open($lib_name, $cell_name, $view_name, "r");
    if (!$design) {
	print "No design!\n";
	exit 1;
    }

    my $topBlock = $design->getTopBlock();
    if (!$topBlock) {
	print "No top block!\n";
	exit 1;
    }

    my $shape_iter = new oa::oaIter::oaShape($topBlock->getShapes());
    while (my $shape = $shape_iter->getNext()) {
	my $bbox = $shape->getBBox();
	print ("Shape on layer " . $shape->getLayerNum() . " with bbox (", join(", ", @$bbox), ")\n");
    }
};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit 0;
