/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%ignore OpenAccess_4::oaPoint::x();
%ignore OpenAccess_4::oaPoint::y();
%ignore OpenAccess_4::oaPoint::oaPoint(); // This is an abomination - the coordinate is uninitialized

%typemap(in) OpenAccess_4::oaPoint& (OpenAccess_4::oaPoint tmpPoint)
{
    // typemap(in) oaPoint&
    AV *arr;
    SV **arr0, **arr1;
    int x, y;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 1 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y))) {
        tmpPoint.set(x, y);
    } else if ($input != &PL_sv_undef && !SvOK($input)) {
        // allow undef value
        tmpPoint.set(0, 0);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpPoint;
}    

%typemap(in) OpenAccess_4::oaPoint* (OpenAccess_4::oaPoint tmpPoint)
{
    // typemap(in) oaPoint*
    AV *arr;
    SV **arr0, **arr1;
    int x, y;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 1 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y))) {
        tmpPoint.set(x, y);
    } else if ($input != &PL_sv_undef && !SvOK($input)) {
        // allow undef
        tmpPoint.set(0, 0);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpPoint;
}    

%typemap(in) const OpenAccess_4::oaPoint& (OpenAccess_4::oaPoint tmpPoint)
{
    // typemap(in) const oaPoint&
    AV *arr;
    SV **arr0, **arr1;
    int x, y;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 1 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y))) {
        tmpPoint.set(x, y);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpPoint;
}

%typemap(in) const OpenAccess_4::oaPoint* (OpenAccess_4::oaPoint tmpPoint)
{
    // typemap(in) const oaPoint*
    AV *arr;
    SV **arr0, **arr1;
    int x, y;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 1 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y))) {
        tmpPoint.set(x, y);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpPoint;
}

%typemap(in) OpenAccess_4::oaPoint, const OpenAccess_4::oaPoint
{
    // typemap(in) oaPoint, const oaPoint
    AV *arr;
    SV **arr0, **arr1;
    int x, y;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 1 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y))) {
        $1.set(x, y);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
}

%typecheck(0) OpenAccess_4::oaPoint&, OpenAccess_4::oaPoint*
{
    // typecheck(0) oaPoint&, oaPoint*
    AV *arr;
    $1 = ($input != &PL_sv_undef && !SvOK($input))
         || (SvROK($input) &&
             SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
             av_len(arr) == 1 &&
             !SvROK(*(av_fetch((AV*) SvRV($input), 0, 0))));
}

%typecheck(0) const OpenAccess_4::oaPoint&, const OpenAccess_4::oaPoint*, OpenAccess_4::oaPoint, const OpenAccess_4::oaPoint
{
    // typecheck(0) const oaPoint&, const oaPoint*, oaPoint, const oaPoint
    AV *arr;
    $1 = (SvROK($input) &&
          SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
          av_len(arr) == 1 &&
          !SvROK(*(av_fetch((AV*) SvRV($input), 0, 0))));
}

%typemap(argout) OpenAccess_4::oaPoint&
{
    // typemap(argout) oaPoint&
    SV *coords[2];
    coords[0] = sv_2mortal(newSViv((IV) $1->x()));
    coords[1] = sv_2mortal(newSViv((IV) $1->y()));
    AV *arr = av_make(2, coords);
    sv_setsv($input, sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaPoint*)->clientdata)));
}

%typemap(argout) OpenAccess_4::oaPoint*
{
    // typemap(argout) oaPoint*
    SV *coords[2];
    coords[0] = sv_2mortal(newSViv((IV) $1->x()));
    coords[1] = sv_2mortal(newSViv((IV) $1->y()));
    AV *arr = av_make(2, coords);
    sv_setsv($input, sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaPoint*)->clientdata)));
}

%typemap(argout) const OpenAccess_4::oaPoint&
{
    // typemap(argout) const oaPoint&
}

%typemap(argout) const OpenAccess_4::oaPoint*
{
    // typemap(argout) const oaPoint*
}

%typemap(out) OpenAccess_4::oaPoint&
{
    // typemap(out) oaPoint&
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[2];
    coords[0] = sv_2mortal(newSViv((IV) $1->x()));
    coords[1] = sv_2mortal(newSViv((IV) $1->y()));
    AV *arr = av_make(2, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaPoint*)->clientdata));
    argvi++;
}

%typemap(out) OpenAccess_4::oaPoint*
{
    // typemap(out) oaPoint*
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[2];
    coords[0] = sv_2mortal(newSViv((IV) $1->x()));
    coords[1] = sv_2mortal(newSViv((IV) $1->y()));
    delete $1; // Note: this typemap is only used in constructors, and since we copy the data to perl SVs we need to free the constructed oaPoint
    AV *arr = av_make(2, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaPoint*)->clientdata));
    argvi++;
}

%typemap(out) OpenAccess_4::oaPoint
{
    // typemap(out) oaPoint
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[2];
    coords[0] = sv_2mortal(newSViv((IV) $1.x()));
    coords[1] = sv_2mortal(newSViv((IV) $1.y()));
    AV *arr = av_make(2, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaPoint*)->clientdata));
    argvi++;
}

%apply (OpenAccess_4::oaPoint) { oaPoint }

