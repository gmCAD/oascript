#! /usr/bin/env perl

###############################################################################
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/08/11  1.0      bpfeil, Si2      OA Scripting Language Labs
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################

use strict;
use warnings;
use File::Basename;
use FindBin;

#
## Path to bindings. Add to PERL5LIB env variable OR here
#use lib "path-to-bindings/amd-init/perl5";

use lib dirname($FindBin::RealBin);
use oa::design;

our $ns;
our $_d;

#
# Many of these values are reused by subsequent labs.
#
# Layer, purpose settings are tool/methodology-specific.
# These are rather arbitrary

our %globs =   (
 'chp_name_lib_netlist',  'Lib',
 'chp_name_lib_symbol',   'LibSymbol',
 'chp_name_view_netlist', 'netlist',
 'chp_name_view_symbol',  'symbol',
 'chp_name_libdefsfile',  'lib.defs',
 'chp_pathlib_symbol',    './LibSymbol',
 'mode_write',      'w',
 'mode_read',       'r',
 'mode_append',     'a',
 'str_layer_dev',   'device',
 'str_layer_text',  'text',
 'str_purpose_dev', 'drawing',
 'str_purpose_text','labels',
 'num_layer_dev',   '230',
 'num_purpose_dev', $oa::oavPurposeNumberDrawing,
 'num_layer_text',   '229',
 'num_purpose_text', '1013'
);


our $scname_lib_netlist;
our $scname_lib_symbol ;
our $scname_view_netlist;
our $scname_view_symbol;


eval {

    oa::oaDesignInit( $oa::oacAPIMajorRevNumber,
                      $oa::oacAPIMinorRevNumber,
                      $oa::oacDataModelRevNumber );

    $ns = new oa::oaNativeNS;

    $scname_lib_netlist = new oa::oaScalarName($ns, $globs{'chp_name_lib_netlist'} );
    $scname_lib_symbol  = new oa::oaScalarName($ns, $globs{'chp_name_lib_symbol'} );
    $scname_view_netlist= new oa::oaScalarName($ns, $globs{'chp_name_view_netlist'} );
    $scname_view_symbol = new oa::oaScalarName($ns, $globs{'chp_name_view_symbol'} );



    &map_libs ( $globs{ 'chp_name_libdefsfile' }, 
		$scname_lib_netlist, 
		$scname_lib_symbol, 
		$globs{ 'chp_pathlib_symbol'});

    # Set up Layer and Purpose nums for the layers in the Tech database.
    # to be used for the device shapes.
    #

    &set_layer_purpose($scname_lib_symbol, 
		       $globs{ 'str_layer_dev' }, 
		       $globs{ 'num_layer_dev' }, 
		       $globs{ 'str_purpose_dev'}, 
		       $globs{ 'num_purpose_dev'},  0);
    &set_layer_purpose($scname_lib_symbol, 
		       $globs{ 'str_layer_text' }, 
		       $globs{ 'num_layer_text' }, 
		       $globs{ 'str_purpose_text' }, 
		       $globs{ 'num_purpose_text'} , 1);
  

    &make_and_symbol();
    &make_or_symbol();
    &make_xor_symbol();
    &make_ha_symbol();
    &make_fa_symbol();
};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit(0);

#------------------------------------------------------------------------------


sub assert($$){
   my $condition = shift;
   my  $line = shift;
    if (! $condition) {
        printf("***ASSERT error %s", $line);
    }
}

sub set_layer_purpose () {
    my $scname_lib  = shift;
    my $layer_name  = shift;
    my $layer_num_in= shift;
    my $purp_name_in= shift; 
    my $purp_num_in = shift; 
    my $close_tech  = shift;
  # First see if a Tech database can be found already open for the lib,
  # i.e., from a prior call to set_layer_purpose (or any other module
  # if this were part of a large application).
  #
    my $tech = oa::oaTech::find( $scname_lib );
    if (!$tech) {
    #
    # If one's not already open in memory, try opening it from disk.
    #
	eval {
	    $tech = oa::oaTech::open( $scname_lib, 'a' );
      #
      # No Tech open in memory, none on disk by that name. So create one.
      #
	};
	if ($@) {
		$tech = oa::oaTech::create( $scname_lib );
		#
		# Set the DBU to 1/3 nanometer for all Netlist type CellViews.
		#
		my $view_type = oa::oaViewType::get(new oa::oaReservedViewType('netlist'));
		$tech->setUserUnits($view_type, $oa::oacMicron);
		$tech->setDBUPerUU( $view_type, 3000);
	}

    }  
  # Try locating a Purpose of the given name. If so, overwrite
  # the purp_num defaulted above with the value found in the Tech.
  # Else, create a new Purpose with this name and num in the Tech.
  #
    my $purpose = oa::oaPurpose::find($tech, $purp_name_in);
    my $purp_num;
    if ($purpose) {
	$purp_num = $purpose->getNumber();
	printf("Found Purpose name=%s num=%x\n",$purp_name_in, $purp_num);
    } else  {
	$purpose = oa::oaPurpose::create($tech, $purp_name_in, $purp_num_in);
	$purp_num = $purpose->getNumber();
	printf("Created Purpose name=%s num=%x\n", $purp_name_in,$purp_num);
    }
  #
  # If there is a Layer by the given name in the Tech already, get its num.
  # Else create one.
  #
    my $layer = oa::oaLayer::find($tech, $layer_name);
    my $layer_num;
    if ($layer) {
	$layer_num = $layer->getNumber();
	printf("Found Layer name=%s  num=%s\n",$layer_name,$layer_num);
    } else {
	$layer = oa::oaPhysicalLayer::create($tech, $layer_name, $layer_num_in);
	$layer_num = $layer->getNumber();
	printf("Created Layer name=%s  num=%s\n",$layer_name,$layer_num);
    }
    if ($close_tech) {
	$tech->save();
	$tech->close();
    }
    return $layer_num, $purp_num;
}


sub map_libs ($) {
    my $chp_name_libdefsfile = shift;
    my $scname_lib_prev = shift;
    my $scname_lib_curr = shift;
    my $chp_pathlib_curr= shift;

    my $ns = new oa::oaUnixNS;
    
    # Perform logical lib name to physical path name mappings for the source library
    # created in the netlist/ lab, then the symbol library to be created in this lab.
    #
    # Make sure the lib.defs is LOCAL to avoid clobbering a real one.
    my $strLibDefListDef;
    oa::oaLibDefList::getDefaultPath( $strLibDefListDef );
    

    if ( $strLibDefListDef ne $globs{ 'chp_name_libdefsfile'} ) {
      printf( "***Missing local %s file.\n",$globs{ 'chp_name_libdefsfile'});
      printf( "   Please create one with the one line shown below: \n");
      printf( "       INCLUDE ../netlist/lib.defs\n");
      exit 4;
	 }
    
    &oa::oaLibDefList::openLibs();
    my $lib_prev = oa::oaLib::find($scname_lib_prev);
    if (! $lib_prev) {
	printf("***Either lib.defs file missing or netlist lab was not completed.");
	exit 1;
    }
    my $strNameLibCurr;
    $scname_lib_curr->get($ns, $strNameLibCurr );
    
    my $lib_curr = oa::oaLib::find( $scname_lib_curr );
    
    if ( $lib_curr ) {
      printf( "%s Lib definition found in %s and opened.",
              $globs{ 'strNameLibCurr' },$globs{ 'chp_name_libdefsfile' });
    } else { 
      # If this program was run once already, a Lib directory will exist.
      # If the lib.defs file was deleted, the Lib for this current lab won't
      # have been opened by oaLibDefFile::load(), so it must be opened now.
      # If the updated lib.defs (with the def for this lab's Lib) remains but
      # this lab's Lib directory was deleted, then the oaLib::find() above would
      # have failed and the exists() test below will also fail;
      # hence, the subsequent create() will be invoked.
      #
	if ( oa::oaLib::exists( $strNameLibCurr ) ) {
	    printf( "Opened from disk existing Lib=%s\n",$strNameLibCurr);
	    $lib_curr = oa::oaLib::open( $scname_lib_curr, $chp_pathlib_curr);
	} else { 
	    # If there is no directory for the Lib, then it must be created now.
	    #

	    $lib_curr = oa::oaLib::create( $scname_lib_curr, 
					   $chp_pathlib_curr, 
                                           new oa::oaLibMode($oa::oacSharedLibMode),
					   "oaDMFileSys" );
	    printf("Created new Lib=%s\n",$strNameLibCurr);
	    #
	    # Update the lib.defs file for future labs that reference this Design.
	    #
#>>>	    my $ldl = oa::oaLibDefList::get( $strLibDefListDef, "a" );
#>>>	    &oa::oaLibDef::create( $ldl, $scname_lib_curr, $chp_pathlib_curr );
#>>>	    $ldl->save();
#>>> Problems with oaPerl and oaPython getting an append of the  libSymbol to the ./lib.defs file.
#>>> See bug report 1336: http://www.si2.org/openeda.si2.org/tracker/?func=detail&atid=314&aid=1336&group_id=78
#>>> Finish lab with plain perl append to lib.defs which is needed for oadump post process step.
	    open(LIBDEFS, '>>./lib.defs');
	    my $str = "\nDEFINE LibSymbol ./LibSymbol\nASSIGN LibSymbol writePath ./LibSymbol\nASSIGN LibSymbol libMode shared\n";
	    print LIBDEFS $str;
	    close(LIBDEFS);

	}
    }

    return $lib_prev, $lib_curr;

}


sub get_block($) {
    my $design = shift;
  # Locate the top Block for the Design.
    return $design->getTopBlock();
}

sub get_cell_name($) {
    my $design = shift;
    my $cell_name;
    $design->getCellName($ns, $cell_name);
    return $cell_name;
}

sub get_lib_name($) {
    my $design = shift;
    my $lib_name;
    $design->getLibName( $ns, $lib_name );
    return $lib_name;
}

sub get_view_name($) {
    my $design = shift;
    my $view_name;
    $design->getViewName( $view_name );
    return $view_name;
}

sub print_views($) {
    my $lib = shift; 
    my $nsunix = new oa::oaUnixNS();

    my $lib_name;
    $lib->getName($lib_name);
    printf("---Views in lib named %s\n",$lib_name);

    my $itIter = new oa::oaIter::oaLib($lib->getViews());
    while (my $view = $itIter->getNext()) {
	my ($view_name, $view_type, $view_type_name);
	$view->getName( $view_name );
        $view_type= $view->getViewType();
	$view_type_name = $view_type->getName();
	printf( "  %s VT=%s\n", $view_name,$view_type_name);
    }
}


sub open_design($$$$) {
    my $scname_lib  = shift; 
    my $scname_cell = shift; 
    my $scname_view = shift; 
    my $mode        = shift;


    printf( "Opening Design %s %s %s\n", $scname_lib->get(), $scname_cell->get(),$scname_view->get()); 

    my $viewType = oa::oaViewType::get( $oa::oacNetlist );

    my $design;
    eval {
	$design = oa::oaDesign::open( $scname_lib, $scname_cell, $scname_view, $viewType, $mode);
    };
    if ($@) {
        my $lineno = __LINE__ - 2;
	my $msg= sprintf("Can't find Design. Check lib.subs file.");
	unexpected_exception( $msg, $lineno )
    }
    return $design;
}


sub make_sch_sym_view($) {
    my $chp_name_cell = shift;

    my $scname_cell = new oa::oaScalarName( $ns, $chp_name_cell );

    my $design = open_design( $scname_lib_netlist, $scname_cell,
                              $scname_view_netlist, $globs{ 'mode_read' } );

    my $block = get_block( $design );
    assert( $block->isValid(), __LINE__ );

    # Save the new type of Design under the same cell/view names
    # but with in a new library local to this lab ($GlobalData.scname_lib_symbol).
    # NOTE: there is no way to change the ViewType with which a Design was created.
    # Then close that source Design.
    $design->saveAs( $scname_lib_symbol, $scname_cell, $scname_view_symbol ); 
    $block = get_block( $design );
    assert( $block->isValid(), __LINE__ );

    $design->close();

    printf( "  Saved design from Lib=%s into Lib=%s\n",
	    $globs{ 'chp_name_lib_netlist'},$globs{ 'chp_name_lib_symbol' });

    # Use open_design() to open the newly created Design (in LibSymbol) in APPEND mode.
    # Return the Design handle.
    $design = open_design( $scname_lib_symbol , $scname_cell, $scname_view_symbol, $globs{ 'mode_append' });

    $block = get_block( $design );

    assert( $block->isValid(), __LINE__ );
    return $design;
}


sub save_close_design($) {
    my $design= shift;
    printf( "  Saving \"%s\" in symbol library named \"%s\"\n",
	    get_cell_name( $design ),  get_lib_name( $design ) );
    $design->save();
    $design->close();
}


sub make_label($$) {
    my $block = shift;
    my $label_position = shift;
    # Using data from the globals singleton, create a Text Object that consists
    # of the Design's cell name. Place it at the label_position, aligned lower left,
    # no rotation, a fixed font, height=4, no overbar, with the flags set to
    # indicate that it is to be visible and displayed in drafting mode.
    my $design = $block->getDesign();
    my $text = oa::oaText::create( $block, 
				   $globs{ 'num_layer_text' },
				   $globs{ 'num_purpose_text' }, 
				   get_cell_name( $design ),
				   $label_position,  
				   $oa::oacLowerLeftTextAlign, 
				   $oa::oacR0,
				   $oa::oacFixedFont, 
				   4 );
	
	# Transfer the text value of the oaText managed object into the str utility variable.
    my $str;
    $text->getText($str);
	
    printf( "  Created label = %s\n", $str);
	
    return $str;
}


# Create Shapes that represent a simple schematic symbol for the leaf cell.
#   (These are not electrical or manufacturing geometries, merely Shapes
#   that comprise the symbol on the schematic.)

sub make_fa_symbol() {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    my $label = "FullAdder";
    my $design = make_sch_sym_view( $label );
    my $block = $design->getTopBlock();

    # Create a rectangle shape to be the full-adder symbol with the lower left at
    # the origin and the upper right at {60,52}. Use the layer and purpose from
    # the globals singleton object.
    #
    #         3,95_________________159,95
    #            |                 |
    #            |                 |
    #            |                 |
    #            |                 |
    #            |_________________|
    #         3, 3                 159, 3
    #
    my @box = ( 3, 3, 159, 95 );
    &oa::oaRect::create( $block, 
			$globs{ 'num_layer_dev' }, 
			$globs{ 'num_purpose_dev' },
			\@box );

    # Use the make_label function to put the cell's name at the location {2,2}
    my @origin = ( 2, 2 );
    &make_label( $block, \@origin );

    &save_close_design( $design );
}

# Create Shapes that represent a simple schematic symbol for the leaf cell.
#   (These are not electrical or manufacturing geometries, merely Shapes
#   that comprise the symbol on the schematic.)

sub make_ha_symbol() {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    my $label  = "HalfAdder";
    my $design = make_sch_sym_view( $label );
    my $block  = $design->getTopBlock();

    # Create a rectangle shape to be the half-adder symbol with the lower left at
    # the origin and the upper right at {64,50}. Use the layer and purpose from
    # the globals singleton object.
    #
    #      0,50__________64,50
    #         |          |
    #         |          |
    #         |          |
    #         |          |
    #         |__________|
    #       0,0          64,0
    #
    my @box = ( 0, 0, 64, 50 );
    &oa::oaRect::create( $block, 
			$globs{ 'num_layer_dev' }, 
			$globs{ 'num_purpose_dev' },
			\@box );

    # Use the make_label function to put the cell's name at the location {2,25}
    my @pt = ( 2, 25 );
    &make_label( $block, \@pt );

    &save_close_design( $design );
}

sub make_and_symbol() {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    my $label = "And";
    my $design = make_sch_sym_view( $label );
    my $block = $design->getTopBlock();

    # make a crude AND symbol out of two open Shapes
    #
    #    0,24              16,24
    #       .---------------+
    #       |                    +
    #       |                       +
    #       |                         +
    #       |                          +
    #       |                           +
    #       |                           o <--- midpoint is 28,12
    #       |                           +
    #       |                          +
    #       |                         +
    #       |                       +
    #       |                    +
    #       .---------------+
    #     0,0              16,0

    # Create a PointArray representing the 3-sided, open box part on the left.
    #
    my @pt0 = ( 16,  0 );
    my @pt1 = (  0,  0 );
    my @pt2 = (  0, 24 );
    my @pt3 = ( 16, 24 );
    my @pts = ( \@pt0, \@pt1, \@pt2,\@pt3 );

    # Create in the owner Block the appropriate, non-closed oaShape using the array.
    oa::oaLine::create( $block, 
			$globs{ 'num_layer_dev' }, 
			$globs{ 'num_purpose_dev' }, 
			\@pts );

    # Create an arc representing the part on the right.
    # To make it easy, assume the curve is simply the right half of
    # an ellipse centered in a bounding Box 12 to the right and 12 left
    #
    #    0,24   4,24       16,24
    #       |   |           |
    #            ---------- + ----------
    #           |                +      |
    #           |                   +   |
    #           |                     + |
    #           |                      +|
    #           |                       +
    #           |                       o <--- midpoint is 28,12
    #           |                       +
    #           |                      +|
    #           |                     + |
    #           |                   +   |
    #           |                +      |
    #            ---------- + ----------
    #       |   |           |
    #     0,0   4,0         16,0

    # Define the BBox for the right arc of the "And" symbol.
    my @bbox = ( 4, 0, 28, 24 );

    # Create an appropriate open Shape for this right-hand part of the "And" symbol.
    # Assume the angle sweeps from -90 to 90 degrees to make it easy)
    my $radiansStart = -1.571;
    my $radiansEnd =    1.571;
    oa::oaArc::create( $block, 
		       $globs{ 'num_layer_dev' }, 
		       $globs{ 'num_purpose_dev' },
		       \@bbox, 
		       $radiansStart, 
		       $radiansEnd );

    # Use the make_label function to put the cell's name at the location {2,2}
    my @pt = ( 2, 2 );
    &make_label( $block, \@pt );

    &save_close_design( $design );
}


sub make_or_symbol() {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    my $label = "Or";
    my $design = make_sch_sym_view( $label );
    my $block = $design->getTopBlock();

    # make a crude OR symbol out of two identical arcs, left and right
    # plus two lines joining them top and bottom
    #
    #    0,24               16,24
    #       .---------------+
    #            +               +
    #               +               +
    #                 +               +
    #                  +               +
    #                   +               +
    #         12,12 --> o               o <--- 28,12
    #                   +               +
    #                  +               +
    #                 +               +
    #               +               +
    #            +               +
    #       .---------------+
    #     0,0               16,0

    # Copy the same code that made the oaArc for the AND symbol and assign it to arc_right.
    my @bbox = ( 4, 0, 28, 24 );
    my $arc_right = oa::oaArc::create( $block, 
				       $globs{ 'num_layer_dev' },  
				       $globs{ 'num_purpose_dev' },
                                       \@bbox, 
				       -1.571,  
				       1.571 );

    # Reuse this arc by making a copy of it in the Block and moving it left by (16,0)
    my @trans = ( -16, 0, $oa::oacR0 );
    $arc_right->copy( \@trans );


    # Create the top Line from PointArray pa
    my @pt1 = ( 0, 24 );
    my @pt2 = ( 16, 24 ); 
    my @pa = ( \@pt1, \@pt2 );
    oa::oaLine::create( $block,  
			$globs{ 'num_layer_dev' },  
			$globs{ 'num_purpose_dev' }, 
			\@pa );

    # Rethe = Y values of pa1 to match those of the bottom line of the symbol
    @pt1 = ( 0, 0 );
    @pt2 = ( 16, 0 );
    @pa = ( \@pt1, \@pt2);

    # Create another Line reusing pa1
    oa::oaLine::create( $block,
			$globs{ 'num_layer_dev' }, 
			$globs{ 'num_purpose_dev' },
			\@pa );

    # Use the make_label function to put the cell's name at the location {2,2}
    my @pt = ( 2, 2 );
    &make_label( $block, \@pt );

    &save_close_design( $design );
}


sub make_xor_symbol() {
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    my $label = "Xor";
    my $design = make_sch_sym_view( $label );

    # make a crude XOR symbol that looks just like the OR, moved to the right 6
    # with an extra copy of the arc on the left at the Y-axis position.
    #
    #    0,24                     22,24
    #       +  .  +---------------+ .
    #            +     +               +
    #               +     +               +
    #                 +     +               +
    #                  +     +               +
    #                   +     +               +
    #         12,12 --> o     o <--18,12      o <--- 34,12
    #                   +     +               +
    #                  +     +               +
    #                 +     +               +
    #               +     +               +
    #           .+    .+              .+
    #       +     +---------------+
    #     0,0                     22,0


    # Open the symbol Design of the Or just create above (in read mode).
    my $mode =        $globs{ 'mode_read' };
    my $view_type =   oa::oaViewType::get( $oa::oacNetlist );
    my $scname_cell = new oa::oaScalarName( $ns, "Or" );



    my $design_or =   oa::oaDesign::open( $scname_lib_symbol, 
					  $scname_cell, 
					  $scname_view_symbol, 
					  $view_type, 
					  $mode );

    # Create an iterator for all the Shapes in the Or's Block
    my$block = $design_or->getTopBlock();

    my $saved_arc  = '';
    my $saved_text = '';

    # Create a loop that retrieves the next Shape.
    my $itIter = new  oa::oaIter::oaShape($block->getShapes());
    while (my $shape = $itIter->getNext()) {
	my (@trans, $copied_shape, $shape_type);
        # Copy each shape to this Xor Design, moving it to the right by 6,
        # saving the handle of the newly copied Shape in copied_shape.
        @trans = ( 6, 0, $oa::oacR0 );
        $copied_shape = $shape->copy( \@trans, $design );

	$shape_type = $copied_shape->getType();
        # If that shape is an oaArc, then save a pointer to it in saved_arc.

	if ( $shape_type == $oa::oacArcType ) {
	    $saved_arc = $copied_shape;
	}
        # If that shape is the oaText (label) save a pointer to it in saved_text
	if ( $shape_type == $oa::oacTextType ) {
	    $saved_text = $copied_shape;
	}
	    
    }

    # At this point the new Xor Design Block has a copy of the symbol
    # shapes created for the Or, but moved to the right by 6, and
    #    - saved_arc is one of the arcs
    #    - saved_text is the Text label

    my @new_arc_box = ( -12, 0, 12, 24 );

    # make one more arc in the Xor Design Block by copying that saved_arc, assigning
    # it to the new_arc variable (the position of it doesn't matter, it will be re). =
    my @trans2 = ( 0, 0, $oa::oacR0 );
    my $new_arc = $saved_arc->copy( \@trans2 );

    # Rethe = Box of new_arc to new_arc_box and the start/stop angles to the same angles
    # obtained from that original saved_arc.
    $saved_arc->set( \@new_arc_box, $saved_arc->getStartAngle(), $saved_arc->getStopAngle() );

    # An oaText label was just copied (with the other Shapes) from the Or Cell,
    # so instead of using make_label to add another one, just change the value
    # of that existing oaText (saved from the loop above) from the lib/cell/view
    # name of the Or to that of the Xor.
    $saved_text->setText( get_cell_name( $design ) );

    &save_close_design( $design );
}




1;
