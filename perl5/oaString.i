/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%ignore OpenAccess_4::oaString;

%typemap(in) OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
{
    $1 = &tmpString;
}

%typemap(in) const OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
{
    if (SvPOK($input))
	tmpString = SvPV($input, PL_na);
    $1 = &tmpString;
}

%typecheck(0) OpenAccess_4::oaString&
{
    $1 = ($input == &PL_sv_undef) ? 0 : 1;
}

%typecheck(0) const OpenAccess_4::oaString&
{
    $1 = SvPOK($input) ? 1 : 0;
}

%typemap(argout) OpenAccess_4::oaString&
{
    sv_setpv($input, (const oaChar*) *$1);
}

%typemap(argout) const OpenAccess_4::oaString&
{
}

%typemap(out) OpenAccess_4::oaString&
{
    $result = sv_2mortal(newSVpv((const oaChar*) *$1, 0));
    argvi++;
}

%typemap(out) OpenAccess_4::oaString
{
    $result = sv_2mortal(newSVpv((const oaChar*) $1, 0));
    argvi++;
}
