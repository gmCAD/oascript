/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%header %{

#include <ext/hash_map>
#include <queue>
#include <oa/oaDesignDB.h>

namespace {
struct StrEq {
    bool operator()(char const *__x, char const *__y) const { return !strcmp(__x, __y); }
};
}

typedef __gnu_cxx::hash_map<const char*, int, __gnu_cxx::hash<char const *>, StrEq> TypeInfoMap;
static TypeInfoMap typeinfo_map;
static swig_type_info *typeinfo_lookup[oavNumObjectTypes];

static swig_type_info*
oaObject_dcast (void **obj)
{
    if (!*obj)
	return NULL;
    oa::oaObject *oaobj = reinterpret_cast<oa::oaObject*>(*obj);
    oa::oaTypeEnum objtype = oaobj->getType();
    return typeinfo_lookup[objtype];
}

static bool use_isvalid;

static void init_use_isvalid () __attribute__((__constructor__));

void
init_use_isvalid ()
{
    use_isvalid = true;
    char *val = getenv("OASCRIPT_USE_ISVALID");
    if (val && strlen(val) && !strcmp(val, "0")) {
        use_isvalid = false;
    }
}

%}

%typemap(in, noblock=1) OpenAccess_4::oaObject *(void  *argp = 0, int res = 0) {
  res = SWIG_ConvertPtr($input, &argp,$descriptor, $disown | %convertptr_flags);
  if (!SWIG_IsOK(res)) { 
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  $1 = %reinterpret_cast(argp, $ltype);
  if (!$1) {
    SWIG_exception_fail(SWIG_ValueError, "in method '$symname', argument $argnum of type '$type' is NULL.");
  } else if (use_isvalid && !$1->isValid()) {
    SWIG_exception_fail(SWIG_ValueError, "in method '$symname', argument $argnum of type '$type' is not a valid oaObject (isValid() returned false).");
  }
}

#undef OAOBJECT
%define OAOBJECT(typename)
%apply OpenAccess_4::oaObject * { typename * };
%enddef

%include <macros/oa_objects.i>

%typemap(out,noblock=1) SWIGTYPE* = SWIGTYPE *DYNAMIC;
%typemap(out,noblock=1) SWIGTYPE& = SWIGTYPE &DYNAMIC;
