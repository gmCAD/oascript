##########################################################################
#
# Copyright 2010 Si2, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##########################################################################
#
# This include has the labs, cleanlabs, and test targets.
#
#  make labs      -- makes the labs but does not clean them first, 
#                    nor does it check them
#  make test      -- runs target-language-specific tests;
#                    cleans the labs, runs, and checks them
#  make cleanlabs -- cleans the labs
#               
# Here are the vars that must be set before you include this:
#
#  LABS         - the set of directories you want to drop down into.
#                 for example, 'perl5 python2.5 ruby 1.8 tcl8.4' if you're at
#                 at the top the scripting tree, 
#                 'labs' if you're in one of the target language
#                 directories such as perl5/ or tcl8.4/,
#                 'sample netlist oadump symbol' if you're in labs/
#  OAS_ROOT     - the ABSOLUTE path to the top of the OA Scripting tree.
#  TEST_TARGETS --> Optional, for additional targets for testing
#
#   e.g., TEST_TARGETS := test_basic test_unit
#     or, TEST_TARGETS :=


include $(OAS_ROOT)/checklog.mk


.PHONY: labs


labs :
	@for lab in $(LABS) ; do \
	  $(MAKE) labs -j 1 -C $$lab ; \
	done

.PHONY: cleanlabs

cleanlabs :
	@for lab in $(LABS) ; do \
	  $(MAKE) -C $$lab cleanlabs; \
	done
	rm -f $(LAB_LOGFILE)

.PHONY: labtest test

test: labtest $(TEST_TARGETS) $(CHECKLOG)

labtest :
	@for lab in $(LABS) ; do \
		echo "Making lab $$lab"; \
	  $(MAKE) test -k -j 1 -C $$lab ; \
	done

#------- eof ------

