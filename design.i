/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef SWIGPERL
%module "oa::design";
#endif

#ifdef SWIGRUBY
%module "oa::design";
#endif

#ifdef SWIGTCL
%module "design";
#endif

#ifdef SWIGPYTHON
%module "design";
#endif

#ifdef SWIGCSHARP
%module "oasCSharpDesign";
#endif

%{

#include <oa/oaDesignDB.h>

USE_OA_NAMESPACE
USE_OA_COMMON_NAMESPACE
USE_OA_PLUGIN_NAMESPACE

oaNameSpace* getDefaultNameSpace();

BEGIN_OA_NAMESPACE

static void
oaDesignInitProxy(oaUInt4    apiMajorRev = oacAPIMajorRevNumber,
		  oaUInt4    apiMinorRev = oacAPIMinorRevNumber)
{
    oaDesignInit(apiMajorRev, apiMinorRev);
}

static void
oaDesignInitProxy(oaUInt4    apiMajorRev,
		  oaUInt4    apiMinorRev,
		  oaUInt4    dataModelRev)
{
    oaDesignInit(apiMajorRev, apiMinorRev, dataModelRev);
}

END_OA_NAMESPACE

// Call oaDesignInit automatically to avoid SEGV if the user forgets.
// The user can choose to NOT call this in the event that an older OA DM
// model is desired by setting $OASCRIPT_USE_INIT = 0.  Scripting languages
// execute this via SWIG_init(), C# via "%pragma(csharp) imclasscode".
static void
oasDesignInit()
{
  char *useinit = getenv("OASCRIPT_USE_INIT");
    if (!useinit || strcmp(useinit, "0")) {
      OpenAccess_4::oaDesignInitProxy();
	}
}
%}

%ignore OpenAccess_4::oaDesignInit;

// Basic agnostic setup
%include "oa.i"

// Optional target-language-specific code:
%include <target_design_pre.i>
%include <generic/extensions/tech/oaUUPoint.i>
%include <generic/extensions/tech/oaUUVector.i>
%include <generic/extensions/tech/oaUUSegment.i>
%include <generic/extensions/tech/oaUUBox.i>



#if !defined(oaDesignDB_P)
#define oaDesignDB_P



// *****************************************************************************
// Public Header Includes
// *****************************************************************************
%import "tech.i"

%include "oa/oaDesignDBTypes.h"

%include "oa/oaAntennaData.h"
%include "oa/oaDesignModTypes.h"
%include "oa/oaDesignMsgs.h"
%include "oa/oaDesignException.h"
%include "oa/oaDesignObject.h"
%include "oa/oaBlockObject.h"
%include "oa/oaDesignCollection.h"
%include "oa/oaPhysCollection.h"
%include "oa/oaConnCollection.h"
%include "oa/oaAttrType.h"
%include "oa/oaSigType.h"
%include "oa/oaSource.h"
%include "oa/oaFig.h"
%include "oa/oaRefHeader.h"
%include "oa/oaRef.h"
%include "oa/oaViaHeader.h"
%include "oa/oaVia.h"
%include "oa/oaShape.h"
%include "oa/oaNet.h"
%include "oa/oaTerm.h"
%include "oa/oaDesign.h"
%include "oa/oaBlock.h"
%include "oa/oaScalarNet.h"
%include "oa/oaBusNet.h"
%include "oa/oaBusNetBit.h"
%include "oa/oaBusNetDef.h"
%include "oa/oaBundleNet.h"
%include "oa/oaScalarTerm.h"
%include "oa/oaBusTerm.h"
%include "oa/oaBusTermBit.h"
%include "oa/oaBusTermDef.h"
%include "oa/oaBundleTerm.h"
%include "oa/oaLPPHeader.h"
%include "oa/oaGuide.h"
%include "oa/oaLayerHeader.h"
%include "oa/oaTextAlign.h"
%include "oa/oaFont.h"
%include "oa/oaInstHeader.h"
%include "oa/oaInst.h"
%include "oa/oaRowHeader.h"
%include "oa/oaRow.h"
%include "oa/oaScalarInst.h"
%include "oa/oaArrayInst.h"
%include "oa/oaVectorInst.h"
%include "oa/oaVectorInstBit.h"
%include "oa/oaVectorInstDef.h"
%include "oa/oaRect.h"
%include "oa/oaPolygon.h"
%include "oa/oaPath.h"
%include "oa/oaSegStyle.h"
%include "oa/oaPathSeg.h"
%include "oa/oaDonut.h"
%include "oa/oaDot.h"
%include "oa/oaEllipse.h"
%include "oa/oaArc.h"
%include "oa/oaLine.h"
%include "oa/oaText.h"
%include "oa/oaTextDisplay.h"
%include "oa/oaRoute.h"
%include "oa/oaNetTermPair.h"
%include "oa/oaNetTermArray.h"
%include "oa/oaInstTerm.h"
%include "oa/oaAssignmentDef.h"
%include "oa/oaAssignment.h"
%include "oa/oaConnectDef.h"
%include "oa/oaHierPath.h"
%include "oa/oaPin.h"
%include "oa/oaTrackPattern.h"
%include "oa/oaMarker.h"
%include "oa/oaCluster.h"
%include "oa/oaBoundary.h"
%include "oa/oaBlockage.h"
%include "oa/oaScanChainSet.h"
%include "oa/oaScanChain.h"
%include "oa/oaScanChainInst.h"
%include "oa/oaSteiner.h"
%include "oa/oaCMap.h"
%include "oa/oaGCellPattern.h"
%include "oa/oaAnalysisPoint.h"
%include "oa/oaAnalysisOpPoint.h"
%include "oa/oaOpPointHeader.h"
%include "oa/oaReducedModel.h"
%include "oa/oaPoleResidue.h"
%include "oa/oaLumpedElmore.h"
%include "oa/oaPiElmore.h"
%include "oa/oaPiPoleResidue.h"
%include "oa/oaElmore.h"
%include "oa/oaNode.h"
%include "oa/oaGroundedNode.h"
%include "oa/oaDevice.h"
%include "oa/oaStdDevice.h"
%include "oa/oaResistor.h"
%include "oa/oaCouplingCap.h"
%include "oa/oaInductor.h"
%include "oa/oaDiode.h"
%include "oa/oaSeriesRL.h"
%include "oa/oaMutualInductor.h"
%include "oa/oaParasiticNetwork.h"
%include "oa/oaSubNetwork.h"
%include "oa/oaRouteOptimizer.h"
%include "oa/oaModObject.h"
%include "oa/oaModule.h"
%include "oa/oaModInst.h"
%include "oa/oaModDesignInst.h"
%include "oa/oaModScalarInst.h"
%include "oa/oaModVectorInst.h"
%include "oa/oaModVectorInstBit.h"
%include "oa/oaModVectorInstDef.h"
%include "oa/oaModInstHeader.h"
%include "oa/oaModModuleInstHeader.h"
%include "oa/oaModNet.h"
%include "oa/oaModBundleNet.h"
%include "oa/oaModBusNet.h"
%include "oa/oaModBusNetBit.h"
%include "oa/oaModScalarNet.h"
%include "oa/oaModBusNetDef.h"
%include "oa/oaModInstTerm.h"
%include "oa/oaModTerm.h"
%include "oa/oaModBundleTerm.h"
%include "oa/oaModBusTerm.h"
%include "oa/oaModBusTermBit.h"
%include "oa/oaModScalarTerm.h"
%include "oa/oaModBusTermDef.h"
%include "oa/oaModConnectDef.h"
%include "oa/oaModAssignment.h"
%include "oa/oaModModuleInst.h"
%include "oa/oaModModuleScalarInst.h"
%include "oa/oaModModuleVectorInst.h"
%include "oa/oaModModuleVectorInstBit.h"
%include "oa/oaOccObject.h"
%include "oa/oaOccurrence.h"
%include "oa/oaOccInst.h"
%include "oa/oaOccDesignInst.h"
%include "oa/oaOccScalarInst.h"
%include "oa/oaOccVectorInst.h"
%include "oa/oaOccVectorInstBit.h"
%include "oa/oaOccVectorInstDef.h"
%include "oa/oaOccInstHeader.h"
%include "oa/oaOccModuleInstHeader.h"
%include "oa/oaOccNet.h"
%include "oa/oaOccBundleNet.h"
%include "oa/oaOccBusNet.h"
%include "oa/oaOccBusNetBit.h"
%include "oa/oaOccScalarNet.h"
%include "oa/oaOccBusNetDef.h"
%include "oa/oaOccInstTerm.h"
%include "oa/oaOccTerm.h"
%include "oa/oaOccBundleTerm.h"
%include "oa/oaOccBusTerm.h"
%include "oa/oaOccBusTermBit.h"
%include "oa/oaOccScalarTerm.h"
%include "oa/oaOccBusTermDef.h"
%include "oa/oaOccConnectDef.h"
%include "oa/oaOccAssignment.h"
%include "oa/oaOccArrayInst.h"
%include "oa/oaOccModuleInst.h"
%include "oa/oaOccModuleScalarInst.h"
%include "oa/oaOccModuleVectorInst.h"
%include "oa/oaOccModuleVectorInstBit.h"
%include "oa/oaOccTraverser.h"
%include "oa/oaDesignInterfaces.h"
%include "oa/oaEvalTextLink.h"
%include "oa/oaPcellLink.h"
%include "oa/oaScriptEngineLink.h"
%include "oa/oaOccShape.h"
%include "oa/oaFigGroup.h"
%include "oa/oaTextLink.h"
%include "oa/oaDesignTraits.h"
%include "oa/oaDesignObserver.h"
%include "oa/oaModObserver.h"
%include "oa/oaOccObserver.h"
%include "oa/oaRegionQuery.h"

#endif

BEGIN_OA_NAMESPACE

#undef oaDesignInit
%rename(oaDesignInit) oaDesignInitProxy;

static void
oaDesignInitProxy(oaUInt4    apiMajorRev = oacAPIMajorRevNumber,
		  oaUInt4    apiMinorRev = oacAPIMinorRevNumber);

static void
oaDesignInitProxy(oaUInt4    apiMajorRev,
		  oaUInt4    apiMinorRev,
		  oaUInt4    dataModelRev);

END_OA_NAMESPACE

// Optional target-language-specific code:
%include <target_design_post.i>

%include <generic/extensions/design.i>

#if !defined(SWIGCSHARP)
%init %{
  // Call oaDesignInit automatically to avoid SEGV if the user forgets.
  // The user can choose to NOT call this in the event that an older OA DM
  // model is desired by setting $OASCRIPT_USE_INIT = 0.  Scripting languages
  // execute this via SWIG_init(), C# via "%pragma(csharp) imclasscode".
  oasDesignInit();
%}
#endif

