/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     08/13/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */


%header %{

  int
    langobj_to_oaTransform (LANG_VALUE_TYPE langTransform, oaTransform& transform)
  {
    void *argp;

    if (LANG_ARRAYP(langTransform) && LANG_ARRAY_SIZE(langTransform) == 3) {

      LANG_VALUE_TYPE langElt = LANG_ARRAY_INDEX(langTransform, 0);
      oaInt4 intVal = (oaInt4) LANG_NUM_TO_LONG(langElt);
      transform.xOffset() = intVal;

      langElt = LANG_ARRAY_INDEX(langTransform, 1);
      intVal = (oaInt4) LANG_NUM_TO_LONG(langElt);
      transform.yOffset() = intVal;

      langElt = LANG_ARRAY_INDEX(langTransform, 2);
      if (SWIG_IsOK(SWIG_ConvertPtr(langElt, &argp, SWIGTYPE_p_OpenAccess_4__oaOrient, 0)) && argp != NULL) {
        transform.orient() = *(reinterpret_cast<oaOrient *>(argp));
      } else if (LANG_INTP(langElt)) {
        intVal = (oaInt4) LANG_NUM_TO_LONG(langElt);
        transform.orient() = oaOrient((oaOrientEnum) intVal);
      } else if (LANG_SYMBOLP(langElt)) {
        LANG_STR_TYPE langstr = LANG_SYM_TO_STR(langElt);
        OpenAccess_4::oaString tmpString;
        tmpString = (const oaChar*)LANG_STR_TO_CSTR(langstr);
        LANG_OA_TRY_RESUME(transform.orient() = oaOrient(tmpString), SWIG_fail)
      } else {
        SWIG_fail;
      }

      return SWIG_OK;
    }

    if (SWIG_IsOK(SWIG_ConvertPtr(langTransform, &argp, SWIGTYPE_p_OpenAccess_4__oaTransform,  0)) && argp != NULL) {
      transform = *(reinterpret_cast<oaTransform*>(argp));
      return SWIG_OK;
    }
    
  fail:
    return SWIG_ERROR;
  }


%}

%typemap(in) const OpenAccess_4::oaTransform& (OpenAccess_4::oaTransform tmpTransform)
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = langobj_to_oaTransform($input, tmpTransform))))
	$1 = &tmpTransform;
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))) && argp != NULL)
	$1 = reinterpret_cast<$1_basetype *>(argp);
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typemap(in) OpenAccess_4::oaTransform
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = langobj_to_oaTransform($input, $1)))) {}
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))) && argp != NULL)
	$1 = *(reinterpret_cast<$1_basetype *>(argp));
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typecheck(0) const OpenAccess_4::oaTransform&
{
  void *tmpVoidPtr;
  $1 = ((LANG_ARRAYP($input) && LANG_ARRAY_SIZE($input) == 3) ||
        SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0)));
}

%typecheck(0) OpenAccess_4::oaTransform = (const OpenAccess_4::oaTransform&);

