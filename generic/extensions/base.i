/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%include <generic/extensions/macros.i>

GETSTRING(oaAppDef, getName);
GETSTRING(oaAppObjectDef, getName);

CONSTRUCTFROMSTRING(oaBundleName);
GETSTRINGWITHNS(oaBundleName, get);
INITNAME(oaBundleName);

GETSTRING(oaConstraintDef, getName);
GETSTRING(oaConstraintGroupDef, getName);
GETSTRING(oaConstraintGroup, getName);
GETSTRING(oaConstraintGroupHeader, getName);
GETSTRING(oaConstraintParamDef, getName);
GETSTRING(oaFeature, getName);
GETSTRING(oaFSComponent, getFullName);
GETSTRING(oaGroupDef, getName);
GETSTRING(oaGroup, getName);

CONSTRUCTFROMSTRING(oaName);
GETSTRINGWITHNS(oaName, get);
INITNAME(oaName);

GETSTRING(oaParam, getName);
GETSTRING(oaProp, getName);
GETSTRING(oaStringProp, getValue);

CONSTRUCTFROMSTRING(oaScalarName);
GETSTRINGWITHNS(oaScalarName, get);
INITNAME(oaScalarName);

CONSTRUCTFROMSTRING(oaSimpleName);
GETSTRINGWITHNS(oaSimpleName, get);
INITNAME(oaSimpleName);

CONSTRUCTFROMSTRING(oaVectorBitName);
GETSTRINGWITHNS(oaVectorBitName, get);
GETSTRINGWITHNS(oaVectorBitName, getBaseName);
INITNAMEWITHBASEONLYOPTION(oaVectorBitName);

CONSTRUCTFROMSTRING(oaVectorName);
GETSTRINGWITHNS(oaVectorName, get);
GETSTRINGWITHNS(oaVectorName, getBaseName);
INITNAMEWITHBASEONLYOPTION(oaVectorName);

GETBOX(oaPointArray, getBBox);
GETPOINT(oaBox, getCenter);

// One of a kind method for oaName
%extend OpenAccess_4::oaName {
        oaString getBitName(const oaNameSpace& ns, oaUInt4 bit) const
        {
                oaString result;
                self->getBitName(ns, bit, result);
                return result;
        }
};

// oaNameSpace static functions to support default namespace
%extend OpenAccess_4::oaNameSpace {
        static void push(oaNameSpace* ns)
        {
                sDefaultNameSpaceManager.push(ns);
        }

        static void pop()
        {
                sDefaultNameSpaceManager.pop();
        }

        static oaNameSpace* getDefault()
        {
                return sDefaultNameSpaceManager.top();
        }
		
		static bool isEmpty()
        {
            return sDefaultNameSpaceManager.isEmpty();
        }

        static void clear()
        {
			sDefaultNameSpaceManager.clear();
        }

};

%extend OpenAccess_4::oaObject {
    static OpenAccess_4::oaObject* fromOaTcl(const char* str)
    {
        if (!str) {
            return 0;
        }
        OpenAccess_4::oaUIntPtr ptr;
        sscanf(str, "oa:%llx", &ptr);
        OpenAccess_4::oaObject* result = (OpenAccess_4::oaObject*)(ptr);
        if (!result->isValid()) {
            return 0;
        }
        return result;
    }

    oaString toOaTcl()
    {
        char buf[100];
        OpenAccess_4::oaUIntPtr ptr = (OpenAccess_4::oaUIntPtr) self;
        snprintf(buf, 100, "oa:0x%llx", ptr);
        return oaString(buf);
    }

    // Define an equality operator so that in the scripting
    // language it is the oaObject* pointers that are compared, not the
    // the scripting language references.
    // We cannot use operator==() here because we are not really within
    // a class.
    bool isEqual(OpenAccess_4::oaObject* rhs)
    {
        return self == rhs;
    }
    unsigned long getHashCode()
    {
        return (unsigned long) self;
    }
};

// Define missing oaCollection<oaAppDef, oaSession>::includes()
// and oaCollection<oaAppObjectDef, oaSession>::includes().
// TODO: Remove the oaCollection<>::includes() workaround once OpenAccess bug 1331 is fixed.
%extend OpenAccess_4::oaCollection<OpenAccess_4::oaAppDef, OpenAccess_4::oaSession> {
        // Define missing includes()
		oaBoolean includes(const oaAppDef *o)
        {
			// Avoid VS warning C4700
			if (!o) return false;
			throw oaError(oacCannotBeCalled, "oaCollection<oaAppDef, oaSession>::includes() per OA bug 1331.");
            return false;
        }
};
%extend OpenAccess_4::oaCollection<OpenAccess_4::oaAppObjectDef, OpenAccess_4::oaSession> {
        // Define missing includes()
		oaBoolean includes(const oaAppObjectDef *o)
        {
			// Avoid VS warning C4700
			if (!o) return false;
			throw oaError(oacCannotBeCalled, "oaCollection<oaAppObjectDef, oaSession>::includes() per OA bug 1331.");
            return false;
        }
};
