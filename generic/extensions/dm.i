/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%include <generic/extensions/macros.i>

GETSTRINGWITHNS(oaCell, getName);
GETSTRINGWITHNS(oaCellDMData, getCellName);
GETSTRINGWITHNS(oaCellViewDMData, getCellName);
GETSTRINGWITHNS(oaCellViewDMData, getViewName);
GETSTRINGWITHNS(oaDMData, getLibName);
GETSTRINGWITHNS(oaViewDMData, getViewName);
GETSTRING(oaDMFile, getName);
GETSTRING(oaDMFile, getPath);
GETSTRING(oaLibDef, getLibPath);
GETSTRING(oaLibDef, getLibWritePath);
GETSCALARNAME(oaLibDef, getLibName);
STATICGETSTRING(oaLibDefList, getDefaultPath);
STATICGETSTRING(oaLibDefList, getDefaultFileName);
GETSTRING(oaLibDefList, getFullPath);
GETSTRING(oaLibDefList, getPath);
GETSTRING(oaLibDefListRef, getRefListPath);
GETSTRINGWITHNS(oaLib, getName);
GETSTRING(oaLib, getPath);
GETSTRING(oaLib, getFullPath);
GETSTRING(oaLib, getWritePath);
GETSTRINGWITHNS(oaView, getName);
GETSTRING(oaViewType, getName);

