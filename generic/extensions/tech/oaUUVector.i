/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%inline %{

class oaUUVector
{
public:
    oaUUVector(const OpenAccess_4::oaVector& vector,
               OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mVector(vector)
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUVector(OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mVector()
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUVector(OpenAccess_4::oaDouble xValIn, OpenAccess_4::oaDouble yValIn,
               OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mVector(tech->uuToDBU(viewType, xValIn), tech->uuToDBU(viewType, yValIn))
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUVector(const oaUUPoint& head,
               OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mVector(head.getPoint())
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUVector(const oaUUPoint& tail, const oaUUPoint& head,
               OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mVector(tail.getPoint(), head.getPoint())
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    ~oaUUVector()
    {
    }

    OpenAccess_4::oaDouble x() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mVector.x());
    }

    OpenAccess_4::oaDouble y() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mVector.y());
    }

    void set(const oaUUPoint& tail, const oaUUPoint& head)
    {
        mVector.set(tail.getPoint(), head.getPoint());
    }

    void setLength(const OpenAccess_4::oaDouble length)
    {
        checkTech();
        mVector.setLength(mTech->uuToDBU(mViewType, length));
    }

    OpenAccess_4::oaDouble getLength() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mVector.getLength());
    }

    OpenAccess_4::oaBoolean leftOf(const oaUUVector& ref) const
    {
        return mVector.leftOf(ref.getVector());
    }

    OpenAccess_4::oaBoolean rightOf(const oaUUVector& ref) const
    {
        return mVector.rightOf(ref.getVector());
    }

    void rotate90()
    {
        mVector.rotate90();
    }

    void rotate180()
    {
        mVector.rotate180();
    }

    void rotate270()
    {
        mVector.rotate270();
    }

    void addToPoint(const oaUUPoint& start, oaUUPoint& result) const
    {
        mVector.addToPoint(start.getPoint(), result.getPoint());
    }

    void subFromPoint(const oaUUPoint& start, oaUUPoint& result) const
    {
        mVector.subFromPoint(start.getPoint(), result.getPoint());
    }

    OpenAccess_4::oaBoolean intersects(const oaUUPoint& ref, const oaUUVector& other,
            const oaUUPoint& otherRef, oaUUPoint& iPt)
    {
        return mVector.intersects(ref.getPoint(), other.getVector(),
                                  otherRef.getPoint(), iPt.getPoint());
                
    }

    OpenAccess_4::oaDouble operator*(const oaUUVector& v) const
    {
        checkTech();
        double scale = mTech->getDBUPerUU(mViewType);
        return (mVector * v.getVector()) / (scale * scale);
    }

    void normalize()
    {
        mVector.normalize();
    }

    OpenAccess_4::oaDouble getCosAngle(const oaUUVector& v) const
    {
        return mVector.getCosAngle(v.getVector());
    }

    const OpenAccess_4::oaVector& getVector() const
    {
        return mVector;
    }

    OpenAccess_4::oaTech* getTech() const
    {
        return mTech;
    }

    OpenAccess_4::oaViewType* getViewType() const
    {
        return mViewType;
    }

private:
    void checkTech() const
    {
        if (!mTech->isValid()) {
            // make this into an exception!
            printf("invalid tech!\n");
        }
    }

private:
    OpenAccess_4::oaVector mVector;
    OpenAccess_4::oaTech* mTech;
    OpenAccess_4::oaViewType* mViewType;
};

%}

