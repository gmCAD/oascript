/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef SWIGPERL
%module "oa::plugin";
#endif

#ifdef SWIGRUBY
%module "oa::plugin";
#endif

#ifdef SWIGTCL
%module "plugin";
#endif

#ifdef SWIGPYTHON
%module "plugin";
#endif

#ifdef SWIGCSHARP
%module "oasCSharpPlugIn";
#endif

%{

#include <oa/oaDesignDB.h>

USE_OA_NAMESPACE
USE_OA_COMMON_NAMESPACE
USE_OA_PLUGIN_NAMESPACE

#undef SWIG_SHADOW
#define SWIG_SHADOW 0

oaNameSpace* getDefaultNameSpace();

%}

// Basic agnostic setup
%include "oa.i"

// Optional target-language-specific code:
%include <target_plugin_pre.i>



#if !defined(oaPlugIn_P)
#define oaPlugIn_P



// *****************************************************************************
// Windows DLL export macros
// *****************************************************************************
#define OA_PLUGIN_DLL_API
#define OA_PLUGIN_DLL_PVT



// *****************************************************************************
// Define the namespace macros for oaPlugIn
// *****************************************************************************
#define BEGIN_OA_PLUGIN_NAMESPACE namespace oaPlugIn {
#define END_OA_PLUGIN_NAMESPACE }
#define USE_OA_PLUGIN_NAMESPACE using namespace oaPlugIn;



// *****************************************************************************
// System header files used by plugIn
// *****************************************************************************
#include <sstream>



// *****************************************************************************
// oaCommon includes.
// *****************************************************************************
%import "common.i"


// *****************************************************************************
// Import namespace oaCommon into namespace oaPlugIn
// *****************************************************************************
BEGIN_OA_PLUGIN_NAMESPACE

USE_OA_COMMON_NAMESPACE

END_OA_PLUGIN_NAMESPACE



// *****************************************************************************
// oaPlugIn includes.
// *****************************************************************************
%include "oa/oaPlugInDMTypes.h"

// This macro causes swig to spin forever in oaPlugInDMObject.h
#undef USE_OA_PLUGIN_NAMESPACE
#define USE_OA_PLUGIN_NAMESPACE

// These seem to be unused declarations in oaPlugInIDMObject.h
// See OpenAccess bug #1253
%ignore IID_ILibDefList;
%ignore IID_ILibDefListMem;
%ignore ILibDefList;
%ignore ILibDefListMem;

%include "oa/oaPlugInIDMObject.h"
%include "oa/oaPlugInDMInterfaces.h"
%include "oa/oaPlugInVCInterfaces.h"
%include "oa/oaPlugInScriptInterfaces.h"
%include "oa/oaPlugInValue.h"



// *****************************************************************************
// oaPlugIn Inline Includes.
// *****************************************************************************
%include "oa/oaPlugInIDMObject.inl"
%include "oa/oaPlugInDMInterfaces.inl"
%include "oa/oaPlugInVCInterfaces.inl"
%include "oa/oaPlugInScriptInterfaces.inl"
%include "oa/oaPlugInValue.inl"

#endif

// Optional target-language-specific code:
%include <target_plugin_post.i>

%include <generic/extensions/plugin.i>

