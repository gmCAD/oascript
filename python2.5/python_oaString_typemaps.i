/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%ignore OpenAccess_4::oaString::format;

%header %{
static int
oaString_tp_print (PyObject *obj, FILE *f, int i)
{
    void *ptr;
    if (!SWIG_IsOK(SWIG_Python_ConvertPtrAndOwn(obj, &ptr, SWIGTYPE_p_OpenAccess_4__oaString, 0, 0))) {
	PyErr_SetString(PyExc_RuntimeError, "In oa.oaString.__print__, couldn't decode oaString object.");
	return -1;
    }
    oaString *s = reinterpret_cast<oaString*>(ptr);
    fputs((const oaChar*) *s, f);
    return 0;
}
%}

%feature("python:tp_print", "&oaString_tp_print") OpenAccess_4::oaString;

%typemap(in) const OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
{
    int res;
    void *argp;
    
    if (PyString_Check($input)) {
	tmpString = PyString_AsString($input);
	$1 = &tmpString;
    } else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0)))) {
	$1 = reinterpret_cast<$1_basetype *>(argp);
    } else {
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
    }
}

%typecheck(0) const OpenAccess_4::oaString&
{
    $1 = PyString_Check($input) || SWIG_IsOK(SWIG_ConvertPtr($input, NULL, $1_descriptor,  0));
}

