/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


%header %{

PyObject*
oaBox_to_tuple (const oaBox& box)
{
    PyObject *result = PyTuple_New(4);
    assert(result);
    PyTuple_SET_ITEM(result, 0, PyInt_FromLong((long) box.left()));
    PyTuple_SET_ITEM(result, 1, PyInt_FromLong((long) box.bottom()));
    PyTuple_SET_ITEM(result, 2, PyInt_FromLong((long) box.right()));
    PyTuple_SET_ITEM(result, 3, PyInt_FromLong((long) box.top()));
    return result;
}

static int
oaBox_tp_print (PyObject *obj, FILE *f, int i)
{
    void *ptr;
    if (!SWIG_IsOK(SWIG_Python_ConvertPtrAndOwn(obj, &ptr, SWIGTYPE_p_OpenAccess_4__oaBox, 0, 0))) {
      PyErr_SetString(PyExc_RuntimeError, "In oa.oaBox.__print__, couldn't decode oaBox object.");
      return -1;
    }
    oaBox *b = reinterpret_cast<oaBox*>(ptr);
    if (!b)
    return -1;
    PyObject *tuple = oaBox_to_tuple(*b);
    int result = PyObject_Print(tuple, f, i);
    Py_DECREF(tuple);
    return result;
}

%}

%feature("python:tp_print", "&oaBox_tp_print") OpenAccess_4::oaBox;

%typemap(in) const OpenAccess_4::oaBox& (OpenAccess_4::oaBox tmpBox)
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = langobj_to_oaBox($input, tmpBox))))
	$1 = &tmpBox;
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))))
	$1 = reinterpret_cast<$1_basetype *>(argp);
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typemap(in) OpenAccess_4::oaBox
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = langobj_to_oaBox($input, $1)))) {}
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))))
	$1 = *(reinterpret_cast<$1_basetype *>(argp));
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typecheck(0) (const OpenAccess_4::oaBox&)
{
    $1 = ((PyTuple_Check($input) && PyTuple_GET_SIZE($input) == 4) ||
	  SWIG_IsOK(SWIG_ConvertPtr($input, NULL, $1_descriptor, 0)));
}

%typecheck(0) (OpenAccess_4::oaBox) = (const OpenAccess_4::oaBox&);

