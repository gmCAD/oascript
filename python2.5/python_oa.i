/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


%header %{

#define LANG_APPEND_OUTPUT(result, obj) (SWIG_Python_AppendOutput(result, obj))

#define LANG_NULLP(obj) (obj == Py_None)
#define LANG_ARRAYP(obj) PyTuple_Check(obj)
#define LANG_INTP(num) (! (PyInt_AsLong(num) == -1 && PyErr_Occurred()))
#define LANG_ARRAY_SIZE(arr) PyTuple_GET_SIZE(arr)
#define LANG_ARRAY_INDEX(arr, index) PyTuple_GET_ITEM(arr, index)
#define LANG_NUM_TO_LONG(num) PyInt_AsLong(num)
#define LANG_LONG_TO_NUM() PyInt_AsLong(num)
#define LANG_NUM_TO_INT(langobj) PyInt_AsLong(langobj)
#define LANG_INT_TO_NUM(cppint) PyInt_FromLong(cppint)
#define LANG_VALUE_TYPE PyObject*
#define LANG_STR_TYPE PyObject*
#define LANG_STRINGP(input) PyString_Check(input)
#define LANG_STR_TO_CSTR(langstr) PyString_AsString(langstr)

// Use a string in place of symbol for Python (no symbol object in Python)
#define LANG_SYMBOLP(sym) (PyString_Check(sym))
#define LANG_SYM_TO_STR(sym) sym

#define LANG_THROW_OAEXCP(e) char *msg = (char*) alloca(e.getMsg().getLength() + 64);\
  sprintf(msg, "OpenAccessException (%d) : %s", e.getMsgId(), (const oaChar*) e.getMsg()); \
  SWIG_exception_fail(SWIG_RuntimeError, msg)

  // Macro to try and catch OA exceptions in some typemaps
#define LANG_OA_TRY_RESUME(action, resume)\
try {\
  action;\
} catch (OpenAccess_4::oaException& e) {\
  LANG_THROW_OAEXCP(e);\
  resume;\
}

#define LANG_OA_TRY(action) LANG_OA_TRY_RESUME(action,)
    
#include <munged_headers/oaAppDefProxy.h>
%}


/* Catch exceptions, or else they are segfaults! */
%exception
{
  try {
    $action;
  } catch (OpenAccess_4::oaException& e) {
    LANG_THROW_OAEXCP(e);
  }
}

%typemap(out) int&
{
    $result = PyInt_FromLong((long) *$1);
}

%apply int& { long& };
%apply int& { unsigned int& };
%apply int& { unsigned long& };
%apply bool { OpenAccess_4::oaBoolean };

%typemap(out) float&
{
    $result = PyFloat_FromDouble((double) *$1);
}

%apply float& { double& };

#undef COLLCLASS
%define COLLCLASS(ns1, paramtype1, ns2, paramtype2)
%feature("valuewrapper") OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>;
%template(oaCollection_##paramtype1##_##paramtype2##) OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>;
%extend OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2> 
{
    %feature("python:slot", "tp_iter", functype="getiterfunc") __iter__;

    OpenAccess_4::oaIter<ns1::paramtype1> __iter__ () const
    { return OpenAccess_4::oaIter<ns1::paramtype1>(*self); }
}
%enddef

#undef ITERCLASS
%define ITERCLASS(ns, paramtype)
%template (oaIter_##paramtype) OpenAccess_4::oaIter<ns::paramtype>;
%extend OpenAccess_4::oaIter<ns::paramtype>
{
    %feature("python:slot", "tp_iternext", functype="iternextfunc") __iternext__;

    ns::paramtype* __iternext__ ()
    { return self->getNext(); }
}
%enddef

#undef ENUMWRAPPER
%define ENUMWRAPPER(TypeNS, TypeName, EnumNS, EnumName)
%extend TypeNS::TypeName
{
    %feature("python:slot", "nb_int", functype="unaryfunc") __int__;

    int __int__ ()
    { return (EnumName)(*self); }
}
%enddef

/* Generic typemaps. */
%include <generic/typemaps/oaNames.i>

