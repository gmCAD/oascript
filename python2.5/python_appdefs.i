/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%pythoncode %{

class oaAppDefTemplate :
    """Base class for template specializations of oa*AppDef<class T>"""

    def __init__ (self, appDefType, objType):
        if (not hasattr(objType, 'dbType')):
            raise TypeError, "Can't use type %s, because it doesn't have a dbType field." % objType.__name__
        if (not hasattr(objType, 'domain')):
            raise TypeError, "Can't use type %s, because it doesn't have a domain field." % objType.__name__
        self.appDefType = appDefType
        self.objType = objType

    def find (self, name, objDef=None):
        if objDef :
            result = oaAppDefProxy.findProxy(name, self.objType.dbType, oa.oacNullIndex, oaDomain(self.objType.domain), objDef)
        else :
            result = oaAppDefProxy.findProxy(name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain))
        if (result and int(result.getType()) == self.appDefType) :
            return result
        return None
        
class oaIntAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaIntAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacIntAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], defValue=0, persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if (len(args) > 2):
            raise TypeError, "Too many arguments to oaIntAppDefTemplate.get"
        if (len(args) > 0):
            defValue = args[0]
        else:
            defValue = 0
        if (len(args) == 2):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, defValue, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist, defValue)
        return oaIntAppDefProxy.getProxy(*getArgs)

class oaFloatAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaFloatAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacFloatAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], defValue=0.0, persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if (len(args) > 2):
            raise TypeError, "Too many arguments to oaFloatAppDefTemplate.get"
        if (len(args) > 0):
            defValue = args[0]
        else:
            defValue = 0.0
        if (len(args) == 2):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, defValue, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist, defValue)
        return oaFloatAppDefProxy.getProxy(*getArgs)

class oaStringAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaStringAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacStringAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], defValue="", persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if (len(args) > 2):
            raise TypeError, "Too many arguments to oaStringAppDefTemplate.get"
        if (len(args) > 0):
            defValue = args[0]
        else:
            defValue = ""
        if (len(args) == 2):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, defValue, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist, defValue)
        return oaStringAppDefProxy.getProxy(*getArgs)

class oaIntraPointerAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaIntraPointerAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacIntraPointerAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if (len(args) > 1):
            raise TypeError, "Too many arguments to oaIntraPointerAppDefTemplate.get"
        if (len(args) == 1):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist)
        return oaIntraPointerAppDefProxy.getProxy(*getArgs)

class oaInterPointerAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaInterPointerAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacInterPointerAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if (len(args) > 1):
            raise TypeError, "Too many arguments to oaInterPointerAppDefTemplate.get"
        if (len(args) == 1):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist)
        return oaInterPointerAppDefProxy.getProxy(*getArgs)

class oaDataAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaDataAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacDataAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], defValue, persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if len(args) < 1:
            raise TypeError, "Too few arguments to oaDataAppDefTemplate.get"
        if len(args) > 2:
            raise TypeError, "Too many arguments to oaDataAppDefTemplate.get"
        defValue = args[0]
        if (len(args) == 2):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, defValue, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist, defValue)
        return oaDataAppDefProxy.getProxy(*getArgs)

class oaVarDataAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaVarDataAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacVarDataAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], [defValue=""], persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if len(args) > 2:
            raise TypeError, "Too many arguments to oaVarDataAppDefTemplate.get"
        persist = True
        defValue = ""
        if len(args) == 1:
            if isinstance(args[0], str): defValue = args[0]
            else: persist = args[0]
        elif len(args) > 1:
            defValue = args[0]
            persist = args[1]
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, defValue, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist, defValue)
        return oaVarDataAppDefProxy.getProxy(*getArgs)

class oaTimeAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaTimeAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacTimeAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], defValue=0, persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if (len(args) > 2):
            raise TypeError, "Too many arguments to oaTimeAppDefTemplate.get"
        if (len(args) > 0):
            defValue = args[0]
        else:
            defValue = 0
        if (len(args) > 1):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, defValue, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist, defValue)
        return oaTimeAppDefProxy.getProxy(*getArgs)

class oaDoubleAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaDoubleAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacDoubleAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], defValue=0.0, persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if (len(args) > 2):
            raise TypeError, "Too many arguments to oaDoubleAppDefTemplate.get"
        if (len(args) > 0):
            defValue = args[0]
        else:
            defValue = 0.0
        if (len(args) > 1):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, defValue, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist, defValue)
        return oaDoubleAppDefProxy.getProxy(*getArgs)

class oaVoidPointerAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaVoidPointerAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacVoidPointerAppDefType, objType)

    def get(self, name, appObjDef=None):
        # get(name, [objdef])
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain))
        return oaVoidPointerAppDefProxy.getProxy(*getArgs)

class oaBooleanAppDefTemplate (oaAppDefTemplate) :
    """Represents a template specialization of oaBooleanAppDef<class T>"""

    def __init__(self, objType):
        oaAppDefTemplate.__init__(self, oacBooleanAppDefType, objType)

    def get(self, name, *args):
        # get(name, [objdef], defValue=false, persist=true)
        appObjDef = None
        if (len(args) and args[0].__class__ == oaAppObjectDef):
            appObjDef = args[0]
            args = args[1:]
        if (len(args) > 2):
            raise TypeError, "Too many arguments to oaBooleanAppDefTemplate.get"
        if (len(args) > 0):
            defValue = args[0]
        else:
            defValue = False
        if (len(args) > 1):
            persist = args[1]
        else:
            persist = True
        if appObjDef:
            getArgs = (name, oacNullIndex, oacNullIndex, oaDomain(oacNoDomain), persist, defValue, appObjDef)
        else:
            getArgs = (name, self.objType.dbType, self.objType.dtIndex, oaDomain(self.objType.domain), persist, defValue)
        return oaBooleanAppDefProxy.getProxy(*getArgs)

#-------------------------------------------------------------------------------

import sys as _sys
oa = _sys.modules['oa']
oa.oaIntAppDef = lambda objType : oaIntAppDefTemplate(objType)
oa.oaFloatAppDef = lambda objType : oaFloatAppDefTemplate(objType)
oa.oaStringAppDef = lambda objType : oaStringAppDefTemplate(objType)
oa.oaIntraPointerAppDef = lambda objType : oaIntraPointerAppDefTemplate(objType)
oa.oaInterPointerAppDef = lambda objType : oaInterPointerAppDefTemplate(objType)
oa.oaDataAppDef = lambda objType : oaDataAppDefTemplate(objType)
oa.oaVarDataAppDef = lambda objType : oaVarDataAppDefTemplate(objType)
oa.oaTimeAppDef = lambda objType : oaTimeAppDefTemplate(objType)
oa.oaDoubleAppDef = lambda objType : oaDoubleAppDefTemplate(objType)
oa.oaVoidPointerAppDef = lambda objType : oaVoidPointerAppDefTemplate(objType)
oa.oaBooleanAppDef = lambda objType : oaBooleanAppDefTemplate(objType)

%}
