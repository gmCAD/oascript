###############################################################################
#
# Copyright 2010 Si2, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# This makefile.defs is similar to the C++ labs makefile.defs, but
#   - it's greatly simplified
#   - there are fewer targets
#   - a 'make test' target has been added that runs and checks the
#     design in one make command
#   - 'make check' combines the C++ labs 'make check' and 'make dump'
#
# The top-level Makefile exports ALL needed environment variables into here.
#
# OTHERWISE, if you're running from either the labs/ directory or
# an individual lab directory, e.g. netlist/:
#
#  - These MUST be set:
#       OA_ROOT
#       OA_OPTMODE
#
#  - If the labs tree has been moved away from the original scripting 
#    intallation, this MUST be set:
#       OAS_ROOT
#
#  - If the following environment variables that specify the scripting language 
#    library paths are not set, then they are guessed. If the guess is wrong,
#    then these need to be set and exported.
#       PYTHONPATH
#       RUBYLIB
#       TCLLIBPATH
#
#  - If the following environment variables that specify the scripting language 
#    executables are not set, then they are guessed. If the guess is wrong, then
#    these need to be set and exported.
#       PERL
#       PYTHON
#       RUBY
#       TCL
#
#  - LD_LIBRARY_PATH *must* point to the OA libraries or the labs will not work
#    correctly. This makefile.defs file computes it in the same way as the C++
#    labs. If it fails because the user's OA environment is different than expected,
#    please change the setting in this file.
#
######################################################################################

# Lab users: Please uncomment and set these variables, or set them in your
# enviornment and export.
#

ifndef OA_ROOT
  # Set OA_ROOT to the top of the OpenAccess installation. e.g.
  #       OA_ROOT = /opt/OpenAccess/oa22.04p054
  $(info setting OA_ROOT)
  OA_ROOT = /home/OpenAccess/oa22.04p054
endif

ifndef OA_OPTMODE
  $(info setting OA_OPTMODE)
  # Set OA_OPTMODE to either dbg or opt
  OA_OPTMODE = opt
endif

# If you moved the labs out of the script tree, then set OAS_ROOT 
# to the top of the OA Scipting installation. e.g.
#       OAS_ROOT = /opt/OpenAccess/oaScript-v0.2-2010.10.18
ifndef OAS_ROOT
  $(info setting OAS_ROOT)
  OAS_ROOT = ../../..
endif

$(info OA_ROOT is $(OA_ROOT))
$(info OA_OPTMODE is $(OA_OPTMODE))
$(info OAS_ROOT is $(OAS_ROOT))


######################################################################################
# You should not have to change anything below here.

LABNAME = $(basename $(PROG))
OUTFILE = $(LABNAME).out
EXT     = $(suffix $(PROG))

ifndef OA_ROOT
  $(info  )
  $(info *** ERROR! OA_ROOT not set )
  $(info  )
  $(info  Please edit makefile.defs and set OA_ROOT to the OpenAccess installation.
  $(info  For example: /opt/OpenAccess/oa22.04p054
  $(info               /myTop/2204064/lib/linux_rhel40_32/dbg)
  $(info  )
  $(error Cannot run until OA_ROOT is set)
endif

ifndef OAS_ROOT
  $(info  )
  $(info *** ERROR! OAS_ROOT not set )
  $(info  )
  $(info  Please edit makefile.defs and set OAS_ROOT to the top of the Scripting installation.
  $(info  For example: /opt/OpenAccess/oaScript-v0.2-2010.10.18
  $(info  )
  $(error Cannot run until OAS_ROOT is set)
endif

ifndef OA_OPTMODE
  $(info  )
  $(info *** ERROR! OA_OPTMODE not set )
  $(info  )
  $(info  Please edit makefile.defs and set OA_OPTMODE to either dbg or opt)
  $(info  )
  $(error Cannot run until OA_OPTMODE is set)
endif


ifndef OA_LIB
  OA_SYSNAME := $(shell $(OA_ROOT)/bin/sysname)_$(if $(findstring 64,$(shell uname -m)),64,32)
  OA_LIB     := lib/$(OA_SYSNAME)/$(OA_OPTMODE)
endif

export LD_LIBRARY_PATH := $(OA_ROOT)/$(OA_LIB):$(LD_LIBRARY_PATH)
$(info Setting LD_LIBRARY_PATH to $(LD_LIBRARY_PATH))

# If we are NOT being invoked from the top-level scripting WG directory, we have
# to guess the paths to the bindings and the scripting language commands based
# on the file extension. If this does not work, the user must set the paths
# and commands and export them.

ifeq "$(EXT)" ".pl"
  export PERL5LIB := $(PERL5LIB):$(OAS_ROOT)/perl5
  $(info Setting PERL5LIB to $(PERL5LIB))
  ifndef PERL
    PERL = perl
  endif
  SCRIPTING_CMD = $(PERL)
  LANGUAGE = PERL
endif

ifeq "$(EXT)" ".py"
  export PYTHONPATH := $(PYTHONPATH):$(OAS_ROOT)/python2.5
  $(info Setting PYTHONPATH to $(PYTHONPATH))
  ifndef PYTHON
    PYTHON = python
  endif
  SCRIPTING_CMD = $(PYTHON)
  LANGUAGE = PYTHON
endif

ifeq "$(EXT)" ".rb"
  export RUBYLIB := $(RUBYLIB):$(OAS_ROOT)/ruby1.8
  $(info Setting RUBYLIB to $(RUBYLIB))
  ifndef RUBY
    RUBY = ruby
  endif
  SCRIPTING_CMD = $(RUBY)
  LANGUAGE = RUBY
endif

ifeq "$(EXT)" ".tcl"
  export TCLLIBPATH := $(TCLLIBPATH) $(OAS_ROOT)/tcl8.4
  $(info Setting TCLLIBPATH to $(TCLLIBPATH))
  ifndef TCL
    TCL = tclsh
  endif
  SCRIPTING_CMD = $(TCL)
  LANGUAGE = TCL
endif

$(info Scripting command is $(SCRIPTING_CMD))
$(info File extension is $(EXT))

$(info labs/makefile.defs. Paths are:)
$(info    PERL5LIB   $(PERL5LIB))
$(info    PYTHONPATH $(PYTHONPATH))
$(info    RUBYLIB is $(RUBYLIB))
$(info    TCLLIBPATH $(TCLLIBPATH))

$(info using logfile $(LAB_LOGFILE))

LLF_NAME = lib.defs

DUMP        = ../oadump/oadump$(EXT)
DIFF        = diff

###########################################
# Targets
###########################################

.PHONY: default

default: help

#################### HELP ##########

.PHONY: help

help:
	@echo ""
	@echo "____________________TARGETS____________________"
	@echo ""
	@echo "  help           This help information."
	@echo ""
	@echo "  run            runs $(PROG) [optional: INPUT='input args']"
	@echo "  test           cleans, runs, and checks $(PROG). Prints an error log."
	@echo ""
	@echo "  check          After a run, compares output against expected Solution/$(GOLD).out"
	@echo "  dump           After a run, uses oadump to check the contents of each cellview in a Lib"
	@echo ""
	@echo "  cleanlocks     Delete remnant DM server locks from data directories"
	@echo "  cleanoutputs   *.dump *.out (and other program outputs)"
	@echo "  cleandesign    $(LIBPATH)/"
	@echo "  clean          cleanderived cleanlocks cleanoutputs cleanlibrary"
	@echo "  cleanliblist   $(LLF_NAME)"
	@echo "  cleanall       clean cleandesign cleanliblist"
	@echo ""
	@echo "____________________MAKE VARIABLES____________________"
	@echo ""
	@echo 'By default in the run: target, variable TEE2FILE="| tee $$(PROG).out" '
	@echo 'which sends a copy of output to the console and a file. Alternatives:'
	@echo ""
	@echo '   make run TEE2FILE=                ...send to console only'
	@echo '   make run TEE2FILE=">file.txt"     ...send to file.txt only'
	@echo ""
	@echo "========================================================================================="
	@echo ""


#################### RUN ##########

.PHONY: run llf

llf:
	@echo "Checking lib.defs"
	@echo "LIBDEFS is $(LLF_NAME)"
	@if [ -z "$(LLF)" ] ;\
  then echo "No library list file needed in advance for this lab." ;\
	else echo $(LLF) >$(LLF_NAME);\
	fi

# The "labs" target is invoked from a higher level makefile instead
# of the "run" target.

run labs: llf
	@echo Running: PROG=$(PROG)
	@if [ -f "./test.inp" ]          ;\
	  then $(SCRIPTING_CMD) $(PROG) < ./test.inp | tee $(OUTFILE) ;\
	  else $(SCRIPTING_CMD) $(PROG) $(INPUT) | tee $(OUTFILE) ;\
	fi


#################### CHECK ##########

.PHONY: diff dump check

check:  diff dump


diff:
	@echo ""
	@echo "Comparing golden Solution/$(GOLD).out with program output ./$(OUTFILE)"
	@if [ ! -f $(OUTFILE) ]; then echo "Must first \`make run\` with INPUT defaults."; exit 1; fi;
	@if $(DIFF) -w -B Solution/$(GOLD).out $(OUTFILE) ; \
    then echo "...Matches" ;\
    else  \
      echo "...does not Match" ;\
      echo "  - $(LANGUAGE): gold file comparison failed in lab $(LABNAME)" \
          >> $(LAB_LOGFILE) ;\
    fi

dump: $(DUMP)
	@echo ""
	@echo "Running $(DUMP) on design $(LIB)" ;\
    if [ ! -z "$(TYPE)" ];  then echo "view[s] of $(TYPE) type ";  fi
	@for curcell in $(CELL) ;\
    do echo "DESIGN: $(LIB)/$(CELL)/$(VIEW)" ;\
      $(SCRIPTING_CMD) $(DUMP) $(LIB) $$curcell $(VIEW) $(TYPE) ;\
       echo "" ;\
       echo "Compare Solution/$${curcell}$(VIEW).dump with new version just created in ./" ;\
       sort --ignore-case -o $${curcell}$(VIEW).dump.tmp $${curcell}$(VIEW).dump ;\
       sort --ignore-case -o Solution/$${curcell}$(VIEW).dump.tmp Solution/$${curcell}$(VIEW).dump ;\
       if $(DIFF)  -w  Solution/$${curcell}$(VIEW).dump.tmp $${curcell}$(VIEW).dump.tmp ;\
         then echo "...Matches"        ;\
         else \
           echo "...does not Match" ;\
           echo "  - $(LANGUAGE): oadump comparison failed for " \
                "$(LIB)/$$curcell/$(VIEW) $(TYPE)" \
                " in lab $(LABNAME)" \
                >> $(LAB_LOGFILE) ;\
         fi ;\
       rm $${curcell}$(VIEW).dump.tmp Solution/$${curcell}$(VIEW).dump.tmp ;\
    done

#################### TEST ##########

.PHONY: test

include $(OAS_ROOT)/checklog.mk

test: cleanall llf run check $(CHECKLOG)

cleanlabs: cleanall

#################### CLEAN ##########

.PHONY: clean cleanall cleanoutputs \
	cleandlls cleanlibrary cleanderived cleanliblist cleanlocks cleandesign

clean: cleanlocks cleanoutputs cleanlibrary

# 
#
cleanall: clean cleandesign cleanliblist cleanlocks

cleanoutputs: 
	@echo .....Removing from `pwd` OUTPUTs: *.dump *.out *.log
	@$(RM) -rf  *.dump  *.out *.log
 
cleanlibrary:
	@echo .....Removing from `pwd` LIBRARY: $(LIBPATH)
	@$(RM) -rf  $(LIBPATH)

cleanliblist:
	@echo .....Removing from `pwd` library def file: $(LLF_NAME)
	@$(RM) -rf  $(LLF_NAME)

cleanlocks:
	@echo .....Removing from $(LIBPATH) LOCKS in design data tree
	@if [ -s "$(LIBPATH)" ];  then find $(LIBPATH) -name *.cdslck   -exec $(RM) -f {} \; ;  fi
	@echo "    " DMFileSys: '*.cdslck' 
	@if [ -s "$(LIBPATH)" ];  then find $(LIBPATH) -name server.xml -exec $(RM) -f {} \; ;  fi
	@echo '     DMTurbo:   server.xml'

cleandesign:
	@if [ ! -z "$(LIBPATH)"  -a  -d "$(LIBPATH)" ]; then \
	  ( cwd=`pwd`; cd $(LIBPATH);  cd ..; echo .....LIBPATH is in directory: `pwd`;\
	   if [ `pwd` != $$cwd ] ;\
	     then echo "***WARNING: You are about to remove an OA data directory ($(LIBPATH)) outside the cwd=$$cwd" ;\
		  echo "Enter 'yes' if you REALLY want to remove $(LIBPATH)..." ;\
		  read answer ;\
		  if [ "$answer" = "yes" ] ;\
		    then echo "     Removing the design data in $(LIBPATH)" ;\
			 $(RM) -rf  $(LIBPATH) ;\
		    else echo "     Leaving $(LIBPATH) intact" ;\
		  fi ;\
             else echo "     Removing testdata directory $(LIBPATH)" ;\
	          $(RM) -rf $(LIBPATH) ;\
	   fi )\
	fi
