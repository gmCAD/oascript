#! /usr/bin/env python2.5
################################################################################
#
#  Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 (the "License"] you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#   Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   09/10/10  1.0     Si2              Tutorial 10th Edition - oaspython version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

#  Each PathSeg represented by a - line representing a different Route
#  with the function creating them listed underneath.
#  _________________________________________________________________________
#  |Top                                                                      |
#  |    ___________________    ___________________    ___________________    |
#  |   |Gate               |  |Gate               |  |Gate               |   |
#  |   |             -     |  |             -     |  |             -     |   |
#  |   |             -     |  |             -     |  |             -     |   |
#  |   |  -          -     |  |  -          -     |  |  -          -     |   |
#  |   |  -     NetShapeRoute |  -     NetShapeRoute |  -     NetShapeRoute  |
#  |   |  --               |  |  --               |  |  --               |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  --    OrInst     |  |  --    OrInst     |  |  --    OrInst     |   |
#  |   |  ---   NandInst1  |  |  ---   NandInst1  |  |  ---   NandInst1  |   |
#  |   |  ---   NandInst2  |  |  ---   NandInst2  |  |  ---   NandInst2  |   |
#  |   |  -     InvInst    |  |  -     InvInst    |  |  -     InvInst    |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   | ConnectRouteForNet|  | ConnectRouteForNet|  | ConnectRouteForNet|   |
#  |   |___________________|  |___________________|  |___________________|   |
#  |                                                                         |
#  |        -                                                                |
#  |        -                                                                |
#  |        -                   -                                            |
#  |        -                RouteForNet                                     |
#  |        -                                                                |
#  |    EmptyNets                                                            |
#  |_________________________________________________________________________|

import sys
import os

import inspect

here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '../..'))

import oa.design


# ****************************** Gratuitous globals ******************************

mode_read  = "r"
mode_write = "w"


# ****************************** Functions ******************************
def lineno():
   # """Returns the current line number in our program."""
    return inspect.currentframe().f_back.f_lineno


def assert_def(condition, msg):
    if not condition:
        print "***ASSERT error %s" % (msg)


def getLayerPurpose (design, layerName, purpName):
#    upvar 1 $num  layerNum
#    upvar 1 $purp layerPurp

    techFile = design.getTech()

    if  ( not techFile):
        if (layerName == "pin"):
            layerNum = 229

        elif (layerName == "device"):
            layerNum = 231
        
        layerPurp = 252
        return 1,layerNum,layerPurp

    layer   = oa.oaLayer.find(techFile, layerName)
    purpose = oa.oaPurpose.find(techFile, purpName)

    if  (( not layer) or (not purpose)):
        return 0,0,0

    layerNum  = layer.getNumber()
    layerPurp = purpose.getNumber()

    return 1, layerNum,layerPurp

# *****************************************************************************
# Create oaInstTerm object with the given parameters, which may not be bound.
# *****************************************************************************

def createInstTerm(net, inst, termName):
    ns = oa.oaNativeNS()

    # It seems that we can't use any subclass of oaName for creating InstTerms.
    #set simpleTermName [oa::oaSimpleName $ns $termName]
    #return [oa::oaInstTerm_create $net $inst [oa::oaSimpleName $ns $termName]]

    #set scTermName [oa::oaScalarName $ns $termName]
    #return [oa::oaInstTerm_create $net $inst $scTermName]

    name = oa.oaName(ns, termName)
    return oa.oaInstTerm.create(net, inst, name)


# *****************************************************************************
# Create an InstTerm object for the given terminal with 'termName' in 'inst'
# *****************************************************************************

def getInstTerm(inst, net, termName):
    instTerm = createInstTerm(net, inst, termName)
    if not instTerm.getTerm():
        print "Inst term for %s unbound. Unable to create InstTerm" % (termName)
        return
    else:
        return instTerm




# *****************************************************************************
# Create Route with the ObjectArray, attach to the specified net, set end Conns
# *****************************************************************************

def connectRouteForNet(block, net, routeObjectArray, start, end):
    route = oa.oaRoute.create(block)
    route.setObjects(routeObjectArray)
    route.addToNet(net)
    if (start != "NULL"):
        route.setBeginConn(start)

    if (end != "NULL"):
        route.setEndConn(end)


# *****************************************************************************
# Create a Pin with the given name, Points and direction, on 'term'
# *****************************************************************************

def createPin(block, term, points, name, dir):
    layer   = ""
    purpose =  ""
    (success,layer,purpose) =getLayerPurpose( block.getDesign(), "pin", "drawing")

    fig = oa.oaPolygon.create(block, layer, purpose, points)
    pin = oa.oaPin.create(term, name, dir)
    fig.addToPin(pin)
    return pin


# *****************************************************************************
# Create a Net with the given name in the given Block.
# *****************************************************************************

def createNet(block, str):
    ns = oa.oaNativeNS()
    name = oa.oaScalarName(ns, str)
    return oa.oaScalarNet.create(block, name)


# ***************************************************************************
# createNetShapeRoute()
# ***************************************************************************

def createNetShapeRoute(design, block, refx, refy):
    # Create path shape, route, terminal and net
    net = createNet(block, "ShapeRouteNet")

    termTypeInput = oa.oaTermType(oa.oacInputTermType)
    termTypeOutput = oa.oaTermType(oa.oacOutputTermType)

    createTerm(net, "TermOnShapeRouteNet", termTypeInput)

    beginExt = 1
    endExt   = 1
    width    = 2
    (success,lNum,pNum)  = getLayerPurpose(design, "device", "drawing1")

    endStyle = oa.oaEndStyle("variable")
    segSty   = oa.oaSegStyle(width, endStyle, endStyle, beginExt, endExt)

    # Route  is created.
    point1 = oa.oaPoint(refx, refy)
    point2 = oa.oaPoint((refx+100), refy)
    seg1   = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray1 = [seg1]

    route1 = oa.oaRoute.create(block)
    route1.setObjects(routeObjectArray1)

    print "Unconnected route is created in Gate view"

    # Creating the shape: Ellipse
    centerx = (refx+100)
    centery = refy

    ellRect = oa.oaBox((centerx-10), (centery-10),(centerx+10), (centery+10))

    fig  = oa.oaEllipse.create(block, lNum, pNum, ellRect)
    fig.addToNet(net)

    print "An Ellipse is created in the Gate view"

    route1.addToNet(net)
    route1.setEndConn(fig)

    startx = centerx
    starty = centery

    point1 = oa.oaPoint(startx, starty)
    point2 = oa.oaPoint((startx+50), starty)
    seg2   = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray2 = [seg2]

    route2 = oa.oaRoute.create(block)
    route2.setObjects(routeObjectArray2)
    route2.addToNet(net)
    route2.setBeginConn(fig)

    p1 = oa.oaPoint((startx+55), starty)
    p2 = oa.oaPoint((startx+65), starty)
    pArray = [p1, p2]

    netPath = oa.oaPath.create(block, lNum, pNum, 1, pArray)
    netPath.addToNet(net)

    print "Net with Shape Route is created in Gate view"

    point1 = oa.oaPoint((startx+70), (starty+5))
    point2 = oa.oaPoint((startx+70), (starty+20))

    seg3   = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray3 = [seg3]

    route3 = oa.oaRoute.create(block)
    route3.setObjects(routeObjectArray3)

    p1 = oa.oaPoint((startx+70), (starty-5))
    p2 = oa.oaPoint((startx+70), (starty-20))
    pArray2 = [p1, p2]

    oa.oaPath.create(block, lNum, pNum, 1, pArray2)





# *****************************************************************************
# Create a ScalarInst in a Block with the given cellname. The View is
# assumed to be 'schematic for this purpose. Open the Design if it is not
# already open in the current session.
# *****************************************************************************

def createInstance(block, trans, cellName, instStr, scNameLib):
    global mode_read

    ns = oa.oaNativeNS()
    gate = oa.oaScalarName(ns, cellName)
    view = oa.oaScalarName(ns, "schematic")
    master = oa.oaDesign.find(scNameLib, gate, view)
    if not master:
        master = oa.oaDesign.open(scNameLib, gate, view, mode_read)
    
    if not master:
        return

    instName = oa.oaScalarName(ns, instStr)
    return oa.oaScalarInst.create(block, master, instName, trans)

# *****************************************************************************
# Create a Term with the given name on the given Net.
# *****************************************************************************

def createTerm(net, str, type):
    ns = oa.oaNativeNS()
    name = oa.oaScalarName(ns, str)
    return oa.oaScalarTerm.create(net, name, type)


# *****************************************************************************
# Create a gate schematic which contains 4 gates, (Or, 2 Nand, and an Inv) and
# connect the gates with physical and logical elements. Routes are used for
# physical connections. The created Design is represented by 'Lib' 'Gate'
# 'schematic' (lib, cell and view names), respectively
# *****************************************************************************

def createGateSchematic(scNameLib):
    global mode_write

    ns = oa.oaNativeNS()
    cell = oa.oaScalarName(ns, "Gate")
    view = oa.oaScalarName(ns, "schematic")

    print "Creating Gate schematic with 0r, 2 nand, Inv instances"

    vtSch = oa.oaViewType.get(oa.oaReservedViewType("schematic"))
  
    design = oa.oaDesign.open(scNameLib, cell, view, vtSch, mode_write)
    design.setCellType(oa.oaCellType("softMacro"))
    block = oa.oaBlock.create(design)

    trans = oa.oaTransform(0, 0)
    orInst = createInstance(block, trans, "Or", "OrInst", scNameLib)

    orBox = oa.oaBox()
    orInst.getBBox(orBox)

    height = orBox.getHeight()
    width  = orBox.getWidth()


    xMid = orBox.left()
    yMid = (orBox.bottom() +  height) / 2

    trans = oa.oaTransform(0, ( (0-2) * height))
    nand1 = createInstance(block, trans, "Nand", "NandInst1", scNameLib)

    nand1Box = oa.oaBox()
    nand1.getBBox(nand1Box)
    yMid2 = (nand1Box.bottom()) + (height / 2)

    nbb=nand1Box.bottom()

    net1 = createNet(block,"N1")
    bbox = oa.oaBox( 0, 0, 0, 0)
    stein1 = oa.oaSteiner.create(block, bbox)

    beginExt = 0
    endExt   = 0
    widthRt  = 2

    (success,lNum,pNum) = getLayerPurpose(design, "device", "drawing1")

    endStyle = oa.oaEndStyle("truncate")
    segSty   = oa.oaSegStyle(widthRt, endStyle, endStyle, beginExt, endExt)

    point1 = oa.oaPoint( (xMid-50), (yMid+10) ) 
    point2 = oa.oaPoint( (xMid-15), (yMid+10) )
    seg1 = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray1 =  [ seg1 ]

    p1 = oa.oaPoint( (xMid-70), (yMid+15))
    p2 = oa.oaPoint( (xMid-55), (yMid+15))
    p3 = oa.oaPoint( (xMid-50), (yMid+10))
    p4 = oa.oaPoint( (xMid-55), (yMid+5))
    p5 = oa.oaPoint( (xMid-70), (yMid+5))
    ptArray = [p1, p2, p3, p4, p5]

    termTypeInput = oa.oaTermType(oa.oacInputTermType)
    term1 = createTerm(net1, "a", termTypeInput)

    pin1 = createPin(block, term1, ptArray, "P1-In", oa.oacLeft)
    connectRouteForNet(block, net1, routeObjectArray1, pin1, stein1)

    point1 = oa.oaPoint( (xMid-15), (yMid+10))
    point2 = oa.oaPoint( (xMid),    (yMid+10))
    seg2 = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray2 = [seg2]

    instTerm = getInstTerm(orInst, net1, "T1")

    connectRouteForNet(block, net1, routeObjectArray2, stein1, instTerm)

    point1 = oa.oaPoint((xMid-15), (yMid+10))
    point2 = oa.oaPoint((xMid-15), (yMid2+10))
    seg3 = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    point1 = oa.oaPoint((xMid-15), (yMid2+10))
    point2 = oa.oaPoint((xMid),    (yMid2+10))
    seg4 = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray3 = [seg3, seg4]

    instTerm = getInstTerm(nand1, net1, "T1")
    connectRouteForNet(block, net1, routeObjectArray3, stein1, instTerm)

    net2 = createNet(block, "N2")
    bbox = oa.oaBox(0, 0, 0, 0)
    stein2 = oa.oaSteiner.create(block, bbox)

    point1 = oa.oaPoint((xMid-50), (yMid-10))
    point2 = oa.oaPoint((xMid-25), (yMid-10))
    seg5 = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray4 = [seg5]
    
    term2 = createTerm(net2, "b", termTypeInput)

    p1 = oa.oaPoint((xMid-70), (yMid-5))
    p2 = oa.oaPoint((xMid-55), (yMid-5))
    p3 = oa.oaPoint((xMid-50), (yMid-10))
    p4 = oa.oaPoint((xMid-55), (yMid-15))
    p5 = oa.oaPoint((xMid-70), (yMid-15))
    ptArray2 = [p1, p2, p3, p4, p5]


    pin2 = createPin(block, term2, ptArray2, "P2-In", oa.oacLeft)

    connectRouteForNet(block, net2, routeObjectArray4, pin2, stein2)

    point1 = oa.oaPoint((xMid-25), (yMid-10))
    point2 = oa.oaPoint((xMid),    (yMid-10))
    seg6   = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray5 = [seg6]

    instTerm = getInstTerm(orInst, net2, "T2")

    connectRouteForNet(block, net2, routeObjectArray5, stein2, instTerm)

    point1 = oa.oaPoint((xMid-25), (yMid-10))
    point2 = oa.oaPoint((xMid-25), (yMid2-10))
    seg7   = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    point1 = point2
    point2 = oa.oaPoint((xMid), (yMid2-10))
    seg8   = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray6 = [ seg7, seg8]

    instTerm = getInstTerm(nand1, net2, "T2")
    connectRouteForNet(block, net2, routeObjectArray6, stein2, instTerm)

    xPt  = (xMid+width)
    yPt  = yMid
    yPt2 = yMid2
    yPt3 = (yPt+yPt2)/2

    yNand2 =  (yPt+yPt2)/ 2 - (height/2)  
    # 0, 0 of nand gate is 65 units inside in x from lower left corner of its bBox.
    xNand2 = xPt+40+65

    trans = oa.oaTransform(xNand2, yNand2)
    nand2 = createInstance(block, trans, "Nand", "NandInst2", scNameLib)

    net3 = createNet(block, "N3")

    instTerm1 = getInstTerm(orInst, net3, "T3")
    instTerm2 = getInstTerm(nand2,  net3, "T1")

    point1 = oa.oaPoint(xPt, yPt)
    point2 = oa.oaPoint((xPt+20), yPt)
    seg9   = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    point1 = point2
    point2 = oa.oaPoint((xPt+20), (yPt3+10))
    seg10  = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    point1 = point2
    point2 = oa.oaPoint((xPt+40), (yPt3+10))
    seg11  = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray7 =  [seg9, seg10, seg11]

    connectRouteForNet(block, net3, routeObjectArray7, instTerm1, instTerm2)

    net4 = createNet(block, "N4")

    instTerm1 = getInstTerm(nand1, net4, "T3")
    instTerm2 = getInstTerm(nand2, net4, "T2")

    point1 = oa.oaPoint(xPt, yPt2)
    point2 = oa.oaPoint((xPt+20), yPt2)
    seg12  = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    point1 = point2
    point2 = oa.oaPoint((xPt+20), (yPt3-10))
    seg13  = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    point1 = point2
    point2 = oa.oaPoint((xPt+40), (yPt3-10))
    seg14  = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray8 = [seg12, seg13, seg14]

    connectRouteForNet(block, net4, routeObjectArray8, instTerm1, instTerm2)

    xfto9 = (xPt +40+width)
    yfto9 = (yPt+yPt2)/2
    net5  = createNet(block, "N5")

    point1 = oa.oaPoint(xfto9, yfto9)
    point2 = oa.oaPoint((xfto9+30), yfto9)
    seg15  = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray9 = [seg15]

    yInv = (yfto9 - 25)
    # {0, 0} of Inv gate is 35 units inside in x from lower left corner of its bBox.
    xInv = (xfto9 + 30 + 35)

    trans = oa.oaTransform(xInv, yInv)
    inv   = createInstance(block, trans, "Inv", "InvInst", scNameLib)
    # createInstTerm(net5, inv, "T1"]

    instTerm1 = getInstTerm(nand2, net5, "T3")
    instTerm2 = getInstTerm(inv, net5, "T1")
    connectRouteForNet(block, net5, routeObjectArray9, instTerm1, instTerm2)

    point1 = oa.oaPoint((xfto9+15), yfto9)
    point2 = oa.oaPoint((xfto9+15), (yfto9-90))
    seg16  = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray10 = [seg16]

    termTypeOutput = oa.oaTermType(oa.oacOutputTermType)
    danglingTerm = createTerm(net5, "Term-open", termTypeOutput)
    connectRouteForNet(block, net5, routeObjectArray10, "NULL", "NULL")

    xpt = (xfto9+15)
    ypt = (yfto9-90)

    p1 = oa.oaPoint((xpt+5), (ypt-20))
    p2 = oa.oaPoint((xpt+5),  (ypt-5))
    p3 = oa.oaPoint( xpt,     ypt)
    p4 = oa.oaPoint((xpt-5), (ypt-5))
    p5 = oa.oaPoint((xpt-5), (ypt-20))
    ptArray3 =  [p1, p2, p3, p4, p5]

    createPin(block, danglingTerm, ptArray3, "P-Out", oa.oacBottom) 

    xfto10 = (xfto9+30+160)
    yfto10 = yfto9

    net6 = createNet(block, "N6")

    xPinPt = (xfto10+30)

    p6  = oa.oaPoint((xPinPt+20), (yfto10+5))
    p7  = oa.oaPoint((xPinPt+ 5), (yfto10+5))
    p8  = oa.oaPoint((xPinPt),    (yfto10))
    p9  = oa.oaPoint((xPinPt+ 5), (yfto10-5))
    p10 = oa.oaPoint((xPinPt+20), (yfto10-5))
    ptArray4 = [p6, p7, p8, p9, p10]


    term3 = createTerm(net6, "SUM", termTypeOutput)
    pin4  = createPin(block, term3, ptArray4, "P3-Out", oa.oacRight)

    instTerm = getInstTerm(inv, net6, "T2")

    point1 = oa.oaPoint(xfto10, yfto10)
    point2 = oa.oaPoint((xfto10+30), yfto10)
    seg17  = oa.oaPathSeg.create(block, lNum, pNum, point1, point2, segSty)

    routeObjectArray11 = [seg17]

    connectRouteForNet(block, net6, routeObjectArray11, instTerm, pin4)

    createNetShapeRoute(design, block, xMid, yfto9)

    design.save()
    design.close()





# *****************************************************************************
# Creates the NAND schematic Design
# *****************************************************************************

def createNandSchematic(scNameLib):
    global mode_write

    ns = oa.oaNativeNS()
    cell = oa.oaScalarName(ns, "Nand")
    viewStr = oa.oaScalarName(ns, "schematic")

    vtSch = oa.oaViewType.get(oa.oaReservedViewType("schematic"))

    view = oa.oaDesign.open(scNameLib, cell, viewStr, vtSch, mode_write)
    block = oa.oaBlock.create(view)

    assert_def(view.isValid(), lineno())

    layer = "pin"
    purpose = "drawing"

    pinLayer = 0
    pinPurp  = 0

    (success,pinLayer, pinPurp) = getLayerPurpose(view, layer, purpose)
    assert_def(success, lineno())

    drLayer = 0
    drPurp  = 0

    (success, drLayer, drPurp) = getLayerPurpose(view, "device", "drawing")
    assert_def(success, lineno()) 

    p1 = oa.oaPoint(-50, 50)
    p2 = oa.oaPoint(0, 50)
    ptArray1 = [p1, p2]

    line = oa.oaLine.create(block, drLayer, drPurp, ptArray1)
    assert_def(line.isValid(), lineno())

    netName1 = oa.oaScalarName(ns, "TopTail")
    net = oa.oaScalarNet.create(block, netName1)
    assert_def(net.isValid(), lineno())
    line.addToNet(net)

    termName1 = oa.oaScalarName(ns, "T1")
    termTypeInput = oa.oaTermType(oa.oacInputTermType)
    term = oa.oaScalarTerm.create(net, termName1, termTypeInput)
    assert_def(term.isValid(), lineno())

    box = oa.oaBox(-65, 46, -50, 54)
    rect = oa.oaRect.create(block, pinLayer, pinPurp, box)
    assert_def(rect.isValid(), lineno())

    pin = oa.oaPin.create(term, "IN1", oa.oacLeft)
    rect.addToPin(pin)
    assert_def(pin.isValid(), lineno())
    p3 = oa.oaPoint(-50, 20)
    p4 = oa.oaPoint(0, 20)
    ptArray2 = [p3, p4]

    line = oa.oaLine.create(block, drLayer, drPurp, ptArray2)
    assert_def(line.isValid(), lineno())

    netName2 = oa.oaScalarName(ns, "BotTail")
    net = oa.oaScalarNet.create(block, netName2)
    assert_def(net.isValid(), lineno())
    line.addToNet(net)

    termName2 = oa.oaScalarName(ns, "T2")
    term = oa.oaScalarTerm.create(net, termName2, termTypeInput)
    assert_def(term.isValid(), lineno())

    box = oa.oaBox(-65, 16, -50, 24)
    rect = oa.oaRect.create(block, pinLayer, pinPurp, box)
    assert_def(rect.isValid(), lineno())

    pin = oa.oaPin.create(term, "IN2", oa.oacLeft)
    rect.addToPin(pin)
    assert_def(pin.isValid(), lineno())

    p5  = oa.oaPoint(0, 0)
    p6  = oa.oaPoint(80, 20)  
    p7  = oa.oaPoint(100, 20)
    p8  = oa.oaPoint(100, 50)
    p9  = oa.oaPoint(80, 70)
    p10 = oa.oaPoint(0, 70)
    ptArray3 = [p5, p6, p7, p8, p9, p10]

    gate = oa.oaPolygon.create(block, drLayer, drPurp, ptArray3)
    assert_def(gate.isValid(), lineno())


    p11  = oa.oaPoint(100, 40)
    p12  = oa.oaPoint(100, 30)  
    p13  = oa.oaPoint(110, 26)
    p14  = oa.oaPoint(118, 35)
    p15  = oa.oaPoint(110, 44)
    ptArray4 = [p11, p12, p13, p14, p15]

    bubble = oa.oaPolygon.create(block, drLayer, drPurp, ptArray4)
    assert_def(bubble.isValid(), lineno())

    p16  = oa.oaPoint(118, 35)
    p17  = oa.oaPoint(140, 35)  

    ptArray5 = [p16, p17]

    line2 = oa.oaLine.create(block, drLayer, drPurp, ptArray5)
    assert_def(line2.isValid(), lineno())

    netName3 = oa.oaScalarName(ns, "OutTail")
    net2 = oa.oaScalarNet.create(block,  netName3)
    assert_def(net2.isValid(), lineno())

    line2.addToNet(net2)
    termName3 = oa.oaScalarName(ns, "T3")
    termTypeOutput = oa.oaTermType(oa.oacOutputTermType)
    term2 = oa.oaScalarTerm.create(net2, termName3, termTypeOutput)

    box = oa.oaBox(140, 31, 155, 39)
    rect2 = oa.oaRect.create(block, pinLayer, pinPurp, box)
    assert_def(rect2.isValid(), lineno())

    pin2 = oa.oaPin.create(term2, "OUT", oa.oacRight)
    rect2.addToPin(pin2)
    assert_def(pin2.isValid(), lineno())

    view.save()
    view.close()


# *****************************************************************************
# Create the INV schematic Design
# *****************************************************************************


def createInvSchematic(scNameLib):
    global write_mode

    ns   = oa.oaNativeNS()
    cell = oa.oaScalarName(ns, "Inv")
    viewStr = oa.oaScalarName( ns, "schematic")

    type = oa.oaViewType.get(oa.oaReservedViewType("schematic"))

    view  = oa.oaDesign_open(scNameLib, cell, viewStr, type, mode_write)
    block = oa.oaBlock.create(view)

    assert_def(view.isValid(), lineno())

    layer   = "pin"
    purpose = "drawing"

    pinLayer = 0
    pinPurp  = 0

    (success, pinLayer, pinPurp) = getLayerPurpose(view, layer, purpose)
    assert_def( success, lineno());

    p1 = oa.oaPoint(-20, 25)
    p2 = oa.oaPoint(0, 25)
    ptArray1 = [p1, p2]
    
    path = oa.oaPath.create(block, pinLayer, pinPurp, 2, ptArray1)
    assert_def(path.isValid(), lineno())

    name = oa.oaScalarName(ns, "TopTail")
    net  = oa.oaScalarNet.create(block, name)
    assert_def(net.isValid(), lineno())
    path.addToNet(net)

    termName = oa.oaScalarName(ns, "T1")
    termTypeInput = oa.oaTermType(oa.oacInputTermType)
    term = oa.oaScalarTerm.create(net, termName, termTypeInput)
    assert_def(term.isValid(),lineno())

    box  = oa.oaBox(-35, 21,-20, 29)
    rect = oa.oaRect.create(block, pinLayer, pinPurp, box)
    assert_def(rect.isValid(), lineno())

    pin = oa.oaPin.create(term, "IN1", oa.oacLeft)
    rect.addToPin(pin)
    assert_def(pin.isValid(), lineno())

    drLayer = 0
    drPurp  = 0

    (success, drLayer, drPurp) = getLayerPurpose(view, "device", "drawing2")
    assert_def(success, lineno())

    p3 = oa.oaPoint(0, 0) 
    p4 = oa.oaPoint(0, 50) 
    p5 = oa.oaPoint(70, 25)
    ptArray3 = [ p3, p4, p5 ]

    gate = oa.oaPolygon.create(block, drLayer, drPurp, ptArray3)
    assert_def(gate.isValid(), lineno())


    p6  = oa.oaPoint(70, 30)
    p7  = oa.oaPoint(70, 20)
    p8  = oa.oaPoint(80, 16)
    p9  = oa.oaPoint(88, 25)
    p10 = oa.oaPoint(80, 34)
    ptArray4 = [p6, p7, p8, p9, p10]

    bubble = oa.oaPolygon.create(block, drLayer, drPurp, ptArray4)
    assert_def(bubble.isValid(), lineno())

    p11 = oa.oaPoint(88, 25)
    p12 = oa.oaPoint(110, 25)
    ptArray5 = [p11, p12]

    path2 = oa.oaPath.create(block, pinLayer, pinPurp, 2, ptArray5)
    assert_def(path2.isValid(), lineno())

    name = oa.oaScalarName(ns, "OutTail")
    net2 = oa.oaScalarNet.create(block, name)
    assert_def(net2.isValid(), lineno())

    path2.addToNet(net2)
    termName = oa.oaScalarName(ns, "T2")
    termTypeOutput = oa.oaTermType(oa.oacOutputTermType)
    term2 = oa.oaScalarTerm.create(net2, termName, termTypeOutput)

    box = oa.oaBox(110, 21, 125, 29)
    rect2 = oa.oaRect.create(block, pinLayer, pinPurp, box)
    assert_def(rect2.isValid(), lineno())

    pin2 = oa.oaPin.create(term2, "OUT", oa.oacRight)
    rect2.addToPin(pin2)
    assert_def(pin2.isValid(), lineno())

    view.save()
    view.close()


# *****************************************************************************
# Create an oaPath element between 'start' and 'end'. Also create a
# Rect Fig and oaPin object with the Rect if 'addpin' is true
# *****************************************************************************

def createFigForNet(design, block, term, net, startpt, endpt, pinName, addpin=1):

    (success, lNum, pNum) = getLayerPurpose(design, "device", "drawing1")

    ptarray = [startpt, endpt]

    startX = startpt.x()
    startY = startpt.y()

    endX = endpt.x()
    endY = endpt.y()

    pt1 = oa.oaPoint(startX, endY)
    pt2 = oa.oaPoint(endX, startY)

    if (endX != startX) and (endX > startX):
        if (endY != startY):
            ptarray = [startpt, pt1, endpt] 

    elif (endX != startX) and (endX < startX):
        if (endY != startY):
            ptarray = [startpt, pt2, endpt]


    #lappend ptArray end
    #print " ========================= "
    #for pt in ptarray:
    #   print "ptarray: (%d, %d) " % tuple(pt)    

    path = oa.oaPath.create(block, lNum, pNum, 2, ptarray)
    path.addToNet(net)

    ttypeStr = ""
    if (term != None):
        ttype=term.getTermType()
        ttypeStr = ttype.getName()

    if addpin:
        dir = ""
        pt1 = endpt
        pt2 = endpt

        if (ttypeStr == "input"):
            pt1 = oa.oaPoint((endX-2), (endY-2))
            dir = oa.oacLeft
        elif (ttypeStr == "output"):
            pt2 = oa.oaPoint((endX+2), (endY+2))
            dir = oa.oacRight

        box  = oa.oaBox(pt1, pt2)
        rect = oa.oaRect.create(block, lNum, pNum, box)
        pin  = oa.oaPin.create(term, pinName, dir)
        rect.addToPin(pin)


    return path

# ***************************************************************************
# Create a few Nets in the top Block with some Routes associated.
# ***************************************************************************

def createEmptyNets(top, topBlock, box):
    refx = box.right()
    refy = box.top()

    # A Net with one terminal but without connection to top
    net1 = createNet(topBlock, "EmptyNet1")
    createTerm(net1, "TermEmptyNet", oa.oaTermType(oa.oacInputTermType))

    print "An empty net with one terminal is created"

    # Creating Net that has route->shape->route connection

    rec_box = oa.oaBox((refx+50), refy, (refx+70), (refy+ 20))
    lNum = ""
    pNum = ""

    beginExt = 0
    endExt   = 0
    width    = 2
    (success , lNum, pNum)= getLayerPurpose(top, "device", "drawing1")

    endStyle = oa.oaEndStyle("truncate")
    segSty   = oa.oaSegStyle(width, endStyle, endStyle, beginExt, endExt)

    rect = oa.oaRect.create(topBlock, lNum, pNum, rec_box)

    point1 = oa.oaPoint(refx, (refy+10))
    point2 = oa.oaPoint((refx+50), (refy+10))
    seg1   = oa.oaPathSeg.create(topBlock, lNum, pNum, point1, point2, segSty)

    routeObjectArray1 = [seg1]
    route1 = oa.oaRoute.create(topBlock)
    route1.setObjects(routeObjectArray1)

    point1 = oa.oaPoint((refx+60), (refy+10))
    point2 = oa.oaPoint((refx+90), (refy+10))
    seg2   = oa.oaPathSeg.create(topBlock, lNum, pNum, point1, point2, segSty)

    routeObjectArray2 = [seg2]
    route2 = oa.oaRoute.create(topBlock)
    route2.setObjects(routeObjectArray2)

    route1.setEndConn(rect)
    route2.setBeginConn(rect)

    routeNet1 = createNet(topBlock, "RouteNet1")
    rect.addToNet(routeNet1)
    route1.addToNet(routeNet1)
    route2.addToNet(routeNet1)

    print "A net with 2 routes and a shape is created"

    # A net with route->route

    point1 = oa.oaPoint((refx+100), refy)
    point2 = oa.oaPoint((refx+120), refy)
    seg3   = oa.oaPathSeg.create(topBlock, lNum, pNum, point1, point2, segSty)

    routeObjectArray3 = [seg3]
    route3 = oa.oaRoute.create(topBlock)
    route3.setObjects(routeObjectArray3)

    point1 = oa.oaPoint((refx+120), (refy-20))
    point2 = oa.oaPoint((refx+120), (refy+20))
    seg4   = oa.oaPathSeg.create(topBlock, lNum, pNum, point1, point2, segSty)

    routeObjectArray4 = [seg4]
    route4 = oa.oaRoute.create(topBlock)
    route4.setObjects(routeObjectArray4)

    point1 = oa.oaPoint((refx+120), (refy-10))
    point2 = oa.oaPoint((refx+130), (refy-10))
    seg5   = oa.oaPathSeg.create(topBlock, lNum, pNum, point1, point2, segSty)

    routeObjectArray5 = [seg5]
    route5 = oa.oaRoute.create(topBlock)
    route5.setObjects(routeObjectArray5)

    routeNet2 = createNet(topBlock, "RouteNet2")
    route3.addToNet(routeNet2)
    route4.addToNet(routeNet2)
    route5.addToNet(routeNet2)

    print "A net with 2 routes connecting to each other is created"



# *****************************************************************************
# Creates a hierarchy in the top level Design with cellname "Sample".
# It contains three Inst of the Design with the cellname "Gate" and
# connects the input and output Terms of the gates. Logical and physical
# connections are also created. The Design has 4 input and an 1 output Terms.
# *****************************************************************************

def createHierarchy(scNameLib):
    global mode_write
    global mode_read

    ns    = oa.oaNativeNS()
    cell  = oa.oaScalarName(ns, "Sample")
    cell2 = oa.oaScalarName(ns, "Gate")
    view  = oa.oaScalarName(ns, "schematic")

    type = oa.oaViewType.get(oa.oaReservedViewType("schematic"))

    print "\nCreating Sample schematic with 3 Gate instances"
    top = oa.oaDesign.open(scNameLib, cell, view, type, mode_write)
    topBlock = oa.oaBlock.create(top)

    top.setCellType(oa.oaCellType("softMacro"))
    master = oa.oaDesign.open(scNameLib, cell2, view, type, mode_read)
    masterBlock = master.getTopBlock()

    bbox = oa.oaBox()
    masterBlock.getBBox(bbox)

    width  = bbox.getWidth()
    height = bbox.getHeight()

    trans = oa.oaTransform( (-height-5), 0, oa.oaOrient(oa.oacR90))
    inst1 = createInstance(topBlock, trans, "Gate", "Inst1", scNameLib)

    trans = oa.oaTransform((height+5), 0, oa.oaOrient(oa.oacR90))
    inst2 = createInstance(topBlock, trans, "Gate", "Inst2", scNameLib)

    trans = oa.oaTransform(0, (width+20))
    inst3 = createInstance(topBlock, trans, "Gate", "Inst3", scNameLib)

    net1  = createNet(topBlock, "N1")
    term1 = createTerm(net1, "T1", oa.oaTermType(oa.oacInputTermType))

    box = oa.oaBox()
    inst1.getBBox(box)

    xEnd  = box.left()
    yEnd  = box.bottom()

    point1 = oa.oaPoint((xEnd+20), yEnd)
    point2 = oa.oaPoint((xEnd+20), (yEnd-50))

    createFigForNet(top, topBlock, term1, net1, point1, point2, "P-1")

    pin = oa.oaPin_find(term1, "P-1")
    instTerm = getInstTerm(inst1, net1, "a")

    lNum = ""
    pNum = ""
    beginExt = 0
    endExt   = 0
    widthRt  = 2
    (success,lNum, pNum) = getLayerPurpose(top, "device", "drawing1")

    endStyle = oa.oaEndStyle("truncate")
    segSty = oa.oaSegStyle(widthRt, endStyle, endStyle, beginExt, endExt)

    point1 = oa.oaPoint((xEnd+20), yEnd)
    point2 = oa.oaPoint((xEnd+20), (yEnd-50))
    seg1   = oa.oaPathSeg.create(topBlock, lNum, pNum, point1, point2, segSty)

    routeObjectArrayElements = [seg1]

    connectRouteForNet(topBlock, net1, routeObjectArrayElements, instTerm, pin)

    net2  = createNet(topBlock, "N2")
    term2 = createTerm(net2, "T2", oa.oaTermType(oa.oacInputTermType))
    createInstTerm(net2, inst1, "b")

    point1 = oa.oaPoint((xEnd+40), yEnd)
    point2 = oa.oaPoint((xEnd+40), (yEnd-50))
    createFigForNet(top, topBlock, term2, net2, point1, point2, "P-2")

    net3 = createNet(topBlock, "N3")
    createInstTerm(net3, inst1, "SUM")

    center= oa.oaPoint()
    box.getCenter(center)

    
    inst3.getBBox(box)

    xEnd3 = box.left()
    yEnd3 = box.bottom()

    center3 = oa.oaPoint()
    box.getCenter(center3)

    centerX = center.x()
    centerY = center.y()
    point1 = oa.oaPoint((centerX+0), (centerY + width/2))
    point2 = oa.oaPoint(xEnd3,  (yEnd3+height-20))
    createFigForNet(top, topBlock, None, net3, point1, point2, "", 0)

    net4  = createNet(topBlock, "N4")
    term3 = createTerm(net4, "T3",  oa.oaTermType(oa.oacInputTermType))
    createInstTerm(net4, inst2, "a")

    inst2.getBBox(box)
    xEnd2 =  box.left()
    yEnd2 =  box.bottom()

    center2 = oa.oaPoint()
    box.getCenter(center2)

    point1 = oa.oaPoint((xEnd2+20), yEnd2)
    point2 = oa.oaPoint((xEnd2+20), (yEnd2-50))
    createFigForNet(top, topBlock, term3, net4, point1, point2, "P-3")

    net5  = createNet(topBlock, "N5")
    term4 = createTerm(net5, "T4", oa.oaTermType(oa.oacInputTermType))
    createInstTerm(net5, inst2, "b")

    point1 = oa.oaPoint((xEnd2+40), yEnd2)
    point2 = oa.oaPoint((xEnd2+40), (yEnd2-50))
    createFigForNet(top, topBlock, term4, net5, point1, point2, "P-4")

    net6 =  createNet(topBlock, "N6")
    createInstTerm(net6, inst2, "SUM")

    end2X = center2.x()
    end2Y = center2.y()
    point1 = oa.oaPoint((end2X+0),  (end2Y + (width/2)))
    point2 = oa.oaPoint(xEnd3, (yEnd3+height-40))
    createFigForNet(top, topBlock, None, net6, point1, point2, "", 0)

    createInstTerm(net3, inst3, "a")
    createInstTerm(net6, inst3, "b")

    net7  = createNet(topBlock, "N7")
    term5 = createTerm(net7, "T5", oa.oaTermType(oa.oacOutputTermType))
    createInstTerm(net7, inst3, "SUM")

    end3X = center3.x()
    end3Y = center3.y()

    point1 = oa.oaPoint((end3X+(width/2)), (end3Y+0))
    point2 = oa.oaPoint((end3X+(width/2+50)), (end3Y+0))

    createFigForNet(top, topBlock, term5, net7, point1, point2, "P-5")
    createEmptyNets(top, topBlock, box)

    top.save()
    top.close()




# *****************************************************************************
# Creates the OR schematic Design.
# *****************************************************************************

def createOrSchematic(scNameLib):
    global mode_write

    ns   = oa.oaNativeNS()
    cell = oa.oaScalarName(ns, "Or")
    viewStr = oa.oaScalarName(ns, "schematic")

    vtSch  = oa.oaViewType_get(oa.oaReservedViewType( "schematic"))

    view  = oa.oaDesign.open(scNameLib, cell, viewStr, vtSch, mode_write)
    block = oa.oaBlock.create(view)

    layer = "pin"
    purpose = "drawing"

    pinLayer = 0
    pinPurp  = 0

    (success, pinLayer, pinPurp) = getLayerPurpose(view, layer, purpose)
    assert_def(success, lineno())

    p1 = oa.oaPoint(-50,50)
    p2 = oa.oaPoint(15,50)
    pointArray1 = [p1, p2]

    path  = oa.oaPath.create(block, pinLayer, pinPurp, 2, pointArray1)
    assert_def(path.isValid(), lineno())

    name = oa.oaScalarName(ns, "TopTail")
    net  = oa.oaScalarNet.create(block, name)
    assert_def(net.isValid(), lineno())
    path.addToNet(net)

    termName = oa.oaScalarName(ns, "T1")
    termTypeInput = oa.oaTermType(oa.oacInputTermType)
    term = oa.oaScalarTerm.create(net, termName, termTypeInput)
    assert_def(term.isValid(), lineno())

    box = oa.oaBox(-65, 46, -50, 54)
    rect = oa.oaRect.create(block, pinLayer, pinPurp, box)
    assert_def(rect.isValid(), lineno())

    pin = oa.oaPin.create(term, "IN1", oa.oacLeft)
    rect.addToPin(pin)
    assert_def(pin.isValid(), lineno())

    p3 = oa.oaPoint(-50,20)
    p4 = oa.oaPoint(15,20)
    pointArray2 = [p3, p4]

    path = oa.oaPath.create(block, pinLayer, pinPurp, 2, pointArray2)
    assert_def(path.isValid(), lineno())

    name = oa.oaScalarName(ns, "BotTail")
    net  = oa.oaScalarNet.create(block, name)
    assert_def(net.isValid(), lineno())
    path.addToNet(net)

    termName  = oa.oaScalarName(ns, "T2")
    term  = oa.oaScalarTerm.create(net, termName, termTypeInput)
    assert_def(term.isValid(), lineno())

    box  = oa.oaBox(-65, 16, -50, 24)
    rect = oa.oaRect.create(block, pinLayer, pinPurp, box)
    assert_def(rect.isValid(), lineno())

    pin  = oa.oaPin.create(term, "IN2", oa.oacLeft)
    rect.addToPin(pin)
    assert_def(pin.isValid(), lineno())

    drLayer = 0
    drPurp  = 0

    (success, drLayer, drPurp) = getLayerPurpose(view, "device", "drawing2")
    assert_def(success, lineno())
   
    pt1 = oa.oaPoint(0, 0)
    pt2 = oa.oaPoint(50, 0)
    pt3 = oa.oaPoint(80, 10)
    pt4 = oa.oaPoint(100, 35)
    pt5 = oa.oaPoint(80, 60)
    pt6 = oa.oaPoint(50, 70)
    pt7 = oa.oaPoint(0, 70)
    pt8 = oa.oaPoint(14, 55)
    pt9 = oa.oaPoint(20, 35)
    pt10 = oa.oaPoint(14, 15)
    ptArray3 = [pt1, pt2, pt3, pt4, pt5, pt6, pt7, pt8,pt9, pt10]

    gate = oa.oaPolygon.create(block, drLayer, drPurp, ptArray3)
    assert_def(gate.isValid(), lineno())

    pt11 = oa.oaPoint(100, 35)
    pt12 = oa.oaPoint(140, 35)
    ptArray5 = [ pt11, pt12]

    path2 = oa.oaPath.create(block, pinLayer, pinPurp, 2, ptArray5)
    assert_def(path2.isValid(), lineno())

    name  = oa.oaScalarName(ns, "OutTail")
    net2  = oa.oaScalarNet.create(block, name)
    assert_def(net2.isValid(), lineno())

    path2.addToNet(net2)
    termName  = oa.oaScalarName(ns, "T3")
    termTypeOutput = oa.oaTermType(oa.oacOutputTermType)
    term2  = oa.oaScalarTerm.create(net2, termName, termTypeOutput)

    box = oa.oaBox(140, 31, 155, 39)
    rect2 = oa.oaRect.create(block, pinLayer, pinPurp, box)
    assert_def(rect2.isValid(), lineno())

    pin2 = oa.oaPin.create(term2, "OUT", oa.oacRight)
    rect2.addToPin(pin2)
    assert_def(pin2.isValid(),lineno())

    view.save()
    view.close()





#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>next



def openLibrary(argv):

    ns = oa.oaUnixNS()
    lib = "NULL"

    libstr=argv[1]
    scNameLib = oa.oaScalarName( ns, libstr )
    strPathLib = argv[2]

    lib = oa.oaLib.find(scNameLib)
    if lib: 
        print "***ERROR error lib is already open"
        return 1


    # The following commented out code illustrates how the API locates a lib.defs file.
    # Unfortunately, the results are unpredictable because the current user may or may
    # not have a lib.defs file either in the current directory or in $HOME.
    # It has been commented out so that automated run/check tests can predict the output.
    # If you are a user who is interested in this, please feel free to uncomment this section.
    strLibDefsFile = oa.oaString()
    oa.oaLibDefList.getDefaultPath(strLibDefsFile)
    
    #if strLibDefsFile == "":
    #    print "No libdefs file"
    #else:
    #    print "Existing lib.defs file"

    ldl = oa.oaLibDefList.get( "lib.defs", "w")

    if oa.oaLib.exists(strPathLib):
        lib = oa.oaLib.open(scNameLib, strPathLib)
        print  "Opened existing %s in %s" % (libstr,  strPathLib)
    else:
        lib = oa.oaLib.create(scNameLib, strPathLib, oa.oaLibMode(oa.oacSharedLibMode), 'oaDMFileSys')
        print "Created %s in %s" % (libstr,  strPathLib) 
    
    oa.oaLibDef.create(ldl, scNameLib, strPathLib)
    ldl.save()
    print "  Adding def for %s to %s to newly created libdefs file\n" % (libstr, strPathLib)

    if not lib.isValid():
        print "***ERROR lib is not valid"
    
    if not oa.oaLib.exists(strPathLib):
        print "***ERROR lib does not exist"
    
    return scNameLib



# ****************************** MAIN ******************************

oa.oaDesignInit()
ns = oa.oaNativeNS()

def main():

    if (len(sys.argv) < 3):
        print " Use : \tsample.py  LibName  LibDirectoryPath"
        return 1 
 
     

    scNameLib = openLibrary(sys.argv)

    createInvSchematic(scNameLib)
    createOrSchematic(scNameLib)
    createNandSchematic(scNameLib)
    createGateSchematic(scNameLib)
    createHierarchy(scNameLib)
    print "\n......Normal completion......"



if (__name__ == "__main__"):
    sys.exit(main())

