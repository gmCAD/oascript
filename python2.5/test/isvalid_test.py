#!/usr/bin/env python
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '..'))

from optparse import OptionParser

import oa.design


def time_isvalid(block, num_iter):
    timer = oa.oaTimer()
    for i in range(num_iter):
        block.isValid()
    return timer.getElapsed()

def main():

    verbose = 0
    nums = (1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000)
    warmup_num = 1000
    
    parser = OptionParser()
    parser.add_option("--libdefs", action="store", type="string", dest="libdefs", metavar="FILE",
                      help="Location of OA library definition file.")
    parser.add_option("--lib", action="store", type="string", dest="lib", metavar="<libname>", help="libname of design")
    parser.add_option("--cell", action="store", type="string", dest="cell", metavar="<cellname>", help="cellname of design")
    parser.add_option("--view", action="store", type="string", dest="view", metavar="<viewname>", help="viewname of design")
    (options, args) = parser.parse_args()

    if (not options.lib or not options.cell or not options.view) :
        print "Missing --lib, --cell, or --view."
        return 1

    oa.oaDesignInit()

    if (options.libdefs) :
        oa.oaLibDefList.openLibs(options.libdefs)
    else :
        oa.oaLibDefList.openLibs()

    ns = oa.oaNativeNS()

    lib_scname = oa.oaScalarName(ns, options.lib)
    lib = oa.oaLib.find(lib_scname)
    if not lib:
        print "Couldn't find lib %s" % options.lib
        return 1
    if not lib.getAccess(oa.oaLibAccess(oa.oacReadLibAccess)):
        print "lib %s: No Access!" % options.lib
        return 1

    cell_scname = oa.oaScalarName(ns, options.cell)
    view_scname = oa.oaScalarName(ns, options.view)

    design = oa.oaDesign.open(lib_scname, cell_scname, view_scname, "r")
    if not design:
        print "No design!"
        return 1

    topBlock = design.getTopBlock()
    if not topBlock:
        print "No top block!"
        return 1

    if verbose:
        print "Warmup call to isValid (%d times)...", warmup_num
    time_isvalid(topBlock, warmup_num)

    print "%16s %16s" % ('# Iter', 'Elapsed (s)')
    print "%16s %16s" % ('------', '-----------')
    for num in nums:
        print "%16d %16.2f" % (num, time_isvalid(topBlock, num))
        
    return 0


if (__name__ == "__main__"):
    sys.exit(main())
