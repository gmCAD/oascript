#!/usr/bin/env python
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.insert(0, os.path.join(os.path.dirname(here), '..'))

from optparse import OptionParser

import oa.design

def main():
    parser = OptionParser()
    parser.add_option("--libdefs", action="store", type="string", dest="libdefs", metavar="FILE",
                      help="Location of OA library definition file.")
    parser.add_option("--lib", action="store", type="string", dest="lib", metavar="<libname>", help="libname of design")
    parser.add_option("--cell", action="store", type="string", dest="cell", metavar="<cellname>", help="cellname of design")
    parser.add_option("--view", action="store", type="string", dest="view", metavar="<viewname>", help="viewname of design")
    parser.add_option("-q", "--quiet", action="store_true", dest="quiet", default=False,
                      help="Suppress output.")
    (options, args) = parser.parse_args()

    if (not options.lib or not options.cell or not options.view) :
        print "Missing --lib, --cell, or --view."
        return 1

    oa.oaDesignInit()

    if (options.libdefs) :
        oa.oaLibDefList.openLibs(options.libdefs)
    else :
        oa.oaLibDefList.openLibs()

    ns = oa.oaNativeNS()

    lib_scname = oa.oaScalarName(ns, options.lib)
    lib = oa.oaLib.find(lib_scname)
    if not lib:
        print "Couldn't find lib %s" % options.lib
        return 1
    if not lib.getAccess(oa.oaLibAccess(oa.oacReadLibAccess)):
        print "lib %s: No Access!" % options.lib
        return 1

    # cell_scname = oa.oaScalarName(ns, options.cell)
    # view_scname = oa.oaScalarName(ns, options.view)

    # design = oa.oaDesign.open(lib_scname, cell_scname, view_scname, "r")
    design = oa.oaDesign.open(options.lib, options.cell, options.view, "r")
    if not design:
        print "No design!"
        return 1

    topBlock = design.getTopBlock()
    if not topBlock:
        print "No top block!"
        return 1
    for shape in topBlock.getShapes():
        bbox = shape.getBBox()
        print "%s on layer %d with bbox %s" % (shape.getType().getName(), shape.getLayerNum(), tuple(bbox))

    if not options.quiet :
        for shape in topBlock.getShapes():
            bbox = oa.oaBox()
            shape.getBBox(bbox)
            print "%s on layer %d with bbox %s" % (shape.getType().getName(), shape.getLayerNum(), bbox)
    else :
        count = 0
        for shape in topBlock.getShapes():
            bbox = oa.oaBox()
            shape.getBBox(bbox)
            count = count + 1
        print "%d shapes" % count

    return 0


if (__name__ == "__main__"):
    sys.exit(main())
