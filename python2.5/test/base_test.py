#!/usr/bin/env python
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '..'))
import time

import oa.base

def main():
    oa.oaBaseInit()
    timer = oa.oaTimer()
    bi_arr = []
    oa.oaBuildInfo.getPackages(bi_arr)
    print "%-15s  %-15s\n" % ("package", "build name")
    print '-' * 30
    for bi in bi_arr:
        print "%-15s  %-15s" % (bi.getPackageName(), bi.getBuildName())

    time.sleep(1)
    print "That took %f seconds.\n\n" % timer.getElapsed()

if __name__ == "__main__":
    main()
