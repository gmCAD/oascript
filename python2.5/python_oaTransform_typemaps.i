/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


%header %{

PyObject*
oaTransform_to_tuple (const oaTransform& transform)
{
    PyObject *result = PyTuple_New(3);
    assert(result);
    PyTuple_SET_ITEM(result, 0, PyInt_FromLong((long) transform.xOffset()));
    PyTuple_SET_ITEM(result, 1, PyInt_FromLong((long) transform.yOffset()));
    PyTuple_SET_ITEM(result, 2, PyInt_FromLong((long) transform.orient()));
    return result;
}

%}

%typemap(in) const OpenAccess_4::oaTransform& (OpenAccess_4::oaTransform tmpTransform)
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = langobj_to_oaTransform($input, tmpTransform))))
	$1 = &tmpTransform;
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))))
	$1 = reinterpret_cast<$1_basetype *>(argp);
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typemap(in) OpenAccess_4::oaTransform
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = langobj_to_oaTransform($input, $1)))) {}
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))))
	$1 = *(reinterpret_cast<$1_basetype *>(argp));
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typecheck(0) (const OpenAccess_4::oaTransform&)
{
    $1 = ((PyTuple_Check($input) && PyTuple_GET_SIZE($input) == 3) ||
	  SWIG_IsOK(SWIG_ConvertPtr($input, NULL, $1_descriptor, 0)));
}

%typecheck(0) (OpenAccess_4::oaTransform) = (const OpenAccess_4::oaTransform&);

