This is a description of the python bindings for OpenAccess.

There is no comprehensive API documentation for the python interface; please
refer to the C++ OpenAccess API documentation.

There are test programs in the test/ directory.

TODO : More about test programs.


--------------------------------------------------------------------------------
Modules and Namespaces
--------------------------------------------------------------------------------

To keep the code size manageable, the API is split into parts, based on the
subdirectories in the OA source area.  Currently, the python modules are:

    oa.base
    oa.common
    oa.design
    oa.dm
    oa.plugin
    oa.tech
    oa.util
    oa.wafer

However, to avoid unnecessary nesting of namespaces, all the classes defined in
these modules are promoted into the 'oa' namespace.  For example, the C++ oaLib
class, which is defined in module oa.dm, can be accessed as oa.oaLib.


--------------------------------------------------------------------------------
Class Instance Methods and Class Static Methods
--------------------------------------------------------------------------------

Class static methods can be invoked in the idiomatic python way:

    design = oa.oaDesign.open(lib, cell, view, 'r')

Note that this interface does NOT support the Pyoa convention for calling
static class methods:

    design = oaDesign.static_open(lib, cell, view, 'r')  ** wrong **

Instance methods can be invoked on an object in the idiomatic python way:

    lib = design.getLib()

When calling an instance method, the python interface will try to verify that
the calling object is valid (using the oaIsValid() from the C++ API).


--------------------------------------------------------------------------------
Enumerated Types and Enum Wrappers
--------------------------------------------------------------------------------

All enumerated values in OA (e.g., oaLibAccessEnum) are available as integer
constants in the oa namespace:

    access = oa.oacReadLibAccess

Classes that are simple wrappers around an enumerated value (e.g. oaLibAccess)
are supported in the wrappers:

    access = oa.oaLibAccess(oa.oacReadLibAccess)
    lib.getAccess(access)

Enum wrapper classes define the __int__() operator, so you may use the int()
operator to compare an enum wrapper object with an enumerated value:

    if (int(ref.getOrient()) == oa.oacR90) :
        ...


--------------------------------------------------------------------------------
oaString, oaName, and oaNameSpace
--------------------------------------------------------------------------------

The oaString class is wrapped in the API.  It has operator overloads which make
it interchangeable with a python string:

    >>> oastr = oa.oaString("foo")
    >>> print oastr
    'foo'
    >>> print "%sbar" % oastr
    'foobar'
    >>> print oastr + "bar"
    'foobar'

Any method which takes an oaString& argument as a place holder for an output
value must be given an oaString instance:

    >>> ns = oa.oaNativeNS()
    >>> libname = oa.oaString()
    >>> lib.getName(ns, libname)
    >>> print "libname = %s" % libname
    'libname = stdcell'

Any method which takes a const oaString& argument may be given either an
oaString, or a native python string.  Both of these will work:

    if (oa.oaLib.exists(oaString("/path/to/lib"))) :
        ...

    if (oa.oaLib.exists("/path/to/lib")) :
        ...

Note also that the "name" classes in OA (oaScalarName, oaSimpleName, etc.)
are not handled specially; they are fully-fledged class objects:

    ns = oa.oaNativeNS()
    libname = oa.oaScalarName(ns, "stdcell")
    lib = oa.oaLib.find(libname)


--------------------------------------------------------------------------------
oaScalarName
--------------------------------------------------------------------------------
The oaScalarName class is specially typemapped to support auto-conversion from
regular strings when used as an input argument.  The string value is
automatically converted to an oaScalarName using the default oaNameSpace.

The default oaNameSpace is contained in the variable:

oa.oaNameSpace.default

And is initialized to an oa.oaNativeNS instance.

To change the default, simply assign a different instance of an oaNameSpace to
the variable.

--------------------------------------------------------------------------------
oaPoint, oaBox, oaTransform
--------------------------------------------------------------------------------

As in the C++ API, coordinates are always integer values in DB units.

oaPoint, oaBox, and oaTransform are fully-fledged classes.  They also have
operators defined to make them behaave like a 2-item, 4-item, and 3-item python
tuple, respectively :

    >>> point = oa.oaPoint(100, 200)
    >>> point
    <oa.base.oaPoint; proxy of <Swig Object of type ... > >
    >>> len(point)
    2
    >>> point[0]
    100
    >>> point[1]
    200
    >>> point * 2
    (100, 200, 100, 200)
    >>> point < (100, 201)
    True

    >>> box = oa.oaBox(2, 4, 6, 8)
    >>> tuple(box)
    (2, 4, 6, 8)   

    >>> t = oa.oaTransform(300, 400, oa.oaOrient(oa.oacR90))
    >>> t[0]
    300
    >>> t[1]
    400
    >>> int(t[2]) == oa.oacR90
    True


--------------------------------------------------------------------------------
Collections and Iterators
--------------------------------------------------------------------------------

oaCollection is supported:

    cell_collection = lib.getCells()
    if (cell_collection.isEmpty()) :
        ...

oaIter's are not used directly.  Instead, oaCollection objects are iterable in
python:

    for cell in lib.getCells() :
        ...


--------------------------------------------------------------------------------
Arrays
--------------------------------------------------------------------------------

The oaArray class is not exposed in python.  Anywhere you would expect to use an
oaArray, the python interface will expect a plain python list instead:

    >>> bi_arr = []
    >>> oa.oaBuildInfo.getPackages(bi_arr)
    >>> bi_arr
    [<oa.base.oaBuildInfo; ... >, <oa.base.oaBuildInfo; ... >, ...]
    >>> str(bi_arr[0].getPackageName())
    'oaBase'

    >>> points = []
    >>> points.append(oa.oaPoint(100, 200))
    >>> points.append(oa.oaPoint(300, 200))
    >>> points.append(oa.oaPoint(300, 400))
    >>> points.append(oa.oaPoint(100, 400))
    >>> polygon.setPoints(points)


--------------------------------------------------------------------------------
oaAppDef
--------------------------------------------------------------------------------

The oa*AppDef template sub-classes are wrapped into two classes:

oa*AppDefTemplate - constructs the template for an object class
oa*AppDefProxy - constructs a proxy to the oa*AppDef class

For example:

    int = oa.oaIntAppDef(oa.oaNet).get("int")
    print "name: %s, default: %s" % (int.getName(), int.getDefault())


--------------------------------------------------------------------------------
Exceptions
--------------------------------------------------------------------------------

Exceptions thrown by the C++ API are converted to python exceptions:

    >>> lib = oa.oaLib.create(oa.oaScalarName(oa.oaNativeNS(), "stdcell"))
    >>> lib = oa.oaLib.create(oa.oaScalarName(oa.oaNativeNS(), "stdcell"))
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    RuntimeError: OpenAccessException (3002) : Library named stdcell exists


--------------------------------------------------------------------------------
Compatibility with Pyoa
--------------------------------------------------------------------------------

** EXPERIMENTAL **

There is some support for running existing Pyoa-based code using these python
wrappers.  Try putting this in your script:

import oa
import oa.compat
