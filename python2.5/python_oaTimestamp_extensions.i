/* -*- mode: python -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


%extend OpenAccess_4::oaTimeStamp
{
    int __int__ () const
    { return (int) (oaUInt4) *self; }

    long __long__ () const
    { return (long) (oaUInt4) *self; }

    %feature("python:slot", "nb_int", functype="unaryfunc") __int__;
    %feature("python:slot", "nb_long", functype="unaryfunc") __long__;
};
