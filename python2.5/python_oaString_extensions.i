/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


%extend OpenAccess_4::oaString
{
    %feature("python:slot", "tp_str", functype="reprfunc") __str__;
    %feature("python:slot", "tp_repr", functype="reprfunc") __repr__;
    %feature("python:slot", "tp_hash", functype="hashfunc") __hash__;
    %feature("python:slot", "nb_add") __add__;
    %feature("python:slot", "sq_contains") __contains__;
    %feature("python:slot", "sq_length", functype="lenfunc") __len__;
    %feature("python:slot", "nb_remainder") __mod__;
    %feature("python:slot", "nb_multiply", functype="binaryfunc") __mul__;

    const char* __str__ () const
    { return (const oaChar*) *self; }

    const char* __repr__ () const
    { return (const oaChar*) *self; }

    long __hash__ () const
    { return __gnu_cxx::__stl_hash_string((const oaChar*) *self); }

    oaString __add__ (const char *other) const
    {
	oaString result(*self);
	result += other;
	return result;
    }
    oaString __add__ (const oaString& other) const
    {
	oaString result(*self);
	result += other;
	return result;
    }

    int __contains__ (const char *other) const
    { return (self->substr(oaString(other)) == self->getLength() ? 0 : 1); }

    int __contains__ (const oaString& other) const
    { return (self->substr(other) == self->getLength() ? 0 : 1); }

    size_t __len__ () const
    { return (size_t) self->getLength(); }

    oaString __mod__ (PyObject *args) const
    {
	// PyUnicode_Format may have different behavior from oaString::format,
	// but this is the best we can do, since we can't pass the varargs along.
	PyObject *pystr = PyUnicode_Decode((const oaChar*) *self, self->getLength(), NULL, NULL);
	PyObject *formatted = PyUnicode_Format(pystr, args);
	PyObject *encoded = PyUnicode_AsEncodedObject(formatted, NULL, NULL);
	oaString result(PyUnicode_AS_DATA(encoded));
	Py_DECREF(encoded);
	Py_DECREF(formatted);
	Py_DECREF(pystr);
	return result;
    }

    oaString __mul__ (size_t num) const
    {
	oaString result;
	while (num-- > 0)
	    result += *self;
	return result;
    }

};

