##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import os
import platform
import sys
import ctypes

ctypes.CDLL(os.path.abspath(os.path.join(__path__[0], "_common.so")), ctypes.RTLD_GLOBAL)
ctypes.CDLL(os.path.abspath(os.path.join(__path__[0], "_base.so")), ctypes.RTLD_GLOBAL)
