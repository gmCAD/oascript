/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%include <macros/dm_collections.i>
%include <macros/dm_iters.i>
%include <macros/dm_enums.i>

%pythoncode %{
import sys as _sys
if _sys.modules.has_key('oa'):
    _oaDict = _sys.modules['oa'].__dict__
    for k in _dm.__all__ :
        _oaDict[k] = _dm.__dict__[k]
%}
