#!/usr/bin/env ruby
################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/15/11  10.0     scarver, Si2     Tutorial 10th Edition - Ruby version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pagesend
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.orgend
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

require 'oa'
include Oa

oaDesignInit

require '../emhlister/dumpUtils.rb'


#     This program creates a simple, hierarchical design: an adder out of
#     2 half-adders, each of which comprises an AND and XOR gate.
#                           _____           ____            _____
#     Leaf cells:       A--|     |      A--|    |       A--|     |
#     from netlist         | AND |--Y      | OR |--Y       | XOR |--Y
#     lab library       B--|_____|      B--|____|       B--|_____|
#
#
#     Embeded Modules created in this code:
#
#     Half-adder block:            Adder block:                           Co|
#         ____________________            __________________________________|__
#        |                    |          |                                  |  |
#        |          ______    |          |   _____       H1c         _____  |  |
#     A--|-----o---|      |   |       A--|--|     |-----------------|     | |  |
#        |     |   | AND1 |---|--C       |  | HA1 | H1s  _____  H2c | OR1 |-o  |
#        |  o------|______|   |       B--|--|_____|-----|     |-----|_____|    |
#        |  |  |              |          |              | HA2 |                |
#        |  |  |              |          |            o-|_____|----------------|--S
#        |  |  |    _____     |          |            |                        |
#        |  |  o---|      |   |          |            |_____________________   |
#        |  |      | XOR1 |---|--S       |__________________________________|__|
#     B--|--o------|______|   |                                             |
#        |                    |                                             |Ci
#        |____________________|
#
#
#     3-bit Top Module:
#
#               B0  A0           B1  A1           B2  A2
#                |  |             |  |             |  |
#        ________|__|_____________|__|_____________|__|______
#       |    ____|__|__       ____|__|__       ____|__|__    |
#       |   |    B  A  |     |    B  A  |     |    B  A  |   |
#       |   |          |     |          |     |          |   |
#       |   |   Add0   |     |   Add1   |     |   Add2   |   |
#       |   |          | C01 |          | C12 |          |   |
#  Ci---|---|Ci      Co|-----|Ci      Co|-----|Ci      Co|---|---Co
#       |   |___S______|     |___S______|     |___S______|   |
#       |_______|________________|________________|__________|
#               |                |                |
#               |                |                |
#               S0               S1               S2



$mode_trunc = "w"
$mode_read  = "r"
$mode_edit  = "a"
$visibleToBlock = 1


def new_section (id)
    print "\n\n||||||||||||||||||||||||||||||||||||||||||| "
    print id
    puts " |||||||||||||||||||||||||||||||||||||||||||\n\n"
end

def assert (expression, expression_text)
    print "  ASSERT ["
    if expression
        print "PASS"
    else
        print "FAIL"
    end
    puts "] #{expression_text}"
end


def assertion (assertion)
    puts "ASSERTION: #{assertion}"
end


# ================================= SUBROUTINES  =============================


def get_mapped_name (obj)
    # Many of the getName functions allow you to enter
    #     $myObj getName
    # to get a simple string representing the Object's name.
    #
    # For those that don't, this function does the ScalarName setup and
    # retrieval, and returns the simple string.

    ns = OaNativeNS.new
    scalar_name = OaScalarName.new

    obj.getName scalar_name
    scalar_name.get(ns, simple_string='')
    return simple_string
end


def get_lcv_name (obj)
    # Templatize a function that exploits the commonality of method names to
    # get the Lib/Cell/View name attributes from different types of containers.

    # Return a String that is the concatenation of the Lib/Cell/View names,
    # separated by the / character as a delimiter, using method[s] that are
    # are valid for all possible Inst types the template parameter might designate.
    return "#{obj.getLibName}/#{obj.getCellName}/#{obj getViewName}"
end


def get_num_terms (mod)
    # Return the number of ModTerms in a Module.
    return mod.getTerms.getCount
end


def get_num_instTerms (mod)
    # Return the number of ModInstTerms in a Module.
    return mod.getInstTerms.getCount
end


def get_num_nets (mod)
    # Return the number of ModNets in a Module
    return mod.getNets.getCount
end


def create_modInstTerm_by_name (inst, name, net)
    # Templatize a function that exploits the commonality of method names to
    # get the master Module of an inst is the same for different ModInst
    # classes
    # Tcl note: a template is not needed here.

    # Set the mod variable to the master Module of the Inst argument.
    mod = inst.getMasterModule

    # A Block-specific Design will not have a master Module, so verify
    # mod exists before trying to use the handle to find its ModTerm ons.
    #
    instTerm = nil

    if mod.isValid
        # Find the term with the name specified in the function argument.
        term = OaModTerm.find(mod, name)

        # Verify the handle is not null {ie, there was a Term by that name}.
        #
        if term.isValid

            # Set the instTerm variable to a ModInstTerm created from the term
            # handle.
            instTerm = OaModInstTerm.create(net, inst, term)

            log "Created InstTerm of \"#{name}\" on Inst \"#{inst.getName}\", "+
                "attached to net \"#{net.getName}\"\n"

            assert(instTerm.isValid, "instTerm.isValid")

        else
            puts "***Couldn't find term named '#{name}' on module. Quitting."
            exit 2
        end
    end
    return instTerm
end


def expect_modTerm_on_net (term, net)
    # Create an oaCollection named coll_terms_on_net consisting of the
    # ModTerms on the ModNet passed as a function argument.
    coll_terms_on_net = net.getTerms

    assert(eval("coll_terms_on_net.getCount == 1"),
                 "coll_terms_on_net.getCount == 1")

    # The only Term in the collection should be the one passed in as an arg.
    coll_terms_on_net.each {|got_term|
        assert(eval("got_term == term"),
                    "got_term == term") }
end


def create_modNet (mod, net_name)
      # Create a ModScalarNet such that the following assertions pass and
      # a corresponding Net is automatically created in the Block Domain.
      net = OaModScalarNet.create(mod, net_name)

      assert(net.isValid, "net.isValid")
      assert(eval("net.getModule == mod"),
                  "net.getModule == mod")
      assert(eval("net.getName == net_name"), "net.getName == net_name")
      assert(eval('net.getType.getName == "ModScalarNet"'),
                  'net.getType.getName == "ModScalarNet"')
      assert(eval('net.getSigType.getName == "signal"'),
                  'net.getSigType.getName == "signal"')
      assert(!net.isGlobal, "!net.isGlobal")
      assert(!net.isImplicit, "!net.IsImplicit")
      assert(net.isEmpty, "net.isEmpty")
      assert(net.getEquivalentNets.isEmpty, "net.getEquivalentNets.isEmpty")
      assert(net.getInstTerms.isEmpty, "net.getInstTerms.isEmpty") 
      assert(net.getTerms.isEmpty, "net.getTerms.isEmpty") 

      log "Created ModScalarNet '#{net_name}'\n"
      return net
end


def add_modNet_term (mod, name, io_dir)
    # Use the create_modNet helper function defined above to create a ModScalarNet
    # using the passed arguments. Then create a ModScalarTerm (with the proper
    # OA API function) with attributes specified by the passed arguments, so that
    # appropriate Objects are propagated to the Block Domain and the assertions pass.
    net  = create_modNet(mod, name)
    term = OaModScalarTerm.create(net, name, io_dir)

    assert(eval("net.getModule == mod"),
                "net.getModule == mod")
    assert(eval('net.getType.getName == "ModScalarNet"'),
                'net.getType.getName == "ModScalarNet"')
    assert(eval('net.getSigType.getName == "signal"'),
                'net.getSigType.getName == "signal"')
    assert(!net.isGlobal, "!net.isGlobal")
    assert(eval("term.getModule == mod"),
                "term.getModule == mod")
    assert(eval('term.getType.getName == "ModScalarTerm"'),
                'term.getType.getName == "ModScalarTerm"')
    assert(eval("term.getTermType.to_i == io_dir"), 
                "term.getTermType == io_dir")
    assert(eval("term.getNet == net"),
                "term.getNet == net")

    log "Term '#{term.getName}' created attached to " +
        "Net '#{term.getNet.getName}'\n"

    expect_modTerm_on_net(term, net)
    return net
end


def create_modInst (type, mod, master, inst_name)
    # Templatize a function that exploits the commonality of method names to
    # create Insts of different types.

    # Ruby note: this C++ functionaility is mimicked here using the "type" arg.
    # If type is "OaModScalarInst", then the inst is created with
    # OaModScalarInst.create

    # Create an Inst using the function arguments to specify its master
    # and its owner Module. Assign to the inst variable.

    inst = eval("#{type}.create(mod, master, inst_name)")

    assert(inst.isValid, "inst.isValid")
    log "Created #{inst.getType.getName} '#{inst.getName}'\n"

    return inst
end


def create_half_adder (design, cell_lib_name, design_lib_name, view)

    # Create the "HalfAdder" Module in the specified design Lib and View
    # (passed as args), assigning it to the mod_ha variable.
    mod_ha = OaModule.create(design, "HalfAdderMod")

    # Open the "Xor" and "And" Designs from the cell Lib and View
    # (passed as args) in READ mode.
    design_xor = OaDesign.open(cell_lib_name, "Xor", view, $mode_read)
    design_and = OaDesign.open(cell_lib_name, "And", view, $mode_read)

    begin
        # Use the create_modInst helper function defined in this lab to
        # attempt to create a ModModuleInst in mod_ha of the top Module
        # of the Xor Design.
      create_modInst("OaModModuleScalarInst", mod_ha,
        design_xor.getTopModule, "Xor1q")
    rescue OaException => excp
      if excp.to_s.include? OacModulesNotInSameDesign.to_s
        puts "{As expected, cannot instantiate Modules from other Designs}"
      else
        abort "Unexpected exception while creating ModInst: #{excp}"
      end
    end

    # Use the create_modInst helper function to create ModInsts named "Xor1"
    # and "And1" of  master Designs Xor and And to be owned/contained by mod_ha.
    inst_xor = create_modInst("OaModScalarInst", mod_ha, design_xor, "Xor1")
    inst_and = create_modInst("OaModScalarInst", mod_ha, design_and, "And1")

    log "Creating Design 'HA'.\n"
    $EmhGlobals.add_indent
    assert(mod_ha.isValid, "mod_ha.isValid")

    netA = add_modNet_term(mod_ha, "A", OacInputTermType)
    netB = add_modNet_term(mod_ha, "B", OacInputTermType)
    netC = add_modNet_term(mod_ha, "C", OacOutputTermType)
    netS = add_modNet_term(mod_ha, "S", OacOutputTermType)

    assert(eval("get_num_nets(mod_ha) == 4"),  "get_num_nets(mod_ha) == 4")
    assert(eval("get_num_terms(mod_ha) == 4"), "get_num_terms(mod_ha) == 4")

    # Call create_modInstTerm_by_name, defined above to create ModInstTerms on
    # inst_xor named "A", "B", "Y", connected to netA, netB, NetS, respectively.
    instTermXorA = create_modInstTerm_by_name(inst_xor, "A", netA)
    instTermXorB = create_modInstTerm_by_name(inst_xor, "B", netB)
    instTermXorY = create_modInstTerm_by_name(inst_xor, "Y", netS)

    # Call create_modInstTerm_by_name, defined above, to create ModInstTerms on
    # inst_and named "A", "B", "Y", connected to netA, netB, NetC, respectively.
    instTermAndA = create_modInstTerm_by_name(inst_and, "A", netA)
    instTermAndB = create_modInstTerm_by_name(inst_and, "B", netB)
    instTermAndY = create_modInstTerm_by_name(inst_and, "Y", netC)

    assert(eval("get_num_instTerms(mod_ha) == 6"),
                "get_num_instTerms(mod_ha) == 6")
    $EmhGlobals.sub_indent
    return mod_ha
end

# ---------- Create Design with Modules and Blocks


def create_design (design_lib_name, cell, view)
    # Create a Design with the names passed as arguments, maskLayout ViewType,
    # overwriting any Design of those names/type that might already exist.
    design = OaDesign.open(design_lib_name, cell, view,
               :maskLayout, $mode_trunc)

    assert(design.isValid, "design.isValid")
    assert(eval("design.getRefCount == 1"), "design.getRefCount == 1")
    assert(eval('design.getType.getName == "Design"'),
                'design.getType.getName == "Design"')

    assert(eval('design.getViewType.getName == "maskLayout"'),
                'design.getViewType.getName == "maskLayout"')

    # Assign design a "block" CellType attribute value.
    design.setCellType(OacBlockCellType)

    assert(eval("design.getCellType.to_i == OacBlockCellType"),
                "design.getCellType == OacBlockCellType")
    return design
end


def create_full_adder (design, cell_lib_name, design_lib_name, view)
    # Open the existing design named "Or" in the CellLib in read mode.
    design_or = OaDesign.open(cell_lib_name, "Or", view, $mode_read)

    # Create an embedded mod_fa FullAdder module owned by the Design passed as an argument.
    mod_fa = OaModule.create(design, "FullAdderMod")

    # Call the CreateHalfAdder utility function to create the mod_ha module, also owned by
    # the design, passing along the mapped names of the cell and design libraries, and View.
    mod_ha = create_half_adder(design, cell_lib_name, design_lib_name, view)

    # Call the CreateModInst utility function defined above to create in
    # the mod_fa parent an Inst named "Ha1" and one named "Ha2 of mod_ha.
    inst_ha1 = create_modInst("OaModModuleScalarInst", mod_fa, mod_ha, "Ha1")
    inst_ha2 = create_modInst("OaModModuleScalarInst", mod_fa, mod_ha, "Ha2")

    # Call the CreateModInst{} utility function defined above to create in
    # the mod_fa parent an Inst named "Or1" of design_or.
    inst_or1 = create_modInst("OaModScalarInst", mod_fa, design_or, "Or1")

    #  Create nets and terminals for each I/O on this block.
    #
    netA  = add_modNet_term(mod_fa, "A",  OacInputTermType)
    netB  = add_modNet_term(mod_fa, "B",  OacInputTermType)
    netCi = add_modNet_term(mod_fa, "Ci", OacInputTermType )
    netCo = add_modNet_term(mod_fa, "Co", OacOutputTermType)
    netS  = add_modNet_term(mod_fa, "S",  OacOutputTermType)
      #
      # Create internal nets
      #
    netH1c = create_modNet(mod_fa, "H1c")
    netH1s = create_modNet(mod_fa, "H1s")
    netH2c = create_modNet(mod_fa, "H2c")
    assert(eval("get_num_terms(mod_fa) == 5"), "get_num_terms(mod_fa) == 5")

    $EmhGlobals.add_indent

    # Create an iterator that collects all the ModuleInsts in mod_fa

    mod_fa.getInsts.each {|modInst| 
        puts "  modInst '#{modInst.getName}'" +
            " type=#{modInst.getType.getName}" +
            " InstHeader type=#{modInst.getHeader.getType.getName}" }

  # Create an iterator that collects all the ModuleInstHeaders in mod_fa

    mod_fa.getModuleInstHeaders.each do |mmiHeader|
        puts "     modModuleInstHeader #{get_mapped_name(mmiHeader)}"

        # Create an iterator that collects all the Insts in the Header
        mmiHeader.getInsts.each {|modInst| 
            puts "       modInst '#{modInst.getName}'"}
    end

  # Create ModInstTerms on the HalfAdder Insts.

    create_modInstTerm_by_name(inst_ha1, "A", netA)
    create_modInstTerm_by_name(inst_ha1, "B", netB)
    create_modInstTerm_by_name(inst_ha1, "C", netH1c)
    create_modInstTerm_by_name(inst_ha1, "S", netH1s)

    create_modInstTerm_by_name(inst_ha2, "A", netH1s)
    create_modInstTerm_by_name(inst_ha2, "B", netCi)
    create_modInstTerm_by_name(inst_ha2, "C", netH2c)
    create_modInstTerm_by_name(inst_ha2, "S", netS)

    # Create ModInstTerms on the Or Insts.

    create_modInstTerm_by_name(inst_or1, "A", netH1c)
    create_modInstTerm_by_name(inst_or1, "B", netH2c)
    create_modInstTerm_by_name(inst_or1, "Y", netCo)

    $EmhGlobals.sub_indent
    return mod_fa
end


def create_adder_3bit (cell_lib_name, design_lib_name, view)
    # Create the top level 3-bit adder Design using the arguments passed in.
    #
    design_3bit = create_design(design_lib_name, "Adder3bit", view)

    # Create a Module named "Adder3bitMod" in the newly created Design.
    mod_3bit = OaModule.create(design_3bit, "Adder3bitMod")

    # Set the module created as the topModule BEFORE creating more hierarchy
    # (or it won't propagate to Block domain).
    design_3bit.setTopModule(mod_3bit, $visibleToBlock)

    # Create mod_fa, a FullAdder Module in the 3bit design.
     #
    mod_fa = create_full_adder(design_3bit, cell_lib_name, design_lib_name, view)

    # Use the create_ModInst helper function to create inside design_3bit's top Module,
    # three ModInsts of the mod_fa FullAdder Module just created above. Name them "Fa0",
    # "Fa1", and "Fa2". {Use the schematic drawing at the top of this file as a guide.end
    mmi_adder0 = create_modInst("OaModModuleScalarInst", mod_3bit, mod_fa, "Fa0")
    mmi_adder1 = create_modInst("OaModModuleScalarInst", mod_3bit, mod_fa, "Fa1")
    mmi_adder2 = create_modInst("OaModModuleScalarInst", mod_3bit, mod_fa, "Fa2")

    # Create in the top module of design_3bit each of the ModScalarNets declared above
    # by calling the add_modNet_term helper function defined above, passing in as
    # the name the last two characters of the corresponding Net name.
    # For example netA0 {and the Term created for it by the helperend
    # should be named "A0", and so on.  All "S*" and "Co" Terms should
    # be Output types, with the "A*", "B*", and "Ci" Terns as Input types.
    # {Use the schematic drawing at the top of this file as a guide.end
    netA0 = add_modNet_term(mod_3bit, "A0", OacInputTermType)
    netB0 = add_modNet_term(mod_3bit, "B0", OacInputTermType)
    netS0 = add_modNet_term(mod_3bit, "S0", OacOutputTermType)
    netA1 = add_modNet_term(mod_3bit, "A1", OacInputTermType)
    netB1 = add_modNet_term(mod_3bit, "B1", OacInputTermType)
    netS1 = add_modNet_term(mod_3bit, "S1", OacOutputTermType)
    netA2 = add_modNet_term(mod_3bit, "A2", OacInputTermType)
    netB2 = add_modNet_term(mod_3bit, "B2", OacInputTermType)
    netS2 = add_modNet_term(mod_3bit, "S2", OacOutputTermType)
    netA3 = add_modNet_term(mod_3bit, "A3", OacInputTermType)
    netB3 = add_modNet_term(mod_3bit, "B3", OacInputTermType)
    netS3 = add_modNet_term(mod_3bit, "S3", OacOutputTermType)
    netCi = add_modNet_term(mod_3bit, "Ci", OacInputTermType)
    netCo = add_modNet_term(mod_3bit, "Co", OacOutputTermType)

    # Create in the top module of design_3bit two ModScalarNets declared above
    # by calling the CreateModNet{} helper function defined above, passing in
    # as the names "C01" and "C02", respectively.
    netC01 = create_modNet(mod_3bit, "C01")
    netC12 = create_modNet(mod_3bit, "C12")

    # Call the CreateModInstTermByName{} helper function defined above
    # to create ModInstTerms on each of the three FullAdder Insts for all
    # master Terms, connecting each to the corresponding Net created above.
    # {Use the schematicdrawing at the top of this file as a guide.end
    #
    itAdd0A  = create_modInstTerm_by_name(mmi_adder0, "A",  netA0)
    itAdd0B  = create_modInstTerm_by_name(mmi_adder0, "B",  netB0)
    itAdd0S  = create_modInstTerm_by_name(mmi_adder0, "S",  netS0)
    itAdd0Ci = create_modInstTerm_by_name(mmi_adder0, "Ci", netCi)
    itAdd0Co = create_modInstTerm_by_name(mmi_adder0, "Co", netC01)
    itAdd1A  = create_modInstTerm_by_name(mmi_adder1, "A",  netA1)
    itAdd1B  = create_modInstTerm_by_name(mmi_adder1, "B",  netB1)
    itAdd1S  = create_modInstTerm_by_name(mmi_adder1, "S",  netS1)
    itAdd1Ci = create_modInstTerm_by_name(mmi_adder1, "Ci", netC01)
    itAdd1Co = create_modInstTerm_by_name(mmi_adder1, "Co", netC12)
    itAdd2A  = create_modInstTerm_by_name(mmi_adder2, "A",  netA2)
    itAdd2B  = create_modInstTerm_by_name(mmi_adder2, "B",  netB2)
    itAdd2S  = create_modInstTerm_by_name(mmi_adder2, "S",  netS2)
    itAdd2Ci = create_modInstTerm_by_name(mmi_adder2, "Ci", netC12)
    itAdd2Co = create_modInstTerm_by_name(mmi_adder2, "Co", netCo)

    return design_3bit
end


# ================================  M A I N  ================================

begin
    lib_net_name  = "Lib"
    lib_mod_name  = "Lib3bitAdder"
    cellName      = "Lib"
    view_name     = "netlist"
    libModDir     = "Lib3bit"
    libDefsFile   = "lib.defs"


    # Perform logical lib name to physical path name mappings for the source
    # library created in the netlist/ lab, then the symbol library to be
    # created in this lab.
    OaLibDefList.getDefaultPath(strLibDefDef='')

    if strLibDefDef != libDefsFile
        puts "***Missing local $libDefsFile file."
        puts "   Please create one with the one line shown below: "
        puts "       INCLUDE ../netlist/lib.defs"
        exit 4
    end
    OaLibDefList.openLibs

    libNet = OaLib.find(lib_net_name)

    if !libNet.isValid
        puts  "***Either lib.defs file missing or netlist lab was not completed."
        exit 1
    end


    # Create (or open if this lab is being rerun) the output 3bit Module
    # library.
    #
    libMod = OaLib.find(lib_mod_name)

		if libMod != nil
        puts "LibMod opened via openLibs of #{strLibDefDef}"
    else
        if OaLib.exists(libModDir)
            #
            # Directory there but DEFINE for it missing in lib.defs.
            #
            libMod = OaLib.open(lib_mod_name, libModDir)
            puts "Opened existing #{lib_mod_name} in directory #{libModDir}"
        else
            # No directory for LibMod.
            #
            libMod = OaLib.create(lib_mod_name, libModDir,
              OacSharedLibMode, "oaDMFileSys")
            puts "Created new #{lib_mod_name} in directory #{libModDir}"
        end
        # Add a LibDef for this new Module Lib to the lib.defs file
        #

        ldl = OaLibDefList.get(strLibDefDef, "a")

        puts "\nAppending new Lib def for #{libMod.getName}->#{libModDir}" +
              " to LibDefs file = #{strLibDefDef}"

        OaLibDef.create(ldl, libMod.getName, libModDir)
        ldl.save
    end

    # Pretend to "import" Verilog descriptions of design modules
    #
    #    module Adder3bit(Co, S0,S1,S2,S3, A0,A1,A2,A3, B0,B1,B2,B3, Ci);
    #      input A0,A1,A2,A3, B0,B1,B2,B3, Ci;
    #      output Co, S0,S1,S2,S3;
    #      FA Add0 (Co,S,A,B,Ci);
    #      FA Add1 (Co,S,A,B,Ci);
    #      FA Add2 (Co,S,A,B,Ci);
    #      FA Add3 (Co,S,A,B,Ci);
    #    endmodule
    #    module FullAdder(Co,S,A,B,Ci);
    #      input A,B,Ci;
    #      output Co,S;
    #      HA Ha1 (C,S,A,B);
    #      HA Ha2 (C,S,A,B);
    #      Or Or1 (Y,A,B);
    #    endmodule
    #    module HalfAdder(C,S,A,B);
    #      input A,B;
    #      output C,S;
    #      And And1 (Y,A,B);
    #      Xor Xor1 (Y,A,B);
    #    endmodule

    # Use the CreateAdder3bit function defined in this lab to
    # create design_3bit using the mapped names of the:
    #    - cell library (from the netlist lab) containing the leaf OR, AND, XOR gates
    #    - design library to hold the 3-bit Adder begin created in this lab
    #    - View name to be used for all the design containers created.
    design_3bit = create_adder_3bit(lib_net_name, lib_mod_name, view_name)

    # Save the design at this initial stage BEFORE any partitioning or
    # clustering is done to it.
    design_3bit.save

    $EmhGlobals.dump_occ_domain(false)
    dump_open_designs

    new_section("Detach FullAdderMod")

    # Locate the full-adder module by name in the Design
    mod_fa = OaModule.find(design_3bit, "FullAdderMod")
    assert(mod_fa.isValid, "mod_fa.isValid")

    # Detach the full-adder Module hierarchy from Design 3bitAdder creating a new
    # Design in the same Lib, with the same View name, but a Cell name of "FAdetach".
    des_fa_detatch = mod_fa.detach(lib_mod_name, "FAdetach", view_name)

    dump_open_designs

    new_section("Embed Or")

    # Embed the top Module of the Or Design into the new FADetach Design.
    OaModule.embed(des_fa_detatch,
           OaDesign.open(lib_net_name, "Or", view_name, $mode_read))

    dump_open_designs

    design_3bit.close
    des_fa_detatch.close

    puts "\n...........Normal Termination........\n"
end
