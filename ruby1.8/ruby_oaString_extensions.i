/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     02/16/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */


%extend OpenAccess_4::oaString
{
    const char* to_str () const {
	return (const oaChar*) *self;
    }
};

/*

TODO: convert to Ruby

%pythoncode %{
oaString.__hash__ = lambda self : hash(str(self))
oaString.__add__ = lambda self, arg : oaString(str(self) + arg)
oaString.__contains__ = lambda self, arg : arg in str(self)
oaString.__len__ = lambda self : len(str(self))
oaString.__mod__ = lambda self, *args : oaString(str(self) % args)
oaString.__mul__ = lambda self, num : oaString(str(self) * num)
%}

*/
