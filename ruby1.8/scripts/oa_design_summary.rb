#! /usr/bin/env ruby
###############################################################################
#
# Copyright 2011 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

require 'getoptlong'
require 'oa'
include Oa

##############################################################################
## USAGE ##
###########

usage = "

oa_design_summary.rb [OPTIONS]

Options:

-l, --lib      Library name
-c, --cell     Cell name
-v, --view     View name
-p, --libpath  Library path
-s, --subobjs  Show sub-object type breakdown
-h, --help     Shows this usage

"

# Get arguments from command line
opt_list = [[ '--help', '-h', GetoptLong::NO_ARGUMENT ],
            [ '--lib', '-l', GetoptLong::REQUIRED_ARGUMENT ],
            [ '--cell', '-c', GetoptLong::REQUIRED_ARGUMENT ],
            [ '--view', '-v', GetoptLong::REQUIRED_ARGUMENT ],
            [ '--subobjs', '-s', GetoptLong::NO_ARGUMENT ]]

opts = GetoptLong.new(*opt_list)

# Set option default values
libname = nil
cellname = nil
viewname = nil
subobjs = nil
get_objects = %w(Assignments Blockages Boundaries BusNetDefs BusTermDefs ConnectDefs Clusters 
                 FigGroups GCellPatterns Guides InstHeaders Insts InstTerms LPPHeaders
                 LayerHeaders Markers Pins Nets Routes RowHeaders Rows ScanChains
                 Shapes Steiners Terms TrackPatterns VectorInstDefs Vias ViaHeaders)

# Update option values based on arguments
opts.each do |opt, arg|
  case opt
  when '--help'    : abort usage
  when '--lib'     : libname = arg
  when '--cell'    : cellname = arg
  when '--view'    : viewname = arg
  when '--subobjs' : subobjs = true
  end
end

unless libname and cellname and viewname
  abort usage + "ERROR: Missing lib/cell/view"
end

##############################################################################
## IMPLEMENTATION ##
####################

class OaBlock
  def format_num(title)
    num_s = self.send('get'+title).getCount.to_s
    dots = "." * (58 - title.length - num_s.length)
    "#{title} #{dots} #{num_s}"
  end

  def format_sub_nums(title)
    collection = self.send('get'+title)
    hash = Hash.new {|h,k| h[k] = 0}
    collection.each do |object|
      hash[object.class.to_s] += 1
    end

    hash.keys.sort.map do |klass|
      klass_s = klass.sub(/^Oa::Oa/, '')
      count_s = hash[klass].to_s
      dots = "." * (55 - klass_s.length - count_s.length)
      "   #{klass_s} #{dots} #{count_s}"
    end
  end
end

# Open lib.defs list
OaLibDefList.openLibs

# Open design and get block
design = OaDesign.open(libname, cellname, viewname, 'r')
unless block = design.getTopBlock
  abort "No block for design: #{design}"
end

# Show summary counts...
puts "-" * 60
puts "Summary for block #{design}"
puts "-" * 60
get_objects.each do |title|
  puts block.format_num(title)
  sub = subobjs ? block.format_sub_nums(title) : []
  puts sub.join("\n") unless sub.empty?
end

design.close
