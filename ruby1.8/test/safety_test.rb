#! /usr/bin/env/ruby
###############################################################################
#
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   07/29/2010: Copied from design_test.rb (original design_test.pl authored by
#               AMD) and modified by Intel Corporation to test safety.
#
###############################################################################

require 'oa'
require 'getoptlong'

include Oa

opts = GetoptLong.new(['--libdefs', GetoptLong::REQUIRED_ARGUMENT],
                      ['--lib', GetoptLong::REQUIRED_ARGUMENT],
                      ['--cell', GetoptLong::REQUIRED_ARGUMENT],
                      ['--view', GetoptLong::REQUIRED_ARGUMENT],
                      ['--test-safety', GetoptLong::NO_ARGUMENT])

libname = nil
cellname = nil
viewname = nil
libdefs = nil

opts.each do |opt, arg|
  case opt
  when '--libdefs'
    libdefs = arg
  when '--lib'
    libname = arg
  when '--cell'
    cellname = arg
  when '--view'
    viewname = arg
  end
end

abort "Missing mandatory --lib argument" unless libname
abort "Missing mandatory --cell argument" unless cellname
abort "Missing mandatory --view argument" unless viewname                                     

begin
  oaDesignInit

  if libdefs
    OaLibDefList.openLibs libdefs
  else
    OaLibDefList.openLibs
  end

  unless lib = OaLib.find(libname)
    abort "Couldn't find library #{libname}"
  end

  unless lib.getAccess(OaLibAccess.new('read'))
    abort "lib #{libname}: No access!"
  end

  design = OaDesign.open(libname, cellname, viewname, 'r')
  design.close

  # Test object safety - should have $OASCRIPT_USE_ISVALID unset or set
  # to 1 first or this will crash.
  if ENV['OASCRIPT_USE_ISVALID'] and
      ENV['OASCRIPT_USE_ISVALID'].length > 0 and
      ENV['OASCRIPT_USE_ISVALID'] == '0'
    puts "Testing safety... expecting SEGV (set $OASCRIPT_USE_ISVALID=1 or simply unset for safety)"
  else
    puts "Testing safety... expecting to trap failure (safety on by default; set $OASCRIPT_USE_ISVALID=0 to be unsafe"
  end
  begin
    design.getTopBlock
  rescue ArgumentError
    puts "Safety OK!"
  else
    abort "Unexpected... design should have been invalid by closing"
  end

rescue OaException => excp
  abort "Caught exception: #{excp}"
end
