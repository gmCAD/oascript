/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     08/20/2010: Copied from Perl bindings (authored by AMD)

 *************************************************************************** */

#undef LOOKUP1D
%define LOOKUP1D(name,param1,param2)
%template(name) OpenAccess_4::oa1DLookupTbl< param1, param2 >;
%enddef

#undef LOOKUP2D
%define LOOKUP2D(name,param1,param2,param3)
%template(name) OpenAccess_4::oa2DLookupTbl< param1, param2, param3 >;
%enddef

%include <macros/oa_lookuptables.i>
