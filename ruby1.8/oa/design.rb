###############################################################################
#
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   05/18/2010: Breaking out from oa.rb (flattening sub-module levels)
#
###############################################################################

require 'oa/design.so'

module Oa
  
  class OaDesign
    def inspect
      "OaDesign(#{self})"
    end

    def to_s
      lib_sn = OaScalarName.new
      cell_sn = OaScalarName.new
      view_sn = OaScalarName.new
      getLibName(lib_sn)
      getCellName(cell_sn)
      getViewName(view_sn)
      "#{lib_sn}/#{cell_sn}/#{view_sn}"
    end
  end

  class OaInst
    def inspect
      origin = OaPoint.new
      getOrigin(origin)
      "OaInst(#{self}, #{origin.inspect})"
    end

    def to_s(ns=OaNativeNS.new)
      name = '' #  OaString.new
      lib_sn = OaScalarName.new
      cell_sn = OaScalarName.new
      view_sn = OaScalarName.new
      getName(ns, name)
      getLibName(lib_sn)
      getCellName(cell_sn)
      getViewName(view_sn)
      "#{name}@#{lib_sn}/#{cell_sn}/#{view_sn}"
    end
  end
  
end
